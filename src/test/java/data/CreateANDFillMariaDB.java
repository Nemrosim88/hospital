package data;


import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

public class CreateANDFillMariaDB {

    private static final String PROP_NAME = "database";
    private static Connection connection;
    private static Statement statement;
    private static final Logger logger = Logger.getLogger(CreateANDFillMariaDB.class);

    @BeforeClass
    public static void setUp() throws Exception {
        ResourceBundle resource = ResourceBundle.getBundle(PROP_NAME);
        Class.forName(resource.getString("MariaDBdriver"));
        logger.info("@BeforeClass, Зарегистрировали драйвер");

        connection = DriverManager.getConnection(resource.getString("MariaDBurl"));
        logger.info("@BeforeClass, Получили connection");

    }

    @AfterClass
    public static void tearDown() throws Exception {
        connection.close();
    }

    @Before
    public void setConnection() {
        try {
            statement = connection.createStatement();

            new CreateAndFill(new HospitalDB(), statement);
            statement.close();
        } catch (SQLException e) {
            logger.info("@Before, Exception -> " + e.getMessage());
        }
    }

    @After
    public void closeConnection() {

    }

}