package data.dao.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.testUtils.dao.TaskTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import entity.Task;
import entity.enums.Type;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class TaskDBTest {

  @Rule
  public final Timeout timeout = Timeout.seconds(2);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<Task> dao;

  @BeforeClass
  public static void setUp() throws Exception {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    dao = new HospitalDAO<>(connection, TaskUtils.getInstance());
  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }


  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }


  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

  @Test
  public void getAllTest() {
    List<Task> expected = TaskTestUtils.getAll();
    List<Task> result = dao.getAll();
    assertThat(expected, is(result));
  }

  @Test
  public void getByIdTest() {
    List<Task> expected = new ArrayList<>();
    expected.add(TaskTestUtils.getAll().get(5));
    List<Task> result = dao.getByKey(Constants.TASK_ID_COLUMN_NAME, 6);
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueTest() {
    List<Task> expected = new ArrayList<>();
    expected.add(TaskTestUtils.getAll().get(0));
    List<Task> result = dao.getByValue(Constants.TASK_INFO_COLUMN_NAME, "Магнитно-резонансная томография мозга");
    assertThat(expected, is(result));
  }

  @Test
  public void insertTest() {
    Task credentials = TaskTestUtils.getNew();
    int expected = dao.insert(credentials);
    assertTrue(expected == 7);
  }

  @Test
  public void isExistTest() {
    Task expected = TaskTestUtils.getAll().get(2);
    Task result = dao.isExist(expected);
    assertThat(expected, is(result));
  }

  @Test
  public void updateTest() throws SQLException {
    Task task = TaskTestUtils.getAll().get(0);
    task.setInfo("Вернуть скальпель из тела пациента");
    task.setType(Type.OPERATION);

    List<Task> expectedList = new ArrayList<>();
    expectedList.add(task);

    boolean successfullyUpdated = dao.update(task);
    assertTrue(successfullyUpdated);

    List<Task> resultList = dao.getByKey(Constants.TASK_ID_COLUMN_NAME, 1);
    assertThat(expectedList, is(resultList));
  }

  @Test
  public void deleteTest() throws SQLException {
    Task user = TaskTestUtils.getAll().get(0);
    int ID = user.getId();

    boolean expected = dao.delete(ID);
    assertTrue(expected);

    List<Task> result = dao.getByKey(Constants.TASK_ID_COLUMN_NAME, ID);

    assertTrue(result.isEmpty());
  }


}
