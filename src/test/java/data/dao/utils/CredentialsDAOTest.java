package data.dao.utils;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.testUtils.dao.CredentialsTestUtils;
import entity.Credentials;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import java.util.ArrayList;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;

import org.junit.rules.Timeout;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class CredentialsDAOTest {

  @Rule
  public final Timeout timeout = Timeout.seconds(2);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<Credentials> dao;

  @BeforeClass
  public static void setUp() throws Exception {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    dao = new HospitalDAO<>(connection, CredentialsUtils.getInstance());
  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }


  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }


  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

  // ------------------------------------------------------------------
  // ----------------------------- TESTS ------------------------------
  // ------------------------------------------------------------------

  @Test
  public void getAllTest() {
    List<Credentials> expected = CredentialsTestUtils.getAll();
    List<Credentials> result = dao.getAll();
    assertThat(expected, is(result));
  }

  @Test
  public void getByIdTest() {
    List<Credentials> expected = new ArrayList<>();
    expected.add(CredentialsTestUtils.getAll().get(5));
    List<Credentials> result = dao.getByKey(Constants.CREDENTIALS_ID_COLUMN_NAME, 6);
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueTest() {
    List<Credentials> expected = new ArrayList<>();
    expected.add(CredentialsTestUtils.getAll().get(5));

    List<Credentials> result = dao.getByValue(Constants.CREDENTIALS_PASSPORT_COLUMN_NAME, "АА038473");
    assertThat(expected, is(result));
  }

  @Test
  public void insertTest() {
    Credentials credentials = CredentialsTestUtils.getNew();
    int expected = dao.insert(credentials);
    assertTrue(expected == 22);
  }

  @Test
  public void isExistTest() {
    Credentials expected = CredentialsTestUtils.getAll().get(5);
    Credentials result = dao.isExist(expected);
    assertThat(expected, is(result));
  }

  @Test
  public void updateTest() throws SQLException {
    Credentials credentials = CredentialsTestUtils.getAll().get(0);
    credentials.setPassport("РА234876");
    credentials.setEmail("dostoevski@google.com");

    List<Credentials> expectedList = new ArrayList<>();
    expectedList.add(credentials);

    boolean successfullyUpdated = dao.update(credentials);
    assertTrue(successfullyUpdated);

    List<Credentials> result = dao.getByKey(Constants.CREDENTIALS_ID_COLUMN_NAME, credentials.getId());
    assertThat(expectedList, is(result));
  }

  @Test
  public void deleteTest() throws SQLException {
    Credentials user = CredentialsTestUtils.getAll().get(0);
    int userID = user.getUserId();

    boolean expected = dao.delete(userID);
    assertTrue(expected);

    List<Credentials> expectedUserAfterDelete = dao.getByKey(Constants.CREDENTIALS_ID_COLUMN_NAME, 1);

    assertTrue(expectedUserAfterDelete.isEmpty());
  }


}
