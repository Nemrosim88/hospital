package data.dao.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.dao.utils.StaffUtils;
import data.testUtils.dao.StaffTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import entity.Staff;
import entity.enums.Position;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class StaffDBTest {

  @Rule
  public final Timeout timeout = Timeout.seconds(2);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<Staff> dao;

  @BeforeClass
  public static void setUp() throws ClassNotFoundException, SQLException {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    dao = new HospitalDAO<>(connection, StaffUtils.getInstance());
  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }

  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }

  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

  // ------------------------------------------------------------------
  // ----------------------------- TESTS ------------------------------
  // ------------------------------------------------------------------

  @Test
  public void getAllTest() {
    List<Staff> expected = StaffTestUtils.getAll();
    List<Staff> result = dao.getAll();
    assertThat(result, is(expected));
  }

  @Test
  public void getByIdTest() {
    List<Staff> expected = new ArrayList<>();
    expected.add(StaffTestUtils.getAll().get(0));
    List<Staff> result = dao.getByKey(Constants.STAFF_ID_COLUMN_NAME, 1);
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueTest() {
    List<Staff> expected = new ArrayList<>();

    expected.add(StaffTestUtils.getAll().get(0));
    expected.add(StaffTestUtils.getAll().get(1));
    expected.add(StaffTestUtils.getAll().get(4));

    List<Staff> result = dao.getByValue(Constants.STAFF_POSITION_COLUMN_NAME, "operator");
    assertThat(expected, is(result));
  }

  @Test
  public void isExistTest() {
    Staff expected = StaffTestUtils.getAll().get(0);
    Staff result = dao.isExist(expected);
    assertThat(expected, is(result));
  }

  @Test
  public void insertTest() {
    Staff entity = StaffTestUtils.getNew();
    int expected = dao.insert(entity);
    assertTrue(expected == 9);
  }

  @Test
  public void updateTest() {
    Staff staff = StaffTestUtils.getAll().get(0);
    staff.setPosition(Position.NURSE);

    boolean successfullyUpdated = dao.update(staff);
    assertTrue(successfullyUpdated);

    List<Staff> expectedList = new ArrayList<>();
    expectedList.add(staff);

    List<Staff> resultList = dao.getByKey(Constants.STAFF_ID_COLUMN_NAME, 1);
    assertThat(expectedList, is(resultList));
  }

  @Test
  public void deleteTest() {
    Staff user = StaffTestUtils.getAll().get(0);
    int ID = user.getId();

    boolean expected = dao.delete(ID);
    assertTrue(expected);

    List<Staff> result = dao.getByKey(Constants.STAFF_ID_COLUMN_NAME, ID);

    assertTrue(result.isEmpty());
  }


}
