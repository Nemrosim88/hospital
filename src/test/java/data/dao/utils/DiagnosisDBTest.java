package data.dao.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.dao.utils.DiagnosisUtils;
import data.testUtils.dao.DiagnosisTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import entity.Diagnosis;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import javax.sql.DataSource;

public class DiagnosisDBTest {

  @Rule
  public final Timeout timeout = Timeout.seconds(2);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<Diagnosis> dao;

  @Test
  public void getAllTest() {
    List<Diagnosis> expected = DiagnosisTestUtils.getAll();
    List<Diagnosis> result = dao.getAll();
    assertThat(result, is(expected));
  }

  @Test
  public void getByIdTest() {
    List<Diagnosis> expected = new ArrayList<>();
    Diagnosis diagnosis = DiagnosisTestUtils.getAll().get(2);
    expected.add(diagnosis);
    List<Diagnosis> result = dao.getByKey(Constants.DIAGNOSIS_ID_COLUMN_NAME, diagnosis.getId());
    assertThat(result, is(expected));
  }

  @Test
  public void getByValueTest() {
    List<Diagnosis> expected = new ArrayList<>();
    expected.add(DiagnosisTestUtils.getAll().get(0));
    expected.add(DiagnosisTestUtils.getAll().get(1));
    expected.add(DiagnosisTestUtils.getAll().get(2));
    List<Diagnosis> result = dao
        .getByValue(Constants.DIAGNOSIS_INFO_COLUMN_NAME, "Поступил для ежегодного мед осмотра");
    assertThat(result, is(expected));
  }

  @Test
  public void isExistTest() {
    Diagnosis expected = DiagnosisTestUtils.getAll().get(1);
    Diagnosis result = dao.isExist(expected);
    assertThat(result, is(expected));
  }

  @Test
  public void insertTest() {
    Diagnosis credentials = DiagnosisTestUtils.getNew();
    int expected = dao.insert(credentials);
    assertTrue(expected == DiagnosisTestUtils.getAll().size() + 1);
  }

  @Test
  public void updateTest() throws SQLException {
    Diagnosis diagnosis = DiagnosisTestUtils.getAll().get(0);
    diagnosis.setInfo("HelloWorld");
    diagnosis.setFinalDiagnosis("Kolobok");

    List<Diagnosis> expectedList = new ArrayList<>();
    expectedList.add(diagnosis);

    boolean successfullyUpdated = dao.update(diagnosis);
    assertTrue(successfullyUpdated);

    List<Diagnosis> resultList = dao.getByKey(Constants.DIAGNOSIS_ID_COLUMN_NAME, diagnosis.getId());
    assertThat(expectedList, is(resultList));
  }

  @Test
  public void deleteTest() throws SQLException {
    Diagnosis diagnosis = DiagnosisTestUtils.getAll().get(0);
    int id = diagnosis.getId();

    boolean expected = dao.delete(id);
    assertTrue(expected);

    List<Diagnosis> expectedUserAfterDelete = dao.getByKey(Constants.DIAGNOSIS_ID_COLUMN_NAME, 1);

    assertTrue(expectedUserAfterDelete.isEmpty());
  }


  @BeforeClass
  public static void setUp() throws Exception {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    dao = new HospitalDAO<>(connection, DiagnosisUtils.getInstance());
  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }


  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }


  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

}
