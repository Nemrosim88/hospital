package data.dao.utils;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.dao.utils.PatientUtils;
import data.testUtils.dao.PatientTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;

import entity.Patient;
import java.util.ArrayList;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PatientDAOTest {
  @Rule
  public final Timeout timeout = Timeout.seconds(2);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<Patient> dao;

  @BeforeClass
  public static void setUp() throws ClassNotFoundException, SQLException {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    dao = new HospitalDAO<>(connection, PatientUtils.getInstance());
  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }

  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }

  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

  @Test
  public void getAllTest() {
    List<Patient> expected = PatientTestUtils.getAll();
    List<Patient> result = dao.getAll();
    assertThat(expected, is(result));
  }

  @Test
  public void getByIdTest() {
    List<Patient> expected = new ArrayList<>();
    Patient patient = PatientTestUtils.getAll().get(2);
    expected.add(patient);
    List<Patient> result = dao.getByKey(Constants.PATIENT_ID_COLUMN_NAME, patient.getId());
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueTest() {
    //TODO У пациента пока нет текстового поля. Добавить тест когда появится
  }

  @Test
  public void isExistTest() {
    Patient expected = PatientTestUtils.getAll().get(2);
    Patient result = dao.isExist(expected);
    assertThat(expected, is(result));
  }

  @Test
  public void insertTest() {
    // Пользователь 22 имеет роль PATIENT но нет записи в таблице "patient"
    Patient registration = PatientTestUtils.getNew();

    int expected = dao.insert(registration);
    assertTrue(expected == 7);
  }

  @Test
  public void updateTest() throws SQLException {
    Patient patient = PatientTestUtils.getAll().get(2);
    patient.setUserId(10);

    List<Patient> expectedList = new ArrayList<>();
    expectedList.add(patient);

    boolean successfullyUpdated = dao.update(patient);
    assertTrue(successfullyUpdated);

    List<Patient> resultList = dao.getByKey(Constants.PATIENT_ID_COLUMN_NAME, patient.getId());
    assertThat(expectedList, is(resultList));
  }

  @Test
  public void deleteTest() throws SQLException {
    Patient registration = PatientTestUtils.getAll().get(0);
    int id = registration.getId();

    boolean expected = dao.delete(id);
    assertTrue(expected);

    List<Patient> expectedUserAfterDelete = dao.getByKey(Constants.PATIENT_ID_COLUMN_NAME, 1);

    assertTrue(expectedUserAfterDelete.isEmpty());
  }
}
