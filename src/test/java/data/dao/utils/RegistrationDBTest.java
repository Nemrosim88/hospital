package data.dao.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.dao.utils.RegistrationUtils;
import data.testUtils.dao.RegistrationTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import entity.Registration;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class RegistrationDBTest {

  @Rule
  public final Timeout timeout = Timeout.seconds(2);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<Registration> dao;

  @BeforeClass
  public static void setUp() throws ClassNotFoundException, SQLException {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    dao = new HospitalDAO<>(connection, RegistrationUtils.getInstance());
  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }

  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }

  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

  @Test
  public void getAllTest() {
    List<Registration> expected = RegistrationTestUtils.getAll();
    List<Registration> result = dao.getAll();
    assertThat(expected, is(result));
  }

  @Test
  public void getByIdTest() {
    List<Registration> expected = new ArrayList<>();
    Registration registration = RegistrationTestUtils.getAll().get(5);
    expected.add(registration);
    List<Registration> result = dao.getByKey(Constants.REGISTRATION_ID_COLUMN_NAME, registration.getId());
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueTest() {
    List<Registration> expected = new ArrayList<>();
    expected.add(RegistrationTestUtils.getAll().get(10));
    List<Registration> result = dao.getByValue(Constants.REGISTRATION_LOGIN_COLUMN_NAME, "login12");
    assertThat(expected, is(result));
  }

  @Test
  public void isExistTest() {
    Registration expected = RegistrationTestUtils.getAll().get(10);
    Registration result = dao.isExist(expected);
    assertThat(expected, is(result));
  }

  @Test
  public void insertTest() {
    // Изначально, в формируемой базе данных у пользователя с ID:6 нет регистрации.
    Registration registration = RegistrationTestUtils.getNew();

    int expected = dao.insert(registration);
    assertTrue(expected == 22);
  }

  @Test
  public void updateTest() throws SQLException {
    Registration registration = RegistrationTestUtils.getAll().get(5);
    registration.setLogin("Tolik");
    registration.setPassword("password");

    List<Registration> expectedList = new ArrayList<>();
    expectedList.add(registration);

    boolean successfullyUpdated = dao.update(registration);
    assertTrue(successfullyUpdated);

    List<Registration> resultList = dao.getByKey(Constants.REGISTRATION_ID_COLUMN_NAME, registration.getId());
    assertThat(expectedList, is(resultList));
  }

  @Test
  public void deleteTest() throws SQLException {
    Registration registration = RegistrationTestUtils.getAll().get(0);
    int id = registration.getId();

    boolean expected = dao.delete(id);
    assertTrue(expected);

    List<Registration> expectedUserAfterDelete = dao.getByKey(Constants.REGISTRATION_ID_COLUMN_NAME, 1);

    assertTrue(expectedUserAfterDelete.isEmpty());
  }


}
