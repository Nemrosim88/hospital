package data.dao.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.dao.utils.ResultUtils;
import data.testUtils.dao.ResultTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import entity.Result;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class ResultDAOTests {

  @Rule
  public final Timeout timeout = Timeout.seconds(2);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<Result> dao;

  @BeforeClass
  public static void setUp() throws ClassNotFoundException, SQLException {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    dao = new HospitalDAO<>(connection, ResultUtils.getInstance());
  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }


  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }


  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

  // ------------------------------------------------------------------
  // ----------------------------- TESTS ------------------------------
  // ------------------------------------------------------------------

  @Test
  public void getAllTest() {
    List<Result> expected = ResultTestUtils.getAll();
    List<Result> result = dao.getAll();
    assertThat(result, is(expected));
  }

  @Test
  public void getByIdTest() {
    Result result = ResultTestUtils.getAll().get(0);

    List<Result> expectedList = new ArrayList<>();
    expectedList.add(result);

    List<Result> resultList = dao.getByKey(Constants.REGISTRATION_ID_COLUMN_NAME, result.getId());
    assertThat(expectedList, is(resultList));
  }

  @Test
  public void getByValueTest() {
    List<Result> expected = new ArrayList<>();
    expected.add(ResultTestUtils.getAll().get(1));
    List<Result> result = dao.getByValue(Constants.RESULT_INFO_COLUMN_NAME, "Удаление аппендицита");
    assertThat(expected, is(result));
  }

  @Test
  public void isExistTest() {
    Result expected = ResultTestUtils.getAll().get(0);
    Result result = dao.isExist(expected);
    assertThat(expected, is(result));
  }

  @Test
  public void insertTest() {
    Result credentials = ResultTestUtils.getNew();
    int expected = dao.insert(credentials);
    assertTrue(expected == 3);
  }

  @Test
  public void updateTest() {
    Result result = ResultTestUtils.getAll().get(0);
    result.setInfo("Всё Ок");

    boolean successfullyUpdated = dao.update(result);
    assertTrue(successfullyUpdated);

    List<Result> expectedList = new ArrayList<>();
    expectedList.add(result);

    List<Result> resultList = dao.getByKey(Constants.REGISTRATION_ID_COLUMN_NAME, 1);
    assertThat(expectedList, is(resultList));
  }

  @Test
  public void deleteTest() {
    Result user = ResultTestUtils.getAll().get(0);
    int ID = user.getId();

    boolean expected = dao.delete(ID);
    assertTrue(expected);

    List<Result> result = dao.getByKey(Constants.REGISTRATION_ID_COLUMN_NAME, ID);

    assertTrue(result.isEmpty());
  }


}
