package data.dao.utils;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.dao.utils.DoctorUtils;
import data.testUtils.dao.DoctorTestUtils;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import entity.Doctor;
import java.util.ArrayList;
import javax.print.Doc;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ResourceBundle;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class DoctorDAOTests {

  @Rule
  public final Timeout timeout = Timeout.seconds(2);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<Doctor> dao;

  @BeforeClass
  public static void setUp() throws ClassNotFoundException, SQLException {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    dao = new HospitalDAO<>(connection, DoctorUtils.getInstance());
  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }

  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }

  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

  @Test
  public void getAllTest() {
    List<Doctor> expected = DoctorTestUtils.getAll();
    List<Doctor> result = dao.getAll();
    assertThat(expected, is(result));
  }

  @Test
  public void getByIdTest() {
    List<Doctor> expected = new ArrayList<>();
    Doctor doctor = DoctorTestUtils.getAll().get(2);
    expected.add(doctor);
    List<Doctor> result = dao.getByKey(Constants.DOCTOR_ID_COLUMN_NAME, doctor.getId());
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueTest() {
    //TODO У врача пока нет текстового поля. Добавить тест когда появится
  }

  @Test
  public void isExistTest() {
    Doctor expected = DoctorTestUtils.getAll().get(2);
    Doctor result = dao.isExist(expected);
    assertThat(expected, is(result));
  }

  @Test
  public void insertTest() {
    // Изначально, в формируемой базе данных у пользователя с ID:6 нет регистрации.
    Doctor registration = DoctorTestUtils.getNew();

    int expected = dao.insert(registration);
    assertTrue(expected == 6);
  }

  @Test
  public void updateTest() throws SQLException {
    Doctor doctor = DoctorTestUtils.getAll().get(2);
    doctor.setCardKeyId(345632);

    List<Doctor> expectedList = new ArrayList<>();
    expectedList.add(doctor);

    boolean successfullyUpdated = dao.update(doctor);
    assertTrue(successfullyUpdated);

    List<Doctor> resultList = dao.getByKey(Constants.DOCTOR_ID_COLUMN_NAME, doctor.getId());
    assertThat(expectedList, is(resultList));
  }

  @Test
  public void deleteTest() throws SQLException {
    Doctor registration = DoctorTestUtils.getAll().get(0);
    int id = registration.getId();

    boolean expected = dao.delete(id);
    assertTrue(expected);

    List<Doctor> expectedUserAfterDelete = dao.getByKey(Constants.DOCTOR_ID_COLUMN_NAME, 1);

    assertTrue(expectedUserAfterDelete.isEmpty());
  }

}
