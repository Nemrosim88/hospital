package data.dao.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import entity.Credentials;
import entity.Registration;
import entity.User;
import data.testUtils.dataBaseCreateAndFill.CreateAndFill;
import data.testUtils.dataBaseCreateAndFill.HospitalDB;
import data.testUtils.dao.UserTestUtils;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javax.sql.DataSource;

import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class UserDBTest {

  @Rule
  public final Timeout timeout = Timeout.seconds(3);

  private static Connection connection;
  private static Statement statement;
  private static HospitalDAO<User> userDA0;
  private static HospitalDAO<Registration> registrationDA0;
  private static HospitalDAO<Credentials> credentialsDA0;

  @Test
  public void getAllUsersTest() {
    List<User> expected = UserTestUtils.getListOfUsers();
    List<User> result = userDA0.getAll();
    assertThat(expected, is(result));
  }

  @Test
  public void getByKeyTest() {
    List<User> expected = new ArrayList<>();

    User user = UserTestUtils.getListOfUsers().get(5);
    expected.add(user);
    List<User> result = userDA0.getByKey(Constants.USER_ID_COLUMN_NAME, user.getId());
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueSurnameTest() {
    List<User> expected = new ArrayList<>();
    expected.add(UserTestUtils.getListOfUsers().get(5));
    List<User> result = userDA0.getByValue(Constants.USER_SURNAME_COLUMN_NAME, "Белинский");
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueNameTest() {
    List<User> expected = new ArrayList<>();
    expected.add(UserTestUtils.getListOfUsers().get(10));
    expected.add(UserTestUtils.getListOfUsers().get(11));
    List<User> result = userDA0.getByValue(Constants.USER_NAME_COLUMN_NAME, "Василий");
    assertThat(expected, is(result));
  }

  @Test
  public void getByValueRoleTest() {
    List<User> expected = new ArrayList<>();
    expected.add(UserTestUtils.getListOfUsers().get(0));
    expected.add(UserTestUtils.getListOfUsers().get(3));
    expected.add(UserTestUtils.getListOfUsers().get(4));
    expected.add(UserTestUtils.getListOfUsers().get(7));
    expected.add(UserTestUtils.getListOfUsers().get(14));
    expected.add(UserTestUtils.getListOfUsers().get(16));
    List<User> result = userDA0.getByValue(Constants.USER_ROLE_COLUMN_NAME, "doctor");
    assertThat(expected, is(result));
  }

  @Test
  public void insertTest() {
    User user = UserTestUtils.getUser();
    int expected = userDA0.insert(user);
    assertTrue(expected == 23);
  }

  @Test
  public void isExistTest() {
    User expected = UserTestUtils.getListOfUsers().get(5);
    User result = userDA0.isExist(expected);
    assertTrue(result != null);
  }

  @Test
  public void updateTest() throws SQLException {
    List<User> expectedList = new ArrayList<>();
    User user = UserTestUtils.getListOfUsers().get(0);
    user.setPatronymic("ГригоРьевич");
    user.setName("иВаН");
    expectedList.add(user);

    boolean successfullyUpdated = userDA0.update(user);
    assertTrue(successfullyUpdated);

    List<User> expectedUserAfterUpdate = userDA0.getByKey(Constants.USER_ID_COLUMN_NAME, user.getId());
    assertThat(expectedList, is(expectedUserAfterUpdate));
  }

  @Test
  public void deleteTest() throws SQLException {
    List<User> expectedList = new ArrayList<>();
    User user = UserTestUtils.getListOfUsers().get(0);
    expectedList.add(user);

    int userID = user.getId();

    boolean expected = userDA0.delete(userID);
    assertTrue(expected);

    List<User> expectedUserAfterDelete = userDA0.getByKey(Constants.USER_ID_COLUMN_NAME, userID);
    List<Credentials> credentials = credentialsDA0.getByKey(Constants.CREDENTIALS_ID_COLUMN_NAME, userID);
    List<Registration> registration = registrationDA0.getByKey(Constants.REGISTRATION_ID_COLUMN_NAME, userID);

    // Проверка на CASCADE SQL таблиц
    assertTrue(expectedUserAfterDelete.isEmpty());
    assertTrue(credentials.isEmpty());
    assertTrue(registration.isEmpty());
  }


  @BeforeClass
  public static void setUp() throws Exception {
    ResourceBundle resource = ResourceBundle.getBundle("database");
    Class.forName(resource.getString("H2driver"));
    DataSource dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    connection = dataSource.getConnection();
    userDA0 = new HospitalDAO<>(connection, UserUtils.getInstance());
    registrationDA0 = new HospitalDAO<>(connection, RegistrationUtils.getInstance());
    credentialsDA0 = new HospitalDAO<>(connection, CredentialsUtils.getInstance());

  }

  @Before
  public void setConnection() throws SQLException {
    statement = connection.createStatement();
    new CreateAndFill(new HospitalDB(), statement);
    statement.close();
  }


  @After
  public void closeConnection() throws SQLException {
    statement = connection.createStatement();
    statement.execute("DROP SCHEMA hospital");
    statement.close();
  }


  @AfterClass
  public static void tearDown() throws Exception {
    connection.close();
    assertTrue(connection.isClosed());
    assertTrue(statement.isClosed());
  }

}
