package data.dao;

import data.dao.utils.*;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@Suite.SuiteClasses(
    {
        UserDBTest.class,
        CredentialsDAOTest.class,
        RegistrationDBTest.class,
        PatientDAOTest.class,
        DoctorDAOTests.class,
        TaskDBTest.class,
        ResultDAOTests.class,
        StaffDBTest.class,
        DiagnosisDBTest.class,
//        CreateANDFillMariaDB.class
    }
)
@RunWith(Suite.class)
public class DAOTestSuite {

}
