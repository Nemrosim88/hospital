package data.testUtils.dao;

import entity.Result;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ResultTestUtils {

  public static Result getNew() {
    return new Result(3, Timestamp.valueOf("2017-09-16 11:20:45"),
        "Всё ок",
        null, 5);
  }

  public static List<Result> getAll() {
    List<Result> list = new ArrayList<>();
    list.add(new Result(1, 6, Timestamp.valueOf("2017-09-15 11:20:45"),
        "Пневмония, профессиональные поражения, туберкулез, доброкачественные и злокачественные опухоли, инородные "
            + "тела в дыхательных путях - отстутствуют.",
        null, 5));

    list.add(new Result(2, 4, Timestamp.valueOf("2017-09-16 11:20:45"),
        "Удаление аппендицита",
        2, null));
    return list;
  }

}
