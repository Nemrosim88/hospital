package data.testUtils.dao;

import entity.Registration;

import java.util.ArrayList;
import java.util.List;

public class RegistrationTestUtils {

  public static Registration getNew() {
    return new Registration(6, "HelloWorld!", "newPassword");
  }

  public static List<Registration> getAll() {
    List<Registration> list = new ArrayList<>();
    list.add(new Registration(1, 1, "login1", "12345"));
    list.add(new Registration(2, 2, "login2", "12345"));
    list.add(new Registration(3, 3, "login3", "12345"));
    list.add(new Registration(4, 4, "login4", "12345"));
    list.add(new Registration(5, 5, "login5", "12345"));

    // У пользователя с ID:6 заведомо нет регистрации
//    list.add(new Registration(6, "login6", "12345"));

    list.add(new Registration(6, 7, "login7", "12345"));
    list.add(new Registration(7, 8, "login8", "12345"));
    list.add(new Registration(8, 9, "login9", "12345"));
    list.add(new Registration(9, 10, "login10", "12345"));
    list.add(new Registration(10, 11, "login11", "12345"));
    list.add(new Registration(11, 12, "login12", "12345"));
    list.add(new Registration(12, 13, "login13", "12345"));
    list.add(new Registration(13, 14, "login14", "12345"));
    list.add(new Registration(14, 15, "login15", "12345"));
    list.add(new Registration(15, 16, "login16", "12345"));
    list.add(new Registration(16, 17, "login17", "12345"));
    list.add(new Registration(17, 18, "login18", "12345"));
    list.add(new Registration(18, 19, "login19", "12345"));
    list.add(new Registration(19, 20, "login20", "12345"));
    list.add(new Registration(20, 21, "login21", "12345"));
    list.add(new Registration(21, 22, "login22", "12345"));

    return list;
  }

}
