package data.testUtils.dao.abstraction;

import entity.abstraction.Entity;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public interface DAOUtils<E extends Entity> {

  static String readQuery(String fileName) throws IOException {
    String path = "\\src\\main\\resources\\sql\\";
    String absolutePath = new File("").getAbsolutePath();
    StringBuilder sb = new StringBuilder();
    Files.readAllLines(Paths.get(absolutePath + path + fileName)).forEach(sb::append);
    return sb.toString();
  }

  E getNew();

  List<E> getAll();

}
