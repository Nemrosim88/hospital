package data.testUtils.dao;

import entity.Credentials;
import java.util.ArrayList;
import java.util.List;

public class CredentialsTestUtils {


  public static Credentials getNew() {
    return new Credentials(22, "ОП567234", "gogol@gukr.net", "+38(066)396-74-25");
  }

  public static List<Credentials> getAll() {
    List<Credentials> list = new ArrayList<>();
    list.add(new Credentials(1,1, "ПА234321", "victorov@gmail.com", "+38(063)111-22-33"));
    list.add(new Credentials(2,2, "ЛО596857", "spiridonov@gmail.com", "+38(050)475-43-90"));
    list.add(new Credentials(3,3, "ПР857968", "arsenskiy@ukr.net", "+38(050)475-43-91"));
    list.add(new Credentials(4,4, "НА867596", "andronovkiy@mail.ru", "+38(050)475-43-92"));
    list.add(new Credentials(5,5, "УК857465", "arsenskiy@gmail.com", "+38(050)475-43-93"));
    list.add(new Credentials(6,6, "АА038473", "belinskiy@gmail.com", "+38(050)475-43-34"));
    list.add(new Credentials(7,7, "КН647586", "blohin@mail.ru", "+38(050)475-45-67"));
    list.add(new Credentials(8,8, "ОГ273648", "burda@ukr.net", "+38(050)475-12-12"));
    list.add(new Credentials(9,9, "ПА194857", "antonovich@ukr.net", "+38(063)475-45-23"));
    list.add(new Credentials(10,10, "НЕ109857", "vadimorskiy@gmail.com", "+38(050)475-43-95"));
    list.add(new Credentials(11,11, "ЕА675840", "vasiliak@gmail.com", "+38(050)475-43-96"));
    list.add(new Credentials(12,12, "ОО305631", "vasil.vasiliak@mail.ru", "+38(050)475-43-97"));
    list.add(new Credentials(13,13, "СН300948", "volodar@gmail.com", "+38(050)475-43-98"));
    list.add(new Credentials(14,14, "МС101937", "gavriliak@ukr.net", "+38(050)475-43-99"));
    list.add(new Credentials(15,15, "УУ475860", "geraldin@gmail.com", "+38(050)475-43-00"));
    list.add(new Credentials(16,16, "ЛО049576", "gordon@epam.ua", "+38(050)475-43-01"));
    list.add(new Credentials(17,17, "СС294836", "gerasimenko@codefire.com", "+38(050)475-43-02"));
    list.add(new Credentials(18,18, "ЗУ394584", "dagmeria@mail.ru", "+38(050)475-43-03"));
    list.add(new Credentials(19,19, "УТ947563", "dianova@gmail.com", "+38(050)475-43-04"));
    list.add(new Credentials(20,20, "УК947574", "dolena@ukr.net", "+38(050)475-43-05"));
    list.add(new Credentials(21,21, "ЛЕ836283", "ditovskaia@gmail.com", "+38(050)475-43-06"));
    return list;
  }


}
