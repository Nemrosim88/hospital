package data.testUtils.dao;

import entity.Patient;

import java.util.ArrayList;
import java.util.List;

public class PatientTestUtils {

  public static Patient getNew() {
    return new Patient(22);
  }

  public static List<Patient> getAll() {
    List<Patient> list = new ArrayList<>();
    list.add(new Patient(1, 6,false));
    list.add(new Patient(2, 7,false));
    list.add(new Patient(3, 10,false));
    list.add(new Patient(4, 11,false));
    list.add(new Patient(5, 12,false));
    list.add(new Patient(6, 19,false));

    // Пользователь 22 имеет роль PATIENT но нет записи в таблице "patient"
    //    Запись будет добавлена в тестах методом getNew

    return list;
  }

}
