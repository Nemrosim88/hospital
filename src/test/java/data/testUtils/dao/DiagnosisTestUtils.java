package data.testUtils.dao;

import entity.Diagnosis;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DiagnosisTestUtils {


  public static Diagnosis getNew() {
    return new Diagnosis(5, 5, "Поступил для ежегодного мед осмотра. Зачем?",
        Timestamp.valueOf("2017-09-29 10:15:13"), Timestamp.valueOf("2017-09-29 10:20:37"),
        "Здоров. Показали предыдущие результаты");
  }

  public static List<Diagnosis> getAll() {
    List<Diagnosis> list = new ArrayList<>();

    list.add(
        new Diagnosis(1, 5, 5, "Поступил для ежегодного мед осмотра",
            Timestamp.valueOf("2015-09-13 09:20:15"), Timestamp.valueOf("2015-09-15 15:11:18"),
            "Здоров"));
    list.add(
        new Diagnosis(2, 5, 5, "Поступил для ежегодного мед осмотра",
            Timestamp.valueOf("2016-09-16 09:40:10"), Timestamp.valueOf("2016-09-17 17:14:19"),
            "Здоров"));
    list.add(
        new Diagnosis(3, 5, 5, "Поступил для ежегодного мед осмотра",
            Timestamp.valueOf("2017-09-15 09:33:21"), Timestamp.valueOf("2017-09-16 12:13:18"),
            "Здоров"));

    return list;
  }

}
