package data.testUtils.dao;

import entity.Staff;
import entity.enums.Position;
import java.util.ArrayList;
import java.util.List;

public class StaffTestUtils {

  public static Staff getNew() {
    return new Staff(13, Position.OPERATOR);
  }

  public static List<Staff> getAll() {
    List<Staff> list = new ArrayList<>();
    list.add(new Staff(1, 2, Position.OPERATOR));
    list.add(new Staff(2, 3, Position.OPERATOR));
    list.add(new Staff(3, 13, Position.NURSE));
    list.add(new Staff(4, 14, Position.NURSE));
    list.add(new Staff(5, 16, Position.OPERATOR));
    list.add(new Staff(6, 18, Position.NURSE));
    list.add(new Staff(7, 20, Position.NURSE));
    list.add(new Staff(8, 21, Position.NURSE));
    return list;
  }

}
