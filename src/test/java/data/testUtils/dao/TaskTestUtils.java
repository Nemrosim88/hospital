package data.testUtils.dao;

import entity.Task;
import entity.enums.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class TaskTestUtils {

  public static Task getNew() {
    return new Task(4, 5, "Удаление правого лёгкого", Timestamp.valueOf("2017-09-17 09:15:22")
        , Type.OPERATION);
  }

  public static List<Task> getAll() {
    List<Task> list = new ArrayList<>();
    list.add(new Task(1, 2, 3, "Магнитно-резонансная томография мозга", Timestamp.valueOf("2017-09-16 10:35:45"),
        Type.PROCEDURE));
    list.add(new Task(2, 3, 1, "Компьютерная томография", Timestamp.valueOf("2017-09-16 10:35:45"),
        Type.PROCEDURE));
    list.add(new Task(3, 1, 2, "Позитронно-эмиссионная томография", Timestamp.valueOf("2017-09-16 10:35:45"),
        Type.PROCEDURE));
    list.add(new Task(4, 4, 6, "Электрокардиография", Timestamp.valueOf("2017-09-16 10:35:45"),
        Type.PROCEDURE));
    list.add(new Task(5, 5, 4, "Эзофагоманометрия", Timestamp.valueOf("2017-09-16 10:35:45"),
        Type.PROCEDURE));
    list.add(new Task(6, 4, 5, "Флюорография органов грудной клетки", Timestamp.valueOf
        ("2017-09-16 10:35:45"),
        Type.PROCEDURE));

    return list;
  }

}
