package data.testUtils.dao;

import entity.Doctor;

import java.util.ArrayList;
import java.util.List;

public class DoctorTestUtils {

  public static Doctor getNew() {
    return new Doctor(6, 17, 823);
  }

  public static List<Doctor> getAll() {
    List<Doctor> list = new ArrayList<>();
    list.add(new Doctor(1, 1, 345));
    list.add(new Doctor(2, 4, 654));
    list.add(new Doctor(3, 5, 194));
    list.add(new Doctor(4, 8, 945));
    list.add(new Doctor(5, 15, 34593));

    // Пользователь 17 имеет роль DOCTOR но нет записи в таблице "doctor"
    //    Запись будет добавлена в тестах методом getNew
    //    list.add(new Doctor(6, 17, 823));

    return list;
  }

}
