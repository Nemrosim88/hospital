package data.testUtils.dataBaseCreateAndFill;

import java.sql.Statement;

public interface DataBase {

  public void createAndFill(Statement statement);

}
