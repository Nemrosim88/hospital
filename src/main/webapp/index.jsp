<%@page contentType="text/html" pageEncoding="utf-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="properties" class="java.util.Properties" scope="session"/>
<c:set var="context" value="${pageContext.request.contextPath}"/>


<!DOCTYPE html>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<html lang="${properties.language}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Hospital page">
    <meta name="author" content="Nemrosim">

    <title>${context}</title>

    <link rel="icon" href="${context}/img/logo_min.gif">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
          crossorigin="anonymous">
    <!-- Custom styles for this -->
    <link href="${context}/css/carousel.css" rel="stylesheet">
    <%--<link href="${pageContext.request.contextPath}/css/mdb.css" rel="stylesheet">--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>


<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">

    <jsp:include page="${context}/jsp/includes/index/nav.jsp">
        <jsp:param name="this_page" value="index"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
        <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
    </jsp:include>

</nav>


<div class="container">

    <%--Карусель с тремя фотографиями--%>
    <c:import url='${context}/jsp/includes/index/carousel.jsp'/>

    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

        <!-- Three columns of text below the carousel -->
        <c:import url='${context}/jsp/includes/index/doctors.jsp'/>


        <!-- START THE FEATURETTES -->

        <hr id="rooms" class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Тут должна быть информация о комнатах. <span
                        class="text-muted">It'll blow your mind.</span></h2>
                <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                    euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                    tellus ac cursus commodo.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Галерея</a></p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto"
                     src="${context}/img/rooms_500x500.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr id="operation" class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7 order-md-2">
                <h2 class="featurette-heading">Oh yeah, it's that good. <span
                        class="text-muted">See for yourself.</span></h2>
                <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                    euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                    tellus ac cursus commodo.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Галерея</a></p>
            </div>
            <div class="col-md-5 order-md-1">
                <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
                     alt="Generic placeholder image">
            </div>
        </div>


        <hr id="equipment" class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
                <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                    euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                    tellus ac cursus commodo.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Галерея</a></p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
                     alt="Generic placeholder image">
            </div>
        </div>
    </div><!-- /.container -->
</div>

<c:import url="${context}/jsp/includes/footer.jsp"/>

</body>
</html>



