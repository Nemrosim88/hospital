Если перенести все tld в эту папку - получит NULLPOINTER

Решение:
Alright I figured out the problem.
There is an attribute called com.sun.jsp.taglibraryCache in applicationScope
that is Class ConcurrentHashMap.
It seems that importing another tag library causes the toString
method of the class to give a NullPointerException.

I fixed my problem by using c:catch to catch the exception


Решение (ДРУГОЕ) было найдено:
Поместить все теги в один файл + добавить в web.xml jsp-config

    <jsp-config>
        <taglib>
            <taglib-uri>myTagLib</taglib-uri>
            <taglib-location>/WEB-INF/myTags/hospitalTags.tld</taglib-location>
        </taglib>
    </jsp-config>

    Всё работает.