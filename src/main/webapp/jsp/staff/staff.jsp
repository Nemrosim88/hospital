<%--@elvariable id="staff" type="entity.Staff"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="${properties.language}">
<head>
    <c:import url="${context}/jsp/includes/staff/head.jsp"/>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${context}/jsp/includes/staff/nav.jsp">
        <jsp:param name="this_page" value="staff"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
        <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
    </jsp:include>
</nav>

<div class="container text-center">

    <h1 class="blog-title">Главная страница мед персонала</h1>

    <div class="card-deck">

        <div class="card text-center">

            <div class="card-body">
                <h4 class="card-title">Задачи</h4>

                    <img class="rounded mx-auto d-block"
                         src="https://png.icons8.com/calendar/office/80"
                         title="Calendar" width="96" height="96">

                <p class="card-text">Все задачи мед персонала для: ${staff.position}</p>

                <form action="controller" method="post">
                    <input type="hidden" name="command" value="viewTasks">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">
                        ${properties.view_tasks}
                    </button>
                </form>
            </div>

        </div>

        <div class="card text-center">
            <div class="card-body">
                <h4 class="card-title">Что-то ещё</h4>
                <a class="navbar-brand"
                   href="${context}/jsp/patient/diagnosis.jsp">
                    <img class="rounded mx-auto d-block"
                         src="https://png.icons8.com/heart-with-pulse-filled/office/80"
                         title="Heart with Pulse Filled" width="96" height="96">
                </a>
                <p class="card-text">Возможен новый функционал</p>
            </div>
            <div class="card-footer">
                <small class="text-muted">В планах</small>
            </div>
        </div>

    </div>

</div>


<c:import url="${context}/jsp/includes/footer.jsp"/>

</body>
</html>
