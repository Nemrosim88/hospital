<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="entity.User" %>

<%--@elvariable id="properties" type="java.util.Properties"--%>

<!DOCTYPE html>
<html lang="${properties.language}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Nemrosim">
    <link rel="icon" href="${context}/img/logo.gif">

    <title>${properties.tasks}</title>

    <link href="${context}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${context}/css/dashboard.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${context}/jsp/includes/staff/tasks/nav.jsp">
        <jsp:param name="this_page" value="staffTasks"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
        <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
    </jsp:include>
</nav>


<div class="container text-center">

    <div class="container">
        <hr class="featurette-divider"/>
        <h3 class="blog-title">ЗАДАЧИ</h3>
        <%--@elvariable id="message" type="java.lang.String"--%>
        <h4 class="blog-title">${message}</h4>
    </div>

    <div class="card-deck">

        <%--@elvariable id="staffTaskList" type="java.util.List"--%>
        <c:forEach items="${staffTaskList}" var="task">

            <%--@elvariable id="task" type="entity.Task"--%>
            <c:set var="user" value="${task.patient.user}"/>
            <%--@elvariable id="task" type="entity.Task"--%>
            <c:set var="doctor" value="${task.doctor.user}"/>

            <div class="card">

                <div class="card-body">
                    <img src="https://png.icons8.com/system-task/office/80"
                         title="System Task" width="80" height="80">
                        <%--@elvariable id="user" type="entity.User"--%>
                    <h5 class="card-title">${user.surname}</h5>
                    <p class="card-text">${user.name}&ensp;${user.patronymic}</p>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${task.info}</li>
                        <li class="list-group-item text-primary"><b>${properties.doctor}:</b>
                        </li>
                        <li class="list-group-item">${doctor.surname}&ensp;${doctor.name}</li>
                    </ul>


                    <form action="controller" method="post">
                        <input type="hidden" name="command" value="addResult">
                        <input type="hidden" name="patientId" value="${task.patient.id}">
                        <input type="hidden" name="taskId" value="${task.id}">
                            <%--@elvariable id="staff" type="entity.Staff"--%>
                        <input type="hidden" name="staffId" value="${staff.id}">
                        <div class="form-group">
                            <input type="datetime-local" class="form-control"
                                   name="dateOfProcedure"
                                   placeholder="03.04.2017" required>
                        </div>
                        <div class="form-group">
                                <%--<label for="info">${properties.result_info}</label>--%>
                            <textarea class="form-control" id="info" rows="3" cols="5"
                                      name="resultInfo" required></textarea>
                        </div>
                        <button class="btn btn-primary btn-lg btn-block">
                                ${properties.add_new_result_to_patient}
                        </button>
                    </form>
                </div>

                <fmt:formatDate value="${task.datetime}" var="formattedDate"
                                type="date" pattern="yyyy.MM.dd HH:mm"/>
                <div class="card-footer">
                    <small class="text-muted">
                        <b>${properties.task_date}:</b>
                            ${formattedDate}
                    </small>
                </div>

            </div>

        </c:forEach>

    </div>


</div>


<c:import url="${context}/jsp/includes/footer.jsp"/>

</body>
</html>
