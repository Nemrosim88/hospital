
<%--@elvariable id="cssProperties" type="java.util.Properties"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ page import="entity.Task" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="entity.enums.Type" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>

<html>

<head>
    <c:import url="${context}/jsp/includes/doctorTasks/head.jsp"/>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${context}/jsp/includes/doctorTasks/nav.jsp">
        <jsp:param name="this_page" value="doctorTasks"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
    </jsp:include>
</nav>

<hr class="featurette-divider">

<%--============================================================================================--%>
<%--=======================   Таблица данных о задачах типа "Операция"   =======================--%>
<%--============================================================================================--%>

<div class="container">

    <label>
        <%--@elvariable id="message" type="java.lang.String"--%>
        ${not empty message? message:''}
    </label>

    <%--@elvariable id="taskList" type="java.util.List"--%>
    <c:forEach items="${taskList}" var="task">

        <table class="${cssProperties.table_default}">
            <thead>
            <tr class="table-primary text-center">
                <th class="text-center" colspan="3">${properties.doctor}</th>
                <th class="text-center" colspan="3">${properties.patient}</th>
                <th class="text-center" colspan="3">${properties.task}</th>
            </tr>
            <tr>
                <th>${properties.surname}</th>
                <th>${properties.name}</th>
                <th>${properties.patronymic}</th>

                <th>${properties.surname}</th>
                <th>${properties.name}</th>
                <th>${properties.patronymic}</th>

                <th>${properties.task_date}</th>
                <th>${properties.type}</th>
                <th>${properties.info}</th>

            </tr>
            </thead>
            <tbody>

            <tr>
                <th scope="row">${task.doctor.user.surname}</th>
                <td>${task.doctor.user.name}</td>
                <td>${task.doctor.user.patronymic}</td>
                <td>${task.patient.user.surname}</td>
                <td>${task.patient.user.name}</td>
                <td>${task.patient.user.patronymic}</td>

                <fmt:formatDate value="${task.datetime}" var="formattedDate"
                                type="date" pattern="yyyy.MM.dd HH:mm"/>
                <td>${formattedDate}</td>
                <td>${task.type}</td>
                <td>${task.info}</td>
            </tr>

            </tbody>
        </table>

        <form action="controller" method="post">
            <input type="hidden" name="command" value="addResultAndTaskToPatient">
            <input type="hidden" name="patientId" value="${task.patient.id}">
                <%--@elvariable id="doctor" type="entity.Doctor"--%>
            <input type="hidden" name="doctorId" value="${doctor.id}">
            <input type="hidden" name="taskId" value="${task.id}">

            <div class="form-group">
                <input type="datetime-local" class="form-control" name="dayOfOperation"
                       placeholder="03.04.2017" required>
            </div>

            <div class="form-group">
                <label for="resultInfo">${properties.result_info}</label>
                <textarea class="form-control" id="resultInfo" rows="3" cols="50"
                          name="resultInfo"></textarea>
            </div>

            <div class="form-group">
                <label for="select">${properties.select_type_of_task}</label>
                <select class="form-control" id="select" name="type">
                        <%--@elvariable id="task" type="entity.Task"--%>
                    <option value="drug" ${task.type == Type.DRUG ? 'selected' : ''}>
                            ${properties.drugs}
                    </option>
                    <option value="procedure" ${task.type == Type.PROCEDURE ? 'selected' : ''}>
                            ${properties.procedure}
                    </option>
                    <option value="operation" ${task.type == Type.OPERATION ? 'selected' : ''}>
                            ${properties.operation}
                    </option>
                </select>
            </div>

            <div class="form-group">
                <label for="info">${properties.task_info}</label>
                <textarea class="form-control" id="info" rows="3" cols="50"
                          name="taskInfo"></textarea>
            </div>

            <button type="submit" class="btn btn-primary">
                    ${properties.add_new_task_to_patient}
            </button>
        </form>

        <hr class="featurette-divider">
    </c:forEach>

</div>

<c:import url="${context}/jsp/includes/footer.jsp"/>

</body>
</html>

