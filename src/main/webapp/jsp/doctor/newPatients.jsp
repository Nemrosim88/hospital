<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<%@ page import="entity.enums.Type" %>

<!DOCTYPE html>
<html lang="${properties.language}">
<head>
    <c:import url="${context}/jsp/includes/newPatients/head.jsp"/>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${context}/jsp/includes/newPatients/nav.jsp">
        <jsp:param name="this_page" value="newPatients"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
    </jsp:include>
</nav>

<div class="container text-center">

    <h1 class="blog-title">Страница добавления задач новым пациентам</h1>
    <div class="card-deck">

        <%--@elvariable id="newPatientsList" type="java.util.List"--%>
        <c:forEach items="${newPatientsList}" var="patient">
            <div class="card text-center">
                <form action="controller" method="post">
                    <input type="hidden" name="command" value="addTaskToPatient">
                    <input type="hidden" name="patientId" value="${patient.id}">
                        <%--@elvariable id="doctor" type="entity.Doctor"--%>
                    <input type="hidden" name="doctorId" value="${doctor.id}">
                    <div class="card-body">
                            <%--@elvariable id="patient" type="entity.Patient"--%>
                        <h4 class="card-title">${patient.user.surname}</h4>
                        <p class="card-text">
                                ${patient.user.name}&ensp;${patient.user.patronymic}</p>
                        <p class="card-text"><b>${properties.day_of_birth}:</b>&ensp;
                                ${patient.user.dayOfBirth}</p>
                        <p class="card-text"><b>${properties.passport}:</b>&ensp;
                                ${patient.user.credentials.passport}</p>
                        <div class="form-group">
                            <label for="select">Select type of task</label>
                            <select class="form-control" id="select" name="type">
                                    <%--@elvariable id="task" type="entity.Task"--%>
                                <option value="drug" ${task.type == Type.DRUG ? 'selected' : ''}>
                                        ${properties.drugs}
                                </option>
                                <option value="procedure" ${task.type == Type.PROCEDURE ? 'selected' : ''}>
                                        ${properties.procedure}
                                </option>
                                <option value="operation" ${task.type == Type.OPERATION ? 'selected' : ''}>
                                        ${properties.operation}
                                </option>
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="info">${properties.task_info}</label>
                            <textarea class="form-control" id="info" rows="3" cols="50"
                                      name="info"></textarea>
                        </div>

                        <button type="submit"
                                class="btn btn-primary">
                                ${properties.add_new_task_to_patient}
                        </button>

                    </div>
                </form>
            </div>


        </c:forEach>

    </div>
</div>


<c:import url="${context}/jsp/includes/footer.jsp"/>

</body>
</html>

