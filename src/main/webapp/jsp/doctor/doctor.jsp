<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<%--@elvariable id="properties" type="java.util.Properties"--%>


<!DOCTYPE html>
<html lang="${properties.language}">
<head>
    <c:import url="${context}/jsp/includes/doctor/doctor/head.jsp"/>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${context}/jsp/includes/doctor/doctor/nav.jsp">
        <jsp:param name="this_page" value="doctor"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
        <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
    </jsp:include>
</nav>


<div class="container-fluid">
    <hr class="featurette-divider">

    <%@ taglib uri="../../WEB-INF/myTags/hospitalTags.tld" prefix="mytag" %>
    <%--@elvariable id="doctor" type="entity.Doctor"--%>
    <mytag:PatientMinInfo doctorId="${doctor.id}" divClass="col-md-4"/>

</div>


<c:import url="${context}/jsp/includes/footer.jsp"/>

</body>
</html>





