<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="context" value="${pageContext.request.contextPath}"/>

<jsp:useBean id="properties" scope="session" type="java.util.Properties"/>

<%@ page import="entity.Doctor" %>
<%@ page import="entity.enums.Type" %>

<!DOCTYPE html>
<html lang="${properties.language}">
<head>
    <jsp:include page="${context}/jsp/includes/doctor/patientInfo/head.jsp">
        <jsp:param name="context" value="${context}"/>
        <jsp:param name="author" value="Nemrosim"/>
    </jsp:include>
</head>
<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${context}/jsp/includes/doctor/patientInfo/nav.jsp">
        <jsp:param name="this_page" value="patientInfo"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
        <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
    </jsp:include>
</nav>

<hr class="featurette-divider">
<%--@elvariable id="message" type="java.lang.String"--%>
<h5>${not empty message ? message : ''}</h5>

<%--============================ Таблица данных о пользователе =============================--%>
<div class="container-fluid">
    <c:import url="${context}/jsp/includes/doctor/patientInfo/patientInfoTable.jsp"/>
</div>

<%--============================ Таблица задач и результатов ===============================--%>
<div class="container-fluid">
    <c:import url="${context}/jsp/includes/doctor/patientInfo/patientTaskAndResult.jsp"/>
</div>

<hr class="featurette-divider">
<%--============================  Выписать пациента, указав окончательный диагноз ==========--%>

<div class="container-fluid">
    <%--@elvariable id="cssProperties" type="java.util.Properties"--%>
    <table class="${cssProperties.table_default}">
        <thead>
        <tr class="table-primary text-center">
            <th class="text-center" colspan="4">${properties.doctor}</th>
            <th class="text-center" colspan="3">${properties.final_diagnosis}</th>
        </tr>
        <tr>
            <th>${properties.surname}</th>
            <th>${properties.name}</th>
            <th>${properties.patronymic}</th>
            <th>${properties.phone_number}</th>

            <th>${properties.info}</th>
            <th>${properties.task_date}</th>
            <th>${properties.final_diagnosis}</th>
        </tr>
        </thead>
        <tbody>

        <%--@elvariable id="diagnosisList" type="java.util.List"--%>
        <c:forEach items="${diagnosisList}" var="diagnosis">
            <tr>
                <td>${diagnosis.doctor.user.surname}</td>
                <td>${diagnosis.doctor.user.name}</td>
                <td>${diagnosis.doctor.user.patronymic}</td>
                <td>${diagnosis.doctor.user.credentials.phoneNumber}</td>

                <td>${diagnosis.info}</td>
                <td>${diagnosis.end}</td>
                <td>${diagnosis.finalDiagnosis}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <div class="container">
        <div class="row">

            <%--@elvariable id="lastTasksClosed" type="java.lang.Boolean"--%>
            <c:choose>
                <c:when test="${lastTasksClosed}">
                    <div class="col">
                        <h2>${properties.add_new_task_to_patient}</h2>

                        <form action="controller" method="post">
                            <input type="hidden" name="command" value="addTaskToPatient">
                            <input type="hidden" name="patientId" value="${patient.id}">
                                <%--@elvariable id="doctor" type="entity.Doctor"--%>
                            <input type="hidden" name="doctorId" value="${doctor.id}">
                            <div class="card-body">
                                    <%--@elvariable id="patient" type="entity.Patient"--%>
                                <div class="form-group">
                                    <label for="select">Select type of task</label>
                                    <select class="form-control" id="select" name="type">
                                            <%--@elvariable id="task" type="entity.Task"--%>
                                        <option value="drug" ${task.type == Type.DRUG ? 'selected' : ''}>
                                                ${properties.drugs}
                                        </option>
                                        <option value="procedure" ${task.type == Type.PROCEDURE ? 'selected' : ''}>
                                                ${properties.procedure}
                                        </option>
                                        <option value="operation" ${task.type == Type.OPERATION ? 'selected' : ''}>
                                                ${properties.operation}
                                        </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="info">${properties.task_info}</label>
                                    <textarea class="form-control" id="info" rows="3" cols="50"
                                              name="info"></textarea>
                                </div>

                                <button type="submit"
                                        class="btn btn-primary btn-lg btn-block">
                                        ${properties.add_new_task_to_patient}
                                </button>

                            </div>
                        </form>
                    </div>
                </c:when>
                <%--@elvariable id="patient" type="entity.Patient"--%>
                <c:when test="${not patient.discharge}">
                    <div class="col">
                        <h2>${properties.discharge_patient}</h2>
                        <form action="controller" method="post">
                            <input type="hidden" name="command" value="discharge">
                            <input type="hidden" name="patientId" value="${patient.id}">
                            <input type="hidden" name="doctorId" value="${doctor.id}">
                                <%--Поле для дополнительной информации--%>
                            <div class="form-group">
                                <label for="additionalInfo">${properties.info}</label>
                                <textarea class="form-control" id="additionalInfo" rows="3"
                                          cols="50"
                                          name="additionalInfo" required></textarea>
                            </div>
                                <%--Поле для окончательного диагноза--%>
                            <div class="form-group">
                                <label for="diagnosis">${properties.final_diagnosis}</label>
                                <textarea class="form-control" id="diagnosis" rows="3" cols="50"
                                          name="diagnosis" required></textarea>
                            </div>

                            <button type="submit" class="btn btn-primary btn-lg btn-block">
                                    ${properties.discharge}
                            </button>
                        </form>
                    </div>
                </c:when>
            </c:choose>
        </div>
    </div>

    <div class="container text-center">
        <h3>

            ${lastTasksClosed
                    ? ''
                    : 'Вы не можете установить диагноз, пока не завершены все процедуры'}
        </h3>
    </div>
</div>

<c:import url="${context}/jsp/includes/footer.jsp"/>
</body>
</html>
