<%--@elvariable id="properties" type="java.util.Properties"--%>
<%--@elvariable id="cssProperties" type="java.util.Properties"--%>
<%@ page import="entity.Task" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>

<!DOCTYPE html>
<html lang="${properties.language}">
<head>
    <c:import url="${context}/jsp/includes/allPatientsInfo/head.jsp"/>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${context}/jsp/includes/allPatientsInfo/nav.jsp">
        <jsp:param name="this_page" value="doctor"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
    </jsp:include>
</nav>

<hr class="featurette-divider">
<%--@elvariable id="taskList" type="java.util.List"--%>
<c:forEach items="${taskList}" var="task">
    <c:set var="image_size" value="20"/>
    <hr class="featurette-divider">
    <div class="container-fluid">

            <%-------------------------------ПАЦИЕНТ--------------------------------%>
        <table class="${cssProperties.table_default}">
            <thead class="thead-inverse">
            <tr class="table-primary text-center">
                <th class="text-center" colspan="9"><h5>${properties.patient_info}</h5></th>
            </tr>
            </thead>

            <tr class="text-center">
                <th class="text-center">${properties.number}</th>
                <th class="text-center">${properties.surname}</th>
                <th class="text-center">${properties.name}</th>
                <th class="text-center">${properties.patronymic}</th>
                <th class="text-center">${properties.day_of_birth}</th>

                <th class="text-center">${properties.discharge}</th>

                <th class="text-center">${properties.passport}</th>
                <th class="text-center">${properties.email}</th>
                <th class="text-center">${properties.phone_number}</th>
            </tr>

            <tbody>
            <tr class="text-center">
                <th scope="row" class="text-center">${task.patient.id}</th>
                <td>${task.patient.user.surname}</td>
                <td>${task.patient.user.name}</td>
                <td>${task.patient.user.patronymic}</td>
                <td>${task.patient.user.dayOfBirth}</td>

                <c:choose>
                    <c:when test="${task.patient.discharge}">
                        <td>
                            <img src="https://png.icons8.com/approval/color/24" title="OK"
                                 width="${image_size}"
                                 height="${image_size}">
                            Выписан
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td>
                            <img src="https://png.icons8.com/cancel/color/24" title="NOT"
                                 width="${image_size}"
                                 height="${image_size}">
                            Не выписан
                        </td>
                    </c:otherwise>
                </c:choose>

                <td>${task.patient.user.credentials.passport}</td>
                <td>${task.patient.user.credentials.email}</td>
                <td>${task.patient.user.credentials.phoneNumber}</td>

            </tr>
            </tbody>
        </table>
    </div>


    <div class="container-fluid">
        <table class="table table-bordered">
                <%-------------------------------Задачи и результаты--------------------------------%>
            <thead>
            <tr class="table-primary text-center">
                <th class="text-center" colspan="4"><h6>${properties.task_number}</h6></th>
                <th class="text-center" colspan="3"><h6>${properties.result}</h6></th>
                <th class="text-center" colspan="4"><h6>${properties.executor}</h6></th>
            </tr>

            <tr class="table-success">
                <th class="text-center">${properties.task_number}</th>
                <th class="text-center">${properties.info}</th>

                <th class="text-center">${properties.task_date}</th>
                <th class="text-center">${properties.type}</th>

                <th class="text-center">${properties.result_number}</th>
                <th class="text-center">${properties.result_date}</th>
                <th class="text-center">${properties.info}</th>

                <th class="text-center">${properties.position}</th>
                <th class="text-center">${properties.surname}</th>
                <th class="text-center">${properties.name}</th>
                <th class="text-center">${properties.phone_number}</th>
            </tr>
            </thead>

            <tbody>
            <tr class="text-center">
                <th scope="row" class="text-center">
                        ${task.id}</th>
                <td>${task.info}</td>


                <fmt:formatDate value="${task.datetime}" var="formattedDate"
                                type="date" pattern="yyyy.MM.dd HH:mm"/>
                <td>${formattedDate}</td>
                <td>${task.type}</td>

                <c:choose>
                    <c:when test="${not empty task.result.id}">
                        <td>
                            <img src="https://png.icons8.com/approval/color/24" title="OK"
                                 width="${image_size}"
                                 height="${image_size}">
                                ${task.result.id}
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td>
                            <img src="https://png.icons8.com/cancel/color/24" title="NOT"
                                 width="${image_size}"
                                 height="${image_size}">
                                ${task.result.id}
                        </td>
                    </c:otherwise>
                </c:choose>

                <td>${not empty task.result.datetime ? task.result.datetime : ''}</td>
                <td>${not empty task.result.info ? task.result.info : ''}</td>



                <c:choose>
                    <c:when test="${not empty task.result.doctor}">
                        <td>DOCTOR</td>
                        <td>${not empty task.result.doctor.user.surname ?
                        task.result.doctor.user.surname : ''}
                        </td>
                        <td>${not empty task.result.doctor.user.name ?
                        task.result.doctor.user.name : ''}
                        </td>
                        <td>${not empty task.result.doctor.user.credentials.phoneNumber ?
                        task.result.doctor.user.credentials.phoneNumber : ''}
                        </td>
                    </c:when>
                    <c:otherwise>
                        <td>${not empty task.result.staff.position ? task.result.staff.position :
                         ''} </td>
                        <td>${not empty task.result.staff.user.surname ?
                        task.result.staff.user.surname : ''}</td>
                        <td>${not empty task.result.staff.user.name ? task.result.staff.user.name
                         : ''}</td>
                        <td>${not empty task.result.staff.user.credentials.phoneNumber ?
                        task.result.staff.user.credentials.phoneNumber : ''}</td>
                    </c:otherwise>
                </c:choose>

            </tr>

            </tbody>
        </table>
    </div>
</c:forEach>


<c:import url="${context}/jsp/includes/footer.jsp"/>

</body>
</html>

