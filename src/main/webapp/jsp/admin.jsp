<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>


<!DOCTYPE html>
<html lang="${properties.language}">

<head>
    <c:import url="${context}/jsp/includes/admin/head.jsp"/>
</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${context}/jsp/includes/admin/nav.jsp">
        <jsp:param name="this_page" value="admin"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
        <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
    </jsp:include>
</nav>

<div class="container text-center">

    <h1 class="blog-title">Главная страница администратора</h1>

    <div class="card-deck">

        <div class="card text-center">

            <div class="card-body">
                <h4 class="card-title">Добавить нового пациента</h4>
                <a class="navbar-brand"
                   href="${context}/jsp/addPatient.jsp">
                    <img class="rounded mx-auto d-block" src="https://png.icons8.com/recovery/color/96"
                         title="Recovery" width="96" height="96">
                </a>
                <p class="card-text">Нажмите на иконку для перехода на страницу добавления
                    нового пациента больницы</p>
            </div>
        </div>

        <div class="card text-center">
            <div class="card-body">
                <h4 class="card-title">Добавить нового врача</h4>
                <a class="navbar-brand"
                   href="${context}/jsp/addDoctor.jsp">
                    <img class="rounded mx-auto d-block"
                         src="https://png.icons8.com/medical-doctor/color/96"
                         title="Medical Doctor" width="96" height="96">
                </a>
                <p class="card-text">Нажмите на иконку для перехода на страницу добавления
                    нового врача больницы</p>
            </div>
        </div>

        <div class="card text-center">
            <div class="card-body">
                <h4 class="card-title">Добавить нового администратора</h4>
                <a class="navbar-brand"
                   href="${context}/jsp/addAdmin.jsp">
                    <img class="rounded mx-auto d-block" src="https://png.icons8.com/admin/office/80"
                         title="Admin" width="96" height="96">
                </a>
                <p class="card-text">Нажмите на иконку для перехода на страницу добавления
                    нового администратора больницы</p>
            </div>
        </div>

        <div class="card text-center">
            <div class="card-body">
                <h4 class="card-title">Добавить нового сотрудника</h4>
                <a class="navbar-brand"
                   href="${context}/jsp/addStaff.jsp">
                    <img class="rounded mx-auto d-block" src="https://png.icons8.com/staff/color/96"
                         title="Staff" width="96" height="96">
                </a>
                <p class="card-text">Нажмите на иконку для перехода на страницу добавления
                    нового сотрудника мед персонала больницы</p>
            </div>
        </div>
    </div>
</div>

<c:import url="${context}/jsp/includes/footer.jsp"/>
</body>
</html>



