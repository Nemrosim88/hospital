<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<!DOCTYPE html>
<html lang="${properties.language}">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Nemrosim">
    <link rel="icon" href="${pageContext.request.contextPath}/img/logo.gif">

    <title>Add doctor</title>

    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/css/dashboard.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


</head>

<body>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <jsp:include page="${pageContext.request.contextPath}/jsp/includes/admin/nav.jsp">
        <jsp:param name="this_page" value="addDoctor"/>
        <jsp:param name="image_size" value="30"/>
        <jsp:param name="command" value="getProperties"/>
        <jsp:param name="form_class" value="form-inline mt-2 mt-md-0"/>
    </jsp:include>
</nav>

<div class="container text-center">

    <h1 class="blog-title">Страница для добавления нового врача</h1>

    <div class="container-fluid">
        <jsp:include page="${pageContext.request.contextPath}/jsp/includes/admin/addDoctor.jsp">
            <jsp:param name="command" value="addDoctor"/>
        </jsp:include>
    </div>


</div>


<c:import url="${pageContext.request.contextPath}/jsp/includes/footer.jsp"/>

</body>
</html>
