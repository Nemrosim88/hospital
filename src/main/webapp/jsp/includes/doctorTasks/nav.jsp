<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>

<button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse"
        data-target="#navbarsExampleDefault"
        aria-controls="navbarsExampleDefault" aria-expanded="false"
        aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="navbar-brand" href="${context}/index.jsp">
                <img src="https://png.icons8.com/real-estate/dusk/64" title="${properties.home}"
                     width="${param.image_size}" height="${param.image_size}">
                ${properties.home}
            </a>
        </li>

        <%--*********************** Doctor main page *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${context}/jsp/doctor/doctor.jsp">
                <img src="https://png.icons8.com/medical-doctor/office/40"
                     title="${properties.doctor_page_name}"
                     width="${param.image_size}" height="${param.image_size}">
                <%--@elvariable id="properties" type="java.util.Properties"--%>
                ${properties.doctor_page_name}
            </a>
        </li>

        <%--*********************** All patients info *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${context}/jsp/doctor/controller?command=allPatientsInfo">
                <img src="https://png.icons8.com/calendar/office/40"
                     title="${properties.all_patients_info_page_name}"
                     width="${param.image_size}" height="${param.image_size}">
                ${properties.all_patients_info_page_name}
            </a>
        </li>

        <%--*********************** New patients *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand"
               href="${context}/jsp/doctor/controller?command=newPatients">
                <img src="https://png.icons8.com/add-user-male/office/40"
                     title="${properties.new_patients}"
                     width="${param.image_size}" height="${param.image_size}">
                ${properties.new_patients}
            </a>
        </li>
    </ul>

    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="us">
        <input type="image" src="https://png.icons8.com/usa/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="ru">
        <input type="image" src="https://png.icons8.com/russian-federation/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="uk">
        <input type="image" src="https://png.icons8.com/ukraine/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="fr">
        <input type="image" src="https://png.icons8.com/france/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>

    <c:import url="${context}/jsp/includes/logoutForm.jsp"/>
</div>
