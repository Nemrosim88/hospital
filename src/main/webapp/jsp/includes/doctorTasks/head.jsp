<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="${context}/img/logo.gif">

<title>${context}</title>

<link href="${context}/css/bootstrap.min.css" rel="stylesheet">

<link href="${context}/css/dashboard.css" rel="stylesheet">
<%--<link href="${pageContext.request.contextPath}/css/mdb.css" rel="stylesheet">--%>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
