<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>
<div class="container">
    <hr class="featurette-divider">
    <footer>

        <%@ page import="java.util.GregorianCalendar, java.util.Calendar" %>
        <%
            GregorianCalendar currentDate = new GregorianCalendar();
            int currentYear = currentDate.get(Calendar.YEAR);
            HttpSession clientSession = request.getSession();
        %>
        <p>&copy; Copyright <%= currentYear %> sesssion <%= clientSession.getId()%>  Nemrosim &amp; Associates</p>
        <p class="float-right"><a href="#">Back to top</a></p>
        <p>&copy; 2017 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
    </footer>
</div>

<script src="${context}/js/jquery-3.2.1.min.js"></script>
<script>window.jQuery || document.write('<script src="../../js/jquery-3.2.1.min.js"><\/script>')</script>
<script src="${context}/js/popper/popper.min.js"></script>
<script src="${context}/js/bootstrap.min.js"></script>
<script src="${context}/js/mdb.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="${context}/js/holder/holder.js"></script>
