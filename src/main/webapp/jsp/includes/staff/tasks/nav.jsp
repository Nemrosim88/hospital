<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="context" value="${pageContext.request.contextPath}"/>


<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
        <%--*********************** HOSPITAL *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${context}/index.jsp">
                <img src="https://png.icons8.com/hospital/office/40"
                     title="${properties.home}"
                     width="${param.image_size}" height="${param.image_size}">
                ${properties.home}
            </a>
        </li>
        <%--*********************** Tasks *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${context}/jsp/staff/controller?command=viewTasks">
                <img src="https://png.icons8.com/system-task/office/40"
                     title="${properties.tasks}"
                     width="${param.image_size}" height="${param.image_size}">
                ${properties.tasks}
            </a>
        </li>
    </ul>

    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="us">
        <input type="image" src="https://png.icons8.com/usa/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="ru">
        <input type="image" src="https://png.icons8.com/russian-federation/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="uk">
        <input type="image" src="https://png.icons8.com/ukraine/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="fr">
        <input type="image" src="https://png.icons8.com/france/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>

    <c:import url="${context}/jsp/includes/logoutForm.jsp"/>
</div>
