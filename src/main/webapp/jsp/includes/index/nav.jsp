<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="context" value="${pageContext.request.contextPath}"/>

<c:set var="doctorPage" value="${context}/jsp/doctor/doctor.jsp"/>
<c:set var="adminPage" value="${context}/jsp/admin.jsp"/>
<c:set var="patientPage" value="${context}/jsp/patient.jsp"/>
<c:set var="staffPage" value="${context}/jsp/staff/staff.jsp"/>

<%--@elvariable id="properties" type="java.util.Properties"--%>
<%--@elvariable id="logged" type="entity.enums.Role"--%>
<%@ page import="entity.enums.Role" %>


<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarCollapse">

    <ul class="navbar-nav mr-auto">
        <li>
            <a class="navbar-brand" href="#">
                <img src="https://png.icons8.com/hospital/office/40"
                     title="${properties.home}"
                     width="${param.image_size}"
                     height="${param.image_size}">
                ${properties.home}
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link ${logged == Role.ADMIN ? 'active': 'disabled'}"
               href="${logged == Role.ADMIN ? adminPage : '#'}">

                <img src="https://png.icons8.com/admin/office/40"
                     title="${properties.admin_page_name}"
                     width="${param.image_size}" height="${param.image_size}">
                ${properties.admin_page_name}
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link ${logged == Role.DOCTOR ? 'active': 'disabled'}"
               href="${logged == Role.DOCTOR ? doctorPage : '#'}">

                <img src="https://png.icons8.com/medical-doctor/color/48"
                     title="${properties.doctor_page_name}"
                     width="${param.image_size}" height="${param.image_size}">

                ${properties.doctor_page_name}
            </a>
        </li>

        <li class="nav-item">


            <a class="nav-link ${logged == Role.PATIENT ? 'active': 'disabled'}"
               href="${logged == Role.PATIENT ? patientPage : '#'}">

                <img src="https://png.icons8.com/pill/office/40"
                     title="${properties.patient_page_name}"
                     width="${param.image_size}" height="${param.image_size}">

                <%--@elvariable id="properties" type="java.util.Properties"--%>
                ${properties.patient_page_name}
            </a>
        </li>

        <li class="nav-item">


            <a class="nav-link ${logged == Role.STAFF ? 'active': 'disabled'}"
               href="${logged == Role.STAFF ? staffPage : '#'}">

                <img src="https://png.icons8.com/staff/color/48"
                     title="${properties.staff_page_name}"
                     width="${param.image_size}" height="${param.image_size}">

                <%--@elvariable id="properties" type="java.util.Properties"--%>
                ${properties.staff_page_name}
            </a>
        </li>

    </ul>

    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="us">
        <input type="image" src="https://png.icons8.com/usa/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="ru">
        <input type="image" src="https://png.icons8.com/russian-federation/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="uk">
        <input type="image" src="https://png.icons8.com/ukraine/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="fr">
        <input type="image" src="https://png.icons8.com/france/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>


    <c:choose>
        <%--@elvariable id="user" type="entity.User"--%>
        <c:when test="${user != null}">
            <c:import url="${context}/jsp/includes/logoutForm.jsp"/>
        </c:when>

        <c:otherwise>
            <%--@elvariable id="message" type="java.lang.String"--%>
            <label style="color: red">${message}&ensp;</label>
            <form class="form-inline mt-2 mt-md-0" action="controller" method="post">
                <input type="hidden" name="command" value="login">
                <c:choose>
                    <%--@elvariable id="login" type="java.lang.String"--%>
                    <c:when test="${login != null}">
                        <input type="text"
                               name="login"
                               class="form-control mr-sm-2"
                               style="background-color: #f8d7da"
                               placeholder="${properties.login}"
                               value="${login}"
                               aria-label="Search">
                        <input class="form-control mr-sm-2"
                               type="password"
                               name="password"
                               style="background-color: #f8d7da"
                               placeholder="${properties.password}"
                               aria-label="Password">
                    </c:when>
                    <c:otherwise>
                        <input class="form-control mr-sm-2"
                               type="text"
                               name="login"
                               placeholder="${not empty cookie.cookie_login ?
                               cookie.cookie_login.value:
                                properties.login}"
                               aria-label="Search">
                        <input class="form-control mr-sm-2"
                               type="password"
                               name="password"
                               placeholder="${properties.password}"
                               aria-label="Password">
                    </c:otherwise>
                </c:choose>

                <button class="btn btn-outline-success my-2 my-sm-0"
                        type="submit">${properties.in}</button>
            </form>
        </c:otherwise>
    </c:choose>

</div>
