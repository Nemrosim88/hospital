<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="first-slide" src="${pageContext.request.contextPath}/img/room_modOne.jpg" alt="First slide">
            <div class="container">
                <div class="carousel-caption d-none d-sm-block" style="margin-bottom: 40px">
                    <h1>Лучшие больничные палаты</h1>
                    <p>Уникальные smart-палаты оборудованы системами дистанционного управления,
                        действующими по принципу «умного дома».
                        Палаты сделаны по стандартам высококлассного пятизвездочного отеля с участием
                        профессиональных дизайнеров.</p>
                    <p><a class="btn btn-lg btn-primary" href="#rooms" role="button">Узнать больше</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="second-slide" src="${pageContext.request.contextPath}/img/operation_modTwo.jpg" alt="Second slide">
            <div class="container">
                <div class="carousel-caption d-none d-sm-block" style="margin-bottom: 40px">
                    <h1>Лучшие операционные палаты</h1>
                    <p>Новые помещения включают в себя технологии следующего поколения,
                        адаптированные под медицинские нужды: сенсорные панели, видеокамеры и другое
                        оборудование для сбора данных о пациенте и помощи хирургической бригаде.</p>
                    <p><a class="btn btn-lg btn-primary" href="#operation" role="button">Узнать больше</a></p>
                </div>
            </div>
        </div>
        <div class="carousel-item">
            <img class="third-slide"
                 src="${pageContext.request.contextPath}/img/best-medical-equipment-min.jpg"
                 alt="Third slide">
            <div class="container">
                <div class="carousel-caption d-none d-sm-block" style="margin-bottom: 40px">
                    <h1>Комплексное оснащение лабораторий</h1>
                    <p>Широкий ассортимент нового медицинского оборудования ведущих мировых производителей.
                        За время деятельности накоплен большой опыт работы как с крупными зарубежными
                        и отечественными компаниями, так и с небольшими фирмами и частными предпринимателями.</p>
                    <p><a class="btn btn-lg btn-primary" href="#equipment" role="button">Browse gallery</a></p>
                </div>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
