<div class="row">


    <div class="col-lg-4">
        <img class="rounded-circle"
             src="${pageContext.request.contextPath}/img/doctor_House_min.jpg"
             alt="Generic placeholder image" width="140" height="140">
        <h2>Доктор Грегори Хаус</h2>
        <p>Блестящий диагност со специализацией в двух основных
            областях: заболевания почек (нефрология) и инфекционные болезни.</p>
        <p><a class="btn btn-secondary" href="https://ru.wikipedia.org/wiki/Доктор_Хаус" role="button">Узнать
            больше</a></p>
    </div><!-- /.col-lg-4 -->


    <div class="col-lg-4">
        <img class="rounded-circle"
             src="${pageContext.request.contextPath}/img/dr-patch-adams_min.jpg"
             alt="Generic placeholder image" width="140" height="140">
        <h2>Докто Патч Адамс</h2>
        <p>Врач, общественный деятель, больничный клоун и писатель.
            Более всего известен своей деятельностью в качестве больничного клоуна
            и считается одним из создателей данного явления.</p>
        <p><a class="btn btn-secondary" href="https://ru.wikipedia.org/wiki/Патч_Адамс" role="button">Узнать
            больше</a></p>
    </div><!-- /.col-lg-4 -->


    <div class="col-lg-4">
        <img class="rounded-circle"
             src="${pageContext.request.contextPath}/img/doctor_strange-min.jpg"
             alt="Generic placeholder image" width="140" height="140">
        <h2>Доктор Стивен Стрэндж</h2>
        <p>Блестящий, но эгоистичный хирург. Практик как мистических, так и боевых искусств.</p>
        <p><a class="btn btn-secondary" href="https://ru.wikipedia.org/wiki/Доктор_Стрэндж" role="button">Узнать
            больше</a></p>
    </div><!-- /.col-lg-4 -->
</div>
<!-- /.row -->
