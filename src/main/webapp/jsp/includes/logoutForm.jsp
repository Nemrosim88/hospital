<%--@elvariable id="properties" type="java.util.Properties"--%>
<%--@elvariable id="OKmessage" type="java.lang.String"--%>
<form class="form-inline mt-2 mt-md-0" action="controller" method="post">
	<input type="hidden" name="command" value="logout">
	<%--@elvariable id="user" type="entity.User"--%>
	<label style="color: forestgreen">&ensp;${user.surname}&ensp;</label>
	<label style="color: forestgreen">${user.name.charAt(0)}.</label>
	<label style="color: forestgreen">${user.patronymic.charAt(0)}.&ensp;</label>
	<button class="btn btn-outline-success my-2 my-sm-0" type="submit">${properties.logout}</button>
</form>

