<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>



<button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse"
        data-target="#navbarsExampleDefault"
        aria-controls="navbarsExampleDefault" aria-expanded="false"
        aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarsExampleDefault">
    <ul class="navbar-nav mr-auto">
        <%--*********************** HOSPITAL *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/index.jsp">
                <img src="https://png.icons8.com/hospital/office/40" title="Больница"
                     width="${param.image_size}" height="${param.image_size}">
                ${properties.home}
            </a>
        </li>

        <%--*********************** Add new admin *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/jsp/addAdmin.jsp">
                <img src="https://png.icons8.com/admin/office/40" title="Add new admin"
                     width="${param.image_size}" height="${param.image_size}">
            </a>
        </li>

        <%--*********************** Add new patient *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/jsp/addPatient.jsp">
                <img src="https://png.icons8.com/recovery/color/48" title="Add new patient"
                     width="${param.image_size}" height="${param.image_size}">
            </a>
        </li>



        <%--*********************** Add new doctor *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/jsp/addDoctor.jsp">
                <img src="https://png.icons8.com/medical-doctor/color/48" title="Add new doctor"
                     width="${param.image_size}" height="${param.image_size}">
            </a>
        </li>

        <%--*********************** Add new staff *************************--%>
        <li class="nav-item active">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/jsp/addStaff.jsp">
                <img src="https://png.icons8.com/staff/color/48" title="Add new staff"
                     width="${param.image_size}" height="${param.image_size}">
            </a>
        </li>

    </ul>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="us">
        <input type="image" src="https://png.icons8.com/usa/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="ru">
        <input type="image" src="https://png.icons8.com/russian-federation/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="uk">
        <input type="image" src="https://png.icons8.com/ukraine/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>
    <label>&ensp;</label>
    <form class="${param.form_class}" action="controller" method="get">
        <input type="hidden" name="jspPage" value="${param.this_page}">
        <input type="hidden" name="command" value="${param.command}">
        <input type="hidden" name="language" value="fr">
        <input type="image" src="https://png.icons8.com/france/office/40"
               width="${param.image_size}" height="${param.image_size}"
               alt="RU"/>
    </form>

    <c:import url="${pageContext.request.contextPath}/jsp/includes/logoutForm.jsp"/>


</div>
