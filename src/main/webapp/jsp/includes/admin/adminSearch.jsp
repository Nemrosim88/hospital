<br>

<div class="container">
    <%--Поле поиска по Имени--%>
    <form action="#" method="post">
        <input type="hidden" name="searchBy" value="name">
        <div class="form-group">
            <label for="searchByName">Фамилия</label>
            <input type="text" class="form-control" id="searchByName" placeholder="Петр">
        </div>
        <button type="button" class="btn btn-lg btn-primary">Поиск по имени</button>
    </form>

    <%--Поле поиска по фамилии--%>
    <form action="#" method="post">
        <input type="hidden" name="searchBy" value="patronymic">
        <div class="form-group">
            <label for="searchBySurname">Фамилия</label>
            <input type="text" class="form-control" id="searchBySurname" placeholder="Петренко">
        </div>
        <button type="button" class="btn btn-lg btn-primary">Поиск по фамилии</button>
    </form>


    <%--Поле поиска по Отчеству--%>
    <form action="#" method="post">
        <input type="hidden" name="searchBy" value="patronymic">
        <div class="form-group">
            <label for="searchByPatronymic">Отчество</label>
            <input type="text" class="form-control" id="searchByPatronymic" placeholder="Петрович">
        </div>
        <button type="button" class="btn btn-lg btn-primary">Поиск по отчеству</button>
    </form>


    <%--Поле поиска по почте--%>
    <form action="#" method="post">
        <input type="hidden" name="searchBy" value="email">
        <div class="form-group">
            <label for="searchByEmail">Отчество</label>
            <input type="text" class="form-control" id="searchByEmail" placeholder="Петрович">
        </div>
        <button type="button" class="btn btn-lg btn-primary">Поиск по отчеству</button>
    </form>


</div>

