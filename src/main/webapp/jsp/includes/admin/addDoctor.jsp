<%--@elvariable id="doctor" type="entity.Doctor"--%>
<%--@elvariable id="doctorId" type="java.lang.Integer"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%--@elvariable id="cssProperties" type="java.util.Properties"--%>
<%--@elvariable id="userId" type="java.lang.Integer"--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<label>
    <b>
        ${not empty doctorId ?  'Новому врачу присвоено ID:' : ''}
        &ensp;
        ${not empty doctorId ?  doctorId : ''}
    </b>
</label>
<label>
    <b>
        &ensp;
        ${not empty userId ?  'Новому пользователю присвоено ID:' : ''}
        &ensp;
        ${not empty userId ?  userId : ''}
    </b>
</label>
<label style="color: ${not empty loginMessage ? 'red' : 'black'}">
    <%--@elvariable id="loginMessage" type="java.lang.String"--%>
    ${not empty loginMessage ? loginMessage : ''}
</label>
<label style="color: ${passportMessage != null ? 'red' : 'black'}">
    <%--@elvariable id="passportMessage" type="java.lang.String"--%>
    ${not empty passportMessage ? passportMessage : ''}
</label>
<%-------------------------------ПАЦИЕНТ--------------------------------%>
<form action="controller" method="post">
    <input type="hidden" name="command" value="${param.command}">


    <table class="${cssProperties.table_default}">
        <thead>
        <tr class="table-primary text-center">
            <th class="text-center" colspan="4">
                <h4>${properties.doctor}</h4>
            </th>
        </tr>
        </thead>

        <tr class="text-center">
            <th class="text-center">${properties.surname}</th>
            <th class="text-center">${properties.name}</th>
            <th class="text-center">${properties.patronymic}</th>
            <th class="text-center">${properties.day_of_birth}</th>
        </tr>

        <tbody>
        <tr class="text-center">
            <th scope="row" class="text-center">
                <div class="form-group">
                    <input type="text" class="form-control" name="surname"
                           maxlength="45"
                           placeholder="${properties.placeholder_surname}"
                           value="${doctor.user.surname}" required>
                </div>
            </th>
            <td>
                <div class="form-group">
                    <input type="text" class="form-control" name="name"
                           maxlength="45"
                           placeholder="${properties.placeholder_name}"
                           value="${doctor.user.name}" required>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input type="text" class="form-control" name="patronymic"
                           maxlength="45"
                           placeholder="${properties.placeholder_patronymic}"
                           value="${doctor.user.patronymic}" required>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input type="date" class="form-control" name="dayOfBirth"
                           placeholder="03.04.1940" value="${doctor.user.dayOfBirth}" required>
                </div>
            </td>

        </tr>

        </tbody>
    </table>


    <table class="${cssProperties.table_default}">
        <thead>
        <tr class="table-primary text-center">
            <th class="text-center" colspan="5">
                <h5>${properties.additionalInfo}</h5>
            </th>
        </tr>
        </thead>

        <tr class="text-center">
            <th class="text-center">${properties.passport}</th>
            <th class="text-center">${properties.email}</th>
            <th class="text-center">${properties.phone_number}</th>
            <th class="text-center">${properties.card_key_number}</th>
            <th class="text-center">${properties.login}</th>
        </tr>

        <tbody>
        <tr class="text-center">
            <th scope="row" class="text-center">
                <div class="form-group">
                    <input type="text" class="form-control" name="passport"
                           maxlength="8"
                           placeholder="${properties.placeholder_passport}"
                           value="${doctor.user.credentials.passport}" required>
                </div>
            </th>
            <td>
                <div class="form-group">
                    <input type="email" class="form-control" name="email"
                           maxlength="45"
                           placeholder="${properties.placeholder_email}"
                           value="${doctor.user.credentials.email}"
                           required>
                </div>
            </td>
            <td>
                <div class="form-group">
                    <input type="text" class="form-control" name="phoneNumber"
                           maxlength="45"
                           placeholder="${properties.placeholder_phone_number}"
                           value="${doctor.user.credentials.phoneNumber}" required>
                </div>
            </td>

            <td>
                <div class="form-group">
                    <input type="number" class="form-control" name="doctorCardKey"
                           maxlength="11"
                           placeholder="3142"
                           value="${doctor.cardKeyId}" required>
                </div>
            </td>

            <td>
                <div class="form-group">
                    <input type="text" class="form-control" name="login"
                           placeholder="${properties.placeholder_login}"
                           value="${doctor.user.registration.login}"
                           required>
                </div>
            </td>
        </tr>

        </tbody>
    </table>

    <button type="submit"
            class="btn btn-primary btn-lg btn-block">${properties.add_new_doctor}</button>
</form>


<div class="table-responsive">
    <label>
        <b>
            Список всех пользователей
        </b>
    </label>
    <%@ taglib uri="../../../WEB-INF/myTags/hospitalTags.tld" prefix="mytag" %>
    <mytag:AllUsers cssClass="table"
                    firstColumnName="id"
                    secondColumnName="surname"
                    thirdColumnName="name"
                    fourthColumnName="patronymic"
                    fifthColumnName="dayOfBirth"
                    role="role"/>
</div>


<hr class="featurette-divider">


