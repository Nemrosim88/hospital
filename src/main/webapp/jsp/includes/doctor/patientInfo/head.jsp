<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="${param.author}">
<link rel="icon" href="${param.context}/img/logo.gif">

<title>${properties.patient_info_page_name}</title>
<link href="${param.context}/css/bootstrap.min.css" rel="stylesheet">
<link href="${param.context}/css/dashboard.css" rel="stylesheet">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
