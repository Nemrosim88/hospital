<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="properties" type="java.util.Properties"--%>
<%--@elvariable id="cssProperties" type="java.util.Properties"--%>
<table class="${cssProperties.table_default}">
    <thead>
    <tr class="table-primary text-center">
        <th class="text-center" colspan="4">${properties.task}</th>
        <th class="text-center" colspan="7">${properties.result}</th>
    </tr>
    <tr>
        <th>#</th>
        <th>${properties.info}</th>
        <th>${properties.task_date}</th>
        <th>${properties.type}</th>
        <th>#</th>
        <th>${properties.result_date}</th>
        <th>${properties.info}</th>
        <th>${properties.position}</th>
        <th>${properties.surname}</th>
        <th>${properties.name}</th>
        <th>${properties.phone_number}</th>
    </tr>
    </thead>
    <tbody>
    <%--@elvariable id="patientTaskList" type="java.util.List"--%>
    <%--@elvariable id="task" type="entity.Task"--%>
    <c:forEach items="${patientTaskList}" var="task">
        <tr>
            <th scope="row">${task.id}</th>
            <td>${task.info}</td>
            <td>${task.datetime}</td>
            <td>${task.type}</td>
            <td>${not empty task.result.id ? task.result.id : '<img src="https://png.icons8.com/cancel/color/24" title="Отмена" width="24" height="24">' }</td>
            <td>${not empty task.result.datetime ? task.result.datetime : ''}</td>
            <td>${not empty task.result.info ? task.result.info : ''}</td>


            <c:choose>
                <c:when test="${not empty task.result.doctor}">
                    <td>DOCTOR</td>
                    <td>${not empty task.result.doctor.user.surname ?
                            task.result.doctor.user.surname : ''}
                    </td>
                    <td>${not empty task.result.doctor.user.name ?
                            task.result.doctor.user.name : ''}
                    </td>
                    <td>${not empty task.result.doctor.user.credentials.phoneNumber ?
                            task.result.doctor.user.credentials.phoneNumber : ''}
                    </td>
                </c:when>
                <c:otherwise>
                    <td>${not empty task.result.staff.position ? task.result.staff.position :
                            ''} </td>
                    <td>${not empty task.result.staff.user.surname ?
                            task.result.staff.user.surname : ''}</td>
                    <td>${not empty task.result.staff.user.name ? task.result.staff.user.name
                            : ''}</td>
                    <td>${not empty task.result.staff.user.credentials.phoneNumber ?
                            task.result.staff.user.credentials.phoneNumber : ''}</td>
                </c:otherwise>
            </c:choose>

        </tr>

    </c:forEach>

    </tbody>
</table>