<%--@elvariable id="cssProperties" type="java.util.Properties"--%>
<%--@elvariable id="patient" type="entity.Patient"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%--@elvariable id="task" type="entity.Task"--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table class="${cssProperties.table_default}">
    <thead>
    <tr class="table-primary text-center">
        <th class="text-center" colspan="9">${properties.patient_info}</th>
    </tr>
    <tr>
        <th>${properties.number}</th>
        <th>${properties.surname}</th>
        <th>${properties.name}</th>
        <th>${properties.patronymic}</th>
        <th>${properties.day_of_birth}</th>

        <th>${properties.discharge}</th>

        <th>${properties.passport}</th>
        <th>${properties.email}</th>
        <th>${properties.phone_number}</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <%--@elvariable id="patientUser" type="entity.User"--%>
        <th scope="row">${patient.id}</th>
        <td>${patient.user.surname}</td>
        <td>${patient.user.name}</td>
        <td>${patient.user.patronymic}</td>
        <td>${patient.user.dayOfBirth}</td>

        <c:choose>
            <c:when test="${patient.discharge}">
                <td>
                    <img src="https://png.icons8.com/approval/color/24" title="OK" width="24" height="24">
                    ВЫПИСАН
                </td>
            </c:when>
            <c:otherwise>
                <td>
                    <img src="https://png.icons8.com/cancel/color/24" title="NOT" width="24" height="24">
                    НЕ ВЫПИСАН
                </td>
            </c:otherwise>
        </c:choose>


        <td>${patient.user.credentials.passport}</td>
        <td>${patient.user.credentials.email}</td>
        <td>${patient.user.credentials.phoneNumber}</td>

    </tr>
    </tbody>
</table>
