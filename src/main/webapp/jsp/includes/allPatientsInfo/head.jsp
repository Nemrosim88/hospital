<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<c:set var="context" value="${pageContext.request.contextPath}"/>

<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="${context}/img/logo.gif">

<title>${properties.all_patients_info_page_name}</title>

<!-- Bootstrap core CSS -->
<link href="${context}/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="${context}/css/dashboard.css" rel="stylesheet">
<link href="${context}/css/mdb.css" rel="stylesheet">
<link rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
