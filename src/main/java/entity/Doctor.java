package entity;

import java.io.Serializable;
import javax.persistence.*;

public class Doctor implements Serializable, entity.abstraction.Entity{

    private int id;
    private int userId;
    private int cardKeyId;

    @Transient
    private User user;

    public Doctor() {
    }

    public Doctor(int userId, int cardKeyId) {
        this.userId = userId;
        this.cardKeyId = cardKeyId;
    }

    public Doctor(int id, int userId, int cardKeyId) {
        this.id = id;
        this.userId = userId;
        this.cardKeyId = cardKeyId;
    }

    @Id
    @Column(name = DOCTOR_ID_COLUMN_NAME)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = DOCTOR_USER_ID_COLUMN_NAME)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = DOCTOR_CARD_KEY_ID_COLUMN_NAME)
    public int getCardKeyId() {
        return cardKeyId;
    }

    public void setCardKeyId(int cardKeyId) {
        this.cardKeyId = cardKeyId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Doctor doctor = (Doctor) o;

        if (id != doctor.id) {
            return false;
        }
        if (userId != doctor.userId) {
            return false;
        }
        return cardKeyId == doctor.cardKeyId;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + cardKeyId;
        return result;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", userId=" + userId +
                ", cardKeyId=" + cardKeyId +
                '}';
    }
}
