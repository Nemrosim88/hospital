package entity;

import entity.abstraction.Entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Transient;

public class Diagnosis implements Serializable, Entity {

    private int id;
    private int patientId;
    private int doctorId;
    private String info;
    private Timestamp start;
    private Timestamp end;
    private String finalDiagnosis;

    @Transient
    private Doctor doctor;
    @Transient
    private Patient patient;

    public Diagnosis() {
    }

    public Diagnosis(int patientId,
                     int doctorId,
                     String info,
                     Timestamp start,
                     Timestamp end,
                     String finalDiagnosis) {
        this.patientId = patientId;
        this.doctorId = doctorId;
        this.info = info;
        this.start = start;
        this.end = end;
        this.finalDiagnosis = finalDiagnosis;
    }

    public Diagnosis(int id,
                     int patientId,
                     int doctorId,
                     String info,
                     Timestamp start,
                     Timestamp end,
                     String finalDiagnosis) {
        this.id = id;
        this.patientId = patientId;
        this.doctorId = doctorId;
        this.info = info;
        this.start = start;
        this.end = end;
        this.finalDiagnosis = finalDiagnosis;
    }

    @Id
    @Column(name = DIAGNOSIS_ID_COLUMN_NAME)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = DIAGNOSIS_PATIENT_ID_COLUMN_NAME)
    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Basic
    @Column(name = DIAGNOSIS_DOCTOR_ID_COLUMN_NAME)
    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    @Basic
    @Column(name = DIAGNOSIS_INFO_COLUMN_NAME)
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Basic
    @Column(name = DIAGNOSIS_START_COLUMN_NAME)
    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    @Basic
    @Column(name = DIAGNOSIS_START_COLUMN_NAME)
    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    @Basic
    @Column(name = DIAGNOSIS_FINAL_DIAGNOSIS_COLUMN_NAME)
    public String getFinalDiagnosis() {
        return finalDiagnosis;
    }

    public void setFinalDiagnosis(String finalDiagnosis) {
        this.finalDiagnosis = finalDiagnosis;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Diagnosis diagnosis = (Diagnosis) o;

        if (id != diagnosis.id) {
            return false;
        }
        if (patientId != diagnosis.patientId) {
            return false;
        }
        if (doctorId != diagnosis.doctorId) {
            return false;
        }
        if (info != null ? !info.equals(diagnosis.info) : diagnosis.info != null) {
            return false;
        }
        if (start != null ? !start.equals(diagnosis.start) : diagnosis.start != null) {
            return false;
        }
        if (end != null ? !end.equals(diagnosis.end) : diagnosis.end != null) {
            return false;
        }
        return finalDiagnosis != null
                ? finalDiagnosis.equals(diagnosis.finalDiagnosis)
                : diagnosis.finalDiagnosis == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + patientId;
        result = 31 * result + doctorId;
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (finalDiagnosis != null ? finalDiagnosis.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Diagnosis{" +
                "id=" + id +
                ", patientId=" + patientId +
                ", doctorId=" + doctorId +
                ", info='" + info + '\'' +
                ", start=" + start +
                ", end=" + end +
                ", finalDiagnosis='" + finalDiagnosis + '\'' +
                '}';
    }
}
