package entity;

import entity.enums.Type;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.*;

@Entity
public class Task implements Serializable, entity.abstraction.Entity {

    private int id;
    private int doctorId;
    private int patientId;
    private String info;
    private Timestamp datetime;
    private Type type;

    // Not for JPA
    @Transient
    private Result result;
    @Transient
    private Doctor doctor;
    @Transient
    private Patient patient;

    public Task() {
    }

    public Task(int doctorId, int patientId, String info, Timestamp datetime, Type type) {
        this.doctorId = doctorId;
        this.patientId = patientId;
        this.info = info;
        this.datetime = datetime;
        this.type = type;
    }

    public Task(int id, int doctorId, int patientId, String info, Timestamp datetime, Type type) {
        this.id = id;
        this.doctorId = doctorId;
        this.patientId = patientId;
        this.info = info;
        this.datetime = datetime;
        this.type = type;
    }


    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "doctorId")
    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    @Basic
    @Column(name = "patientId")
    public int getPatientId() {
        return patientId;
    }

    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }

    @Basic
    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Basic
    @Column(name = "datetime")
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Basic
    @Column(name = "type")
    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Task task = (Task) o;

        if (id != task.id) {
            return false;
        }
        if (info != null ? !info.equals(task.info) : task.info != null) {
            return false;
        }
        if (datetime != null ? !datetime.equals(task.datetime) : task.datetime != null) {
            return false;
        }
        if (type != null ? !type.equals(task.type) : task.type != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", doctorId=" + doctorId +
                ", patientId=" + patientId +
                ", info='" + info + '\'' +
                ", datetime=" + datetime +
                ", type=" + type +
                '}';
    }
}
