package entity.enums;

import java.io.Serializable;

public enum Position implements Serializable {

  NURSE(1), OPERATOR(2);

  private final int value;

  private Position(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public static Position ofStatusCode(int value) {

    switch (value) {

      case 0:
        return NURSE;

      case 1:
        return OPERATOR;

      default:
        return null;

    }
  }

  public static Position ofString(String position) {

    switch (position) {

      case "nurse":
        return NURSE;

      case "operator":
        return OPERATOR;

      default:
        return null;
    }
  }
}
