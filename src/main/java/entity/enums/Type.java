package entity.enums;

import java.io.Serializable;

public enum Type implements Serializable {

  DRUG(1), PROCEDURE(2), OPERATION(3);

  private final int value;

  private Type(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public static Type ofStatusCode(int value) {

    switch (value) {

      case 0:
        return DRUG;

      case 1:
        return PROCEDURE;

      case 2:
        return OPERATION;

      default:
        return null;

    }
  }

  public static Type ofString(String type) {

    switch (type) {

      case "drug":
        return DRUG;

      case "procedure":
        return PROCEDURE;

      case "operation":
        return OPERATION;

      default:
        return null;


    }
  }
}
