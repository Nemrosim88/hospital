package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Credentials implements Serializable, entity.abstraction.Entity {

  private int id;
  private int userId;
  private String passport;
  private String email;
  private String phoneNumber;

  public Credentials() {
  }

  /**
   * Конструктор для класса Credentials (Персональные данные).
   *
   * @param passport паспортные данные
   * @param email email пользователя
   * @param phoneNumber телефонные номер
   */
  public Credentials(String passport, String email, String phoneNumber) {
    this.passport = passport;
    this.email = email;
    this.phoneNumber = phoneNumber;
  }

  /**
   * Конструктор для класса Credentials (Персональные данные).
   *
   * @param userId userId
   * @param passport passport
   * @param email email
   * @param phoneNumber phoneNumber
   */
  public Credentials(int userId, String passport, String email, String phoneNumber) {
    this.userId = userId;
    this.passport = passport;
    this.email = email;
    this.phoneNumber = phoneNumber;
  }

  public Credentials(int id, int userId, String passport, String email, String phoneNumber) {
    this.id = id;
    this.userId = userId;
    this.passport = passport;
    this.email = email;
    this.phoneNumber = phoneNumber;
  }

  @Id
  @Column(name = "Id")
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "userId")
  public int getUserId() {
    return userId;
  }

  public void setUserId(int id) {
    this.userId = id;
  }

  @Basic
  @Column(name = "passport")
  public String getPassport() {
    return passport;
  }

  public void setPassport(String passport) {
    this.passport = passport;
  }

  @Basic
  @Column(name = "email")
  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  @Basic
  @Column(name = "phoneNumber")
  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Credentials that = (Credentials) o;

    if (id != that.id) {
      return false;
    }
    if (userId != that.userId) {
      return false;
    }
    if (passport != null ? !passport.equals(that.passport) : that.passport != null) {
      return false;
    }
    if (email != null ? !email.equals(that.email) : that.email != null) {
      return false;
    }
    return phoneNumber != null ? phoneNumber.equals(that.phoneNumber) : that.phoneNumber == null;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + userId;
    result = 31 * result + (passport != null ? passport.hashCode() : 0);
    result = 31 * result + (email != null ? email.hashCode() : 0);
    result = 31 * result + (phoneNumber != null ? phoneNumber.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Credentials{" +
        "id=" + id +
        ", userId=" + userId +
        ", passport='" + passport + '\'' +
        ", email='" + email + '\'' +
        ", phoneNumber='" + phoneNumber + '\'' +
        '}'+"\n";
  }
}
