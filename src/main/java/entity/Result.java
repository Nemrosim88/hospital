package entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class Result implements Serializable, entity.abstraction.Entity {

    private int id;
    private int taskId;
    private Timestamp datetime;
    private String info;
    private Integer doctorId;
    private Integer staffId;

    @Transient
    private User user;
    @Transient
    private Doctor doctor;
    @Transient
    private Staff staff;

    public Result() {
    }

    public Result(int taskId,
                  Timestamp datetime,
                  String info,
                  Integer doctorId,
                  Integer staffId) {
        this.taskId = taskId;
        this.datetime = datetime;
        this.info = info;
        this.doctorId = doctorId;
        this.staffId = staffId;
    }

    public Result(int id,
                  int taskId,
                  Timestamp datetime,
                  String info,
                  Integer doctorId,
                  Integer staffId) {
        this(taskId, datetime,info,doctorId,staffId);
        this.id = id;
    }

    @Basic
    @Column(name = "taskId")
    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "doctorId")
    public Integer getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(Integer doctorId) {
        this.doctorId = doctorId;
    }

    @Basic
    @Column(name = "staffId")
    public Integer getStaffId() {
        return staffId;
    }

    public void setStaffId(Integer staffId) {
        this.staffId = staffId;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "datetime")
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Basic
    @Column(name = "info")
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Result result = (Result) o;

        if (id != result.id) {
            return false;
        }
        if (taskId != result.taskId) {
            return false;
        }
        if (datetime != null ? !datetime.equals(result.datetime) : result.datetime != null) {
            return false;
        }
        if (info != null ? !info.equals(result.info) : result.info != null) {
            return false;
        }
        if (doctorId != null ? !doctorId.equals(result.doctorId) : result.doctorId != null) {
            return false;
        }
        return staffId != null ? staffId.equals(result.staffId) : result.staffId == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + taskId;
        result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
        result = 31 * result + (info != null ? info.hashCode() : 0);
        result = 31 * result + (doctorId != null ? doctorId.hashCode() : 0);
        result = 31 * result + (staffId != null ? staffId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Result{" +
                "id=" + id +
                ", taskId=" + taskId +
                ", datetime=" + datetime +
                ", info='" + info + '\'' +
                ", doctorId=" + doctorId +
                ", staffId=" + staffId +
                "}\n";
    }
}
