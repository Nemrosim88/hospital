package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Registration implements Serializable, entity.abstraction.Entity {

  private int id;
  private int userId;
  private String login;
  private String password;

  public Registration() {
  }

  public Registration(String login, String password) {
    this.login = login;
    this.password = password;
  }

  public Registration(int userId, String login, String password) {
    this.userId = userId;
    this.login = login;
    this.password = password;
  }

  public Registration(int id, int userId, String login, String password) {
    this.id = id;
    this.userId = userId;
    this.login = login;
    this.password = password;
  }

  @Id
  @Column(name = "id")
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "userId")
  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  @Basic
  @Column(name = "login")
  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  @Basic
  @Column(name = "password")
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Registration that = (Registration) o;

    if (id != that.id) {
      return false;
    }
    if (userId != that.userId) {
      return false;
    }
    if (login != null ? !login.equals(that.login) : that.login != null) {
      return false;
    }
    return password != null ? password.equals(that.password) : that.password == null;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + userId;
    result = 31 * result + (login != null ? login.hashCode() : 0);
    result = 31 * result + (password != null ? password.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "Registration{" +
        "id=" + id +
        ", userId=" + userId +
        ", login='" + login + '\'' +
        ", password='" + password + '\'' +
        '}';
  }
}
