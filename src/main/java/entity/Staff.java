package entity;

import entity.enums.Position;

import java.io.Serializable;
import javax.persistence.*;

@Entity
public class Staff implements Serializable, entity.abstraction.Entity {

    private int id;
    private int userId;
    private Position position;

    //Для удобства
    @Transient
    private User user;

    public Staff() {
    }

    public Staff(int userId, Position position) {
        this.userId = userId;
        this.position = position;
    }

    public Staff(int id, int userId, Position position) {
        this.id = id;
        this.userId = userId;
        this.position = position;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "userId")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "position")
    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Staff staff = (Staff) o;

        if (id != staff.id) {
            return false;
        }
        if (position != null ? !position.equals(staff.position) : staff.position != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (position != null ? position.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Staff{" +
                "id=" + id +
                ", userId=" + userId +
                ", position=" + position +
                ", user=" + user +
                '}';
    }
}
