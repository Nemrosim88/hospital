package entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

@Entity
public class Patient implements Serializable, entity.abstraction.Entity {

    private int id;
    private int userId;
    private boolean discharge;

    @Transient
    private User user;
    @Transient
    private List<Diagnosis> diagnosisList;

    public Patient() {
    }

    public Patient(int userId) {
        this.userId = userId;
    }

    public Patient(int id, int userId) {
        this.id = id;
        this.userId = userId;
    }

    public Patient(int id, int userId, boolean discharge) {
        this.id = id;
        this.userId = userId;
        this.discharge = discharge;
    }

    public Patient(int userId, boolean discharge) {
        this.userId = userId;
        this.discharge = discharge;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "userId")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "discharge")
    public boolean getDischarge() {
        return discharge;
    }

    public void setDischarge(boolean discharge) {
        this.discharge = discharge;
    }

    public boolean isDischarge() {
        return discharge;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Diagnosis> getDiagnosisList() {
        return diagnosisList;
    }

    public void setDiagnosisList(List<Diagnosis> diagnosisList) {
        this.diagnosisList = diagnosisList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Patient patient = (Patient) o;

        if (id != patient.id) {
            return false;
        }
        if (userId != patient.userId) {
            return false;
        }
        return discharge == patient.discharge;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + userId;
        result = 31 * result + (discharge ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "id=" + id +
                ", userId=" + userId +
                ", discharge=" + discharge +
                '}';
    }
}
