package tags.cards;

import control.controller.Controller;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.*;
import org.apache.log4j.Logger;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

public class PatientMinInfo extends BodyTagSupport implements Constants {

    private static final Logger logger = Logger.getLogger(PatientMinInfo.class);
    private int doctorId;
    private String divClass;

    public String getDivClass() {
        return divClass;
    }

    public void setDivClass(String divClass) {
        this.divClass = divClass;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public int doStartTag() throws JspException {
        logger.info(": doStartTag()");
        try {
            JspWriter out = pageContext.getOut();

            String command = "patientInfo";

            Connection connection = Controller.connection;
            logger.info(": connection -> " + connection);

            GetHospitalDAO dao = GetHospitalDAO.getInstance(connection);

            //Получаем все таски врача по его ID
            List<Task> taskList = dao.getTaskDAO().getByKey(TASK_DOCTOR_ID_COLUMN_NAME, doctorId);

            out.print("<section id=\"cascading-cards\">");
            out.print("<div class=\"row\">");

            for (Task task : taskList) {

                dao.fillTaskWithAllInfo(task);

                User user = task.getPatient().getUser();
                String prettyUserName = String.format("%s %s %s",
                        user.getSurname(),
                        user.getName(),
                        user.getPatronymic());

                out.print("<div class=\"" + divClass + "\">");
                out.print("<section>");
                out.print("<div class=\"card card-cascade narrower mb-r\" "
                        + "style=\"margin-top:" +
                        " 52px\">");
                out.print("<div class=\"view overlay hm-white-slight\">");
                out.print(
                        "<img src=\"/img/icons/avatar.jpg\" class=\"img-fluid\" alt=\"\">");
                out.print("<a>");
                out.print("<div class=\"mask\"><h1>" + prettyUserName + "</h1></div>");
                out.print("</a>");
                out.print("</div>");

                out.print("<div class=\"card-body\">");

                Credentials credentials = task.getPatient().getUser().getCredentials();
                out.print("<h5 class=\"blue-text\"><i class=\"fa fa-phone-square\"></i> Телефон:"
                        + credentials.getPhoneNumber()
                        + "</h5>");
                out.print("<h5 class=\"blue-text\"><i class=\"fa fa-at\" "
                        + "aria-hidden=\"true\"></i></i> Email:" + credentials
                        .getEmail() + "</h5>");

                out.print(
                        "<h4 class=\"card-title text-center\"><img "
                                + "src=\"https://png.icons8.com/work/color/48\" "
                                + "title=\"Work\" width=\"48\" height=\"48\">"
                                + task.getType() + "</h4>");

                out.print("<p class=\"card-text text-center\">" + task.getInfo() + "</p>");
                out.print("<ul class=\"list-group list-group-flush\">");
                out.print("<li class=\"list-group-item text-center\"><b>Назначено:</b></li>");
                out.print("<li class=\"list-group-item text-center\">"
                        + task.getDatetime().toString() + "</li>");
                out.print("<li class=\"list-group-item text-center\"><b>Результат:</b></li>");

                Result result = task.getResult();
                String resultToCard = result != null
                        ? result.getDatetime().toString()
                        : "Отсутствует";
                if (resultToCard.equals("Отсутствует")) {
                    out.print("<li class=\"list-group-item list-group-item-danger text-center\">"
                            + "<img src=\"https://png.icons8.com/cancel/color/48\"" +
                            " title=\"Cancel\" width=\"48\" height=\"48\">" + resultToCard
                            + "</li>");
                } else {
                    out.print("<li class=\"list-group-item list-group-item-success text-center\">"
                            + "<img src=\"https://png.icons8.com/ok/color/48\" "
                            + "title=\"Ok\" width=\"48\" height=\"48\">" + resultToCard + "</li>");
                }

                Patient patient = task.getPatient();
                if (patient.getDischarge()) {
                    out.print("<li class=\"list-group-item list-group-item-success text-center\">"
                            + "<img src=\"https://png.icons8.com/ok/color/48\" "
                            + "title=\"Ok\" width=\"48\" height=\"48\">" + "Выписан" + "</li>");
                } else if (!patient.getDischarge()) {
                    out.print("<li class=\"list-group-item list-group-item-danger text-center\">"
                            + "<img src=\"https://png.icons8.com/cancel/color/48\" "
                            + "title=\"Cancel\" width=\"48\" height=\"48\">"
                            + "Не выписан" + "</li>");
                } else {
                    out.print("<li class=\"list-group-item list-group-item-danger text-center\">"
                            + "<img src=\"https://png.icons8.com/ok/color/48\" "
                            + "title=\"Ok\" width=\"48\" height=\"48\">"
                            + "Информация отсутствует" + "</li>");
                }

                out.print("</ul>");
                out.print("<form class=\"form-inline mt-2 mt-md-0\" "
                        + "action=\"controller\" method=\"post\">");
                out.print("<input type=\"hidden\" name=\"command\" value=\"" + command + "\">");
                out.print("<input type=\"hidden\" name=\"patientId\" value=\""
                        + patient.getId() + "\">");
                out.print("<button class=\"btn btn-primary btn-lg btn-block\" "
                        + "type=\"submit\">Полная информация</button>");
                out.print("</form>");
//                out.print("<a class=\"btn btn-blue\" > Полная информация</a>");
                out.print("</div>");
                out.print("</div>");
                out.print("</section>");
                out.print("</div>");
            }
            out.print("</div>");
            out.print("</section>");

        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        return SKIP_BODY;
    }

    @Override
    public void setPageContext(PageContext pageContext) {
        super.setPageContext(pageContext);
    }

}
