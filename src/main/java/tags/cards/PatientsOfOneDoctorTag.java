package tags.cards;

import data.Constants;
import data.dao.GetHospitalDAO;
import data.dao.implementation.HospitalDAO;
import data.pool.ConnectionPoolForMariaDB;
import entity.*;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;

public class PatientsOfOneDoctorTag extends BodyTagSupport implements Constants {

    private int doctorId;

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            JspWriter out = pageContext.getOut();


            Connection connection = ConnectionPoolForMariaDB.getInstance().getConnection();
            GetHospitalDAO dao = GetHospitalDAO.getInstance(connection);

            HospitalDAO<User> userDAO = dao.getUserDAO();
            HospitalDAO<Credentials> credentialsDAO = dao.getCredentialsDAO();

            //Получаем все таски врача по его ID
            List<Task> taskList = dao.getTaskDAO().getByKey(TASK_DOCTOR_ID_COLUMN_NAME, doctorId);

            for (Task task : taskList) {

                int patientId = task.getPatientId();
                Patient patient =
                        dao.getPatientDAO().getByKey(PATIENT_ID_COLUMN_NAME, patientId).get(0);

                int userId = patient.getUserId();
                User user = dao.getUserDAO().getByKey(USER_ID_COLUMN_NAME, userId).get(0);

                // У пользователь будет только однин Credentials. ID - уникален
                Credentials credentials = new Credentials("Не указано", "Не указано", "Не указано");

                List<Credentials> credentialsList =
                        dao.getCredentialsDAO().getByKey(CREDENTIALS_USER_ID_COLUMN_NAME, userId);
                if (!credentialsList.isEmpty()) {
                    credentials = credentialsList.get(0);
                }

                // Результат будет только одни. ID - уникален. Результат true/false
                List<Result> hospitalDAOByKey =
                        dao.getResultDAO().getByKey(RESULT_TASK_ID_COLUMN_NAME, task.getId());
                Result result = null;
                if (!hospitalDAOByKey.isEmpty()) {
                    result = hospitalDAOByKey.get(0);
                }


                out.print("<div class=\"col-md-auto\">");

                out.print("<div class=\"card text-center\" style=\"width: 20rem;\">");
                out.print("<img class=\"rounded-circle mx-auto d-block\" "
                        + "data-src=\"holder.js/200x200/auto\" "
                        + "alt=\"Card image cap\" width=\"200\" height=\"200\">");
                out.print("<div class=\"card-body\">");

                String prettyUserName = String.format("%s %s %s",
                        user.getSurname(),
                        user.getName(),
                        user.getPatronymic());
                out.print("<h4 class=\"card-title\">" + prettyUserName + "</h4>");
                out.print("<p class=\"card-text\" >  Card title  </p>");
                out.print("</div>");

                out.print("<ul class=\"list-group list-group-flush\">");


                out.print("<li class=\"list-group-item\" ><b>ДАННЫЕ ПАЦИЕНТА</b></li>");

                out.print("<li class=\"list-group-item\" >Паспорт: "
                        + credentials.getPassport() + "</li>");
                out.print("<li class=\"list-group-item\" >Email: "
                        + credentials.getEmail() + "</li>");
                out.print("<li class=\"list-group-item\" >Тел. номер: "
                        + credentials.getPhoneNumber() + "</li>");


                out.print("<li class=\"list-group-item\" ><b>ЗАДАЧА</b></li>");

                out.print("<li class=\"list-group-item\" >Задача: "
                        + task.getType() + "</li>");
                out.print("<li class=\"list-group-item\" >Постановка: "
                        + task.getDatetime().toString() + "</li>");
                out.print("<li class=\"list-group-item\" >Инфо: "
                        + task.getInfo() + "</li>");


                out.print("<li class=\"list-group-item\" ><b>РЕЗУЛЬТАТЫ</b></li>");

                if (result == null) {

                    out.print("<li class=\"list-group-item\" >Результатов нет</li>");

                } else {

                    if (result.getStaffId() != null) {

                        HospitalDAO<Staff> staffDAO = dao.getStaffDAO();
                        Staff staff =
                                staffDAO.getByKey(STAFF_ID_COLUMN_NAME, result.getStaffId()).get(0);

                        User staffUser =
                                userDAO.getByKey(USER_ID_COLUMN_NAME, staff.getUserId()).get(0);

                        Credentials staffCredentials =
                                credentialsDAO.getByKey(USER_ID_COLUMN_NAME, staffUser.getId())
                                        .get(0);

                        String prettyStaffName = String.format("%s %c. %c.&#10;%s",
                                staffUser.getSurname(),
                                staffUser.getName().charAt(0),
                                staffUser.getPatronymic().charAt(0),
                                staffCredentials.getPhoneNumber());
                        out.print("<li class=\"list-group-item\" >Дата: "
                                + result.getDatetime().toString() + "</li>");
                        out.print("<li class=\"list-group-item\" >Результат: "
                                + result.getInfo() + "</li>");
                        out.print("<li class=\"list-group-item\" >Выполнил: "
                                + prettyStaffName + "</li>");

                    } else {

                        HospitalDAO<Doctor> doctorDAO = dao.getDoctorDAO();

                        int doctorId = result.getDoctorId();

                        Doctor doctor =
                                doctorDAO.getByKey(DOCTOR_ID_COLUMN_NAME, doctorId)
                                        .get(0);

                        User doctorUser =
                                userDAO.getByKey(USER_ID_COLUMN_NAME, doctor.getUserId())
                                        .get(0);

                        Credentials doctorCredentials =
                                credentialsDAO.getByKey(USER_ID_COLUMN_NAME, doctorUser.getId())
                                        .get(0);

                        String prettyDoctorName = String.format("%s %c. %c.&#10;%s",
                                doctorUser.getSurname(),
                                doctorUser.getName().charAt(0),
                                doctorUser.getPatronymic().charAt(0),
                                doctorCredentials.getPhoneNumber());
                        out.print("<li class=\"list-group-item\" >Дата: "
                                + result.getDatetime().toString() + "</li>");
                        out.print("<li class=\"list-group-item\" >Результат: "
                                + result.getInfo() + "</li>");
                        out.print("<li class=\"list-group-item\" >Выполнил: "
                                + prettyDoctorName + "</li>");
                    }
                }

                out.print("<a href=\"#\" class=\"btn btn-primary\">Go somewhere</a>");
                out.print("</div>");
                out.print("</div>");

            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
        return SKIP_BODY;
    }


    @Override
    public void setPageContext(PageContext pageContext) {
        super.setPageContext(pageContext);
    }


}
