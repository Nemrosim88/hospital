package control.filters;

import org.apache.log4j.Logger;

import javax.servlet.*;
import java.io.IOException;

public class CharsetFilter implements Filter {

  private final static Logger logger = Logger.getLogger(CharsetFilter.class);
  private String encoding;

  @Override
  public void init(FilterConfig config) throws ServletException {
    logger.info(": init()");
    if (encoding == null) {
      encoding = "UTF-8";
    }
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    logger.info(": doFilter()");

    if (null == request.getCharacterEncoding()) {
      request.setCharacterEncoding(encoding);
    }

    response.setContentType("text/html; charset=UTF-8");
    response.setCharacterEncoding("UTF-8");

    chain.doFilter(request, response);
  }

  @Override
  public void destroy() {
    logger.info(": destroy()");

  }
}
