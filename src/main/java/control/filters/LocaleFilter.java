package control.filters;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class LocaleFilter implements Filter {

    private final static Logger logger = Logger.getLogger(LocaleFilter.class);

    @Override
    public void init(FilterConfig config) throws ServletException {
        logger.info(": init()");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        logger.info(": doFilter");

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String contextPath = httpRequest.getContextPath();

        Properties properties = (Properties) httpRequest.getSession().getAttribute("properties");
        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        if (properties == null || properties.isEmpty()) {
            logger.info(": properties == null  OR empty");
            properties = new Properties();

            InputStream is = loader.getResourceAsStream(contextPath + "/i18n/text.properties");
            InputStreamReader isr = new InputStreamReader(is, "UTF-8");

            properties.load(isr);

            logger.info(": loading properties from file");

            is.close();
            isr.close();

            httpRequest.getSession().setAttribute("properties", properties);
            logger.info(": setAttribute properties");
        }

        Properties cssProperties =
                (Properties) httpRequest.getSession().getAttribute("cssProperties");

        if (cssProperties == null || cssProperties.isEmpty()) {
            logger.info(": css properties == null OR empty");
            cssProperties = new Properties();

            InputStream is = loader.getResourceAsStream(contextPath + "/css/css.properties");
            InputStreamReader isr = new InputStreamReader(is, "UTF-8");

            cssProperties.load(isr);

            logger.info(": loading css properties from file");

            is.close();
            isr.close();

            httpRequest.getSession().setAttribute("cssProperties", cssProperties);
            logger.info(": setAttribute cssProperties");
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        logger.info(": destroy()");
    }

}
