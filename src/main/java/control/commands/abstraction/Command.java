package control.commands.abstraction;


import data.Constants;

public interface Command extends Constants{

    /**
     * This method will be executed depending on class type.
     *
     * @return location of jsp page that will process the response.
     */
    String execute();
}
