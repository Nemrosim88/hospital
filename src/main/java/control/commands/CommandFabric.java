package control.commands;

import control.commands.implementation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;

import control.commands.implementation.admin.AddAdmin;
import control.commands.implementation.admin.AddDoctor;
import control.commands.implementation.admin.AddPatient;
import control.commands.implementation.admin.AddStaff;
import control.commands.implementation.doctor.*;
import control.commands.implementation.staff.AddResult;
import control.commands.implementation.staff.ViewTasks;
import org.apache.log4j.Logger;

/**
 * This class is implementing fabric pattern. Depending on command from html form.
 */
public class CommandFabric {

    private final static Logger logger = Logger.getLogger(CommandFabric.class);

    /**
     * Метод для получения нужного экземпляра класса комманды.
     *
     * @param command    текстовое представление команды. "login" - создаётся Login
     * @param connection connection
     * @param request    request
     * @param response   response
     * @return возвращает строку - результат работы команды
     */
    public String get(String command,
                      Connection connection,
                      HttpServletRequest request,
                      HttpServletResponse response) {
        logger.info(String.format(": get() method. Variables: " +
                "command = %s, " +
                "connection = %s, " +
                "request = %s, " +
                "response = "
                + "%s", command, connection, request, response));

        switch (command) {
            case "login":
                return Login.getInstance(connection, request, response)
                        .execute();
            case "addPatient":
                return AddPatient.getInstance(connection, request, response)
                        .execute();
            case "addTaskToPatient":
                return AddTaskToPatient.getInstance(connection, request, response)
                        .execute();
            case "addResultAndTaskToPatient":
                return AddResultAndTaskToPatient.getInstance(connection, request, response)
                        .execute();
            case "addResult":
                return AddResult.getInstance(connection, request, response)
                        .execute();
            case "addAdmin":
                return AddAdmin.getInstance(connection, request, response)
                        .execute();
            case "addDoctor":
                return AddDoctor.getInstance(connection, request, response)
                        .execute();
            case "newPatients":
                return NewPatients.getInstance(connection, request, response)
                        .execute();
            case "addStaff":
                return AddStaff.getInstance(connection, request, response)
                        .execute();
            case "patientInfo":
                return PatientInfo.getInstance(connection, request, response)
                        .execute();
            case "viewTasks":
                return ViewTasks.getInstance(connection, request, response)
                        .execute();
            case "allPatientsInfo":
                return AllPatientsInfo.getInstance(connection, request, response)
                        .execute();
            case "getProperties":
                return GetProperties.getInstance(connection, request, response)
                        .execute();
            case "logout":
                return InvalidateSession.getInstance(connection, request, response)
                        .execute();
            case "discharge":
                return DischargePatient.getInstance(connection, request, response)
                        .execute();
            case "doctorTasks":
                return DoctorTasks.getInstance(connection, request, response)
                        .execute();
            default:
                logger.warn("Unknown COMMAND");
                return "/";
        }

    }


}
