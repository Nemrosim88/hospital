package control.commands.utils;

import control.service.SendEmail;
import entity.Credentials;
import entity.Registration;
import entity.User;
import entity.enums.Role;
import org.apache.commons.lang.RandomStringUtils;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * Class contains methods that was most of the time repeated in code.
 */
public class CommandsUtils {

    private CommandsUtils() {
    }

    /**
     * Method for getting user params from request and creating new User Object.
     *
     * @param request request
     * @param setRole Role
     * @return User
     * @see Role
     * @see User
     * @see HttpServletRequest
     */
    public static User getUserFromRequest(HttpServletRequest request, Role setRole) {
        String surname = request.getParameter("surname");
        String name = request.getParameter("name");
        String patronymic = request.getParameter("patronymic");
        Date dayOfBirth = Date.valueOf(request.getParameter("dayOfBirth"));

        return new User(surname, name, patronymic, dayOfBirth, setRole);
    }

    /**
     * Method for getting credentials params from request
     * and creating new Credentials Object.
     *
     * @param request request
     * @return Credentials
     * @see Credentials
     * @see HttpServletRequest
     */
    public static Credentials getCredentialsFromRequest(HttpServletRequest request) {
        String passport = request.getParameter("passport");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phoneNumber");

        return new Credentials(passport, email, phoneNumber);
    }


    /**
     * Method for getting registration params from request
     * and creating new Registration Object.
     *
     * @param request HttpServletRequest
     * @return Registration
     * @see Registration
     * @see HttpServletRequest
     */
    public static Registration getRegistrationFromRequest(HttpServletRequest request) {
        Registration registration = new Registration();
        registration.setLogin(request.getParameter("login"));
        return registration;
    }

    /**
     * Method generating random password with six chars length using letters and numbers.
     *
     * @return generated password
     */
    public static String generateRandomPassword() {
        int length = 6;
        boolean useLetters = true;
        boolean useNumbers = true;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }

    /**
     * Send email to user notifying his of his login (entered by administrator)
     * and random generated password.
     *
     * @param credentials  credentials
     * @param registration registration
     * @see Credentials
     * @see Registration
     */
    public static void sentEmailToUserWithRegistrationData(Credentials credentials,
                                                           Registration registration) {
        try {
            SendEmail.mail(credentials.getEmail(),
                    registration.getLogin(),
                    registration.getPassword());
        } catch (MessagingException e) {
            e.printStackTrace();
        }

    }
}
