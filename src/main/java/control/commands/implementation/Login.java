package control.commands.implementation;

import control.commands.abstraction.Command;
import control.controller.Controller;
import control.service.Cookies;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.*;
import entity.enums.Role;
import org.apache.log4j.Logger;

import java.sql.Connection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Login implements Command {

    private static final Logger logger = Logger.getLogger(Login.class);
    public static final String REDIRECT_PAGE = Controller.contextPath + "/";


    private static Login command;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;
    private static Registration registration;

    private GetHospitalDAO dao;
    private Role userRole;
    private User user;


    private static final String LOGIN_PARAM = "login";
    private static final String PASSWORD_PARAM = "password";
    private static final String ERROR_MESSAGE = "Nickname or Password doesn't match ";

    private Login() {
    }

    public static Login getInstance(Connection conn,
                                    HttpServletRequest req,
                                    HttpServletResponse resp) {
        connection = conn;
        request = req;
        response = resp;
        if (command == null) {
            command = new Login();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");

        logger.info(": connection = " + connection);

        dao = GetHospitalDAO.getInstance(connection);

        if (registrationExistsInDB()) {
            setValuesForResponse();
            new Cookies(response, registration);
        } else {
//            request.setAttribute(LOGIN_PARAM, login);
            request.setAttribute("message", ERROR_MESSAGE);
        }

        return REDIRECT_PAGE;
    }


    /**
     * Private method for setting values for response to user back to browser.
     */
    private void setValuesForResponse() {
        logger.info(": setValuesForResponse()");

        user = dao.getUserDAO().getByKey(USER_ID_COLUMN_NAME, registration.getUserId()).get(0);

        checkRoleOfUserAndSetAppropriateAttributesToSession();

        request.getSession().setAttribute("user", user);
        request.getSession().setAttribute("logged", userRole);
    }


    /**
     * Private method for checking user's role and depending of -
     * setting appropriate attributes to session.
     */
    private void checkRoleOfUserAndSetAppropriateAttributesToSession() {
        int userId = user.getId();
        userRole = user.getRole();
        logger.info(": USER LOGGED IN. Role == " + userRole + " with ID:" + userId);

        if (userRole == Role.DOCTOR) {
            Doctor doctor = dao.getDoctorDAO().getByKey(DOCTOR_USER_ID_COLUMN_NAME, userId).get(0);
            logger.info(": USER THAT LOGGED IN is: " + doctor);

            request.getSession().setAttribute("doctor", doctor);

        } else if (userRole == Role.STAFF) {
            Staff staff = dao.getStaffDAO().getByKey(STAFF_USER_ID_COLUMN_NAME, userId).get(0);
            logger.info(": USER THAT LOGGED IN is: " + staff);

            request.getSession().setAttribute("staff", staff);

        } else if (userRole == Role.PATIENT) {
            Patient patient =
                    dao.getPatientDAO().getByKey(PATIENT_USER_ID_COLUMN_NAME, userId).get(0);
            logger.info(": USER THAT LOGGED IN is: " + patient);

            request.getSession().setAttribute("patient", patient);
        }
    }


    /**
     * Private method for checking existence of login AND password in database.
     * @return if login and password exists in database - true
     */
    private boolean registrationExistsInDB() {
        logger.info(": registrationExistsInDB()");

        String login = request.getParameter(LOGIN_PARAM);
        String password = request.getParameter(PASSWORD_PARAM);
        Registration registrationFromRequest = new Registration(login, password);

        registration = dao.getRegistrationDAO().isExist(registrationFromRequest);
        return registration != null;
    }

}