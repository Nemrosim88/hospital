package control.commands.implementation.staff;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.Staff;
import entity.Task;
import entity.enums.Position;
import entity.enums.Type;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

public class ViewTasks implements Command {

    private final static Logger logger = Logger.getLogger(ViewTasks.class);

    private static ViewTasks command;

    private static HttpServletResponse response;
    private static HttpServletRequest request;
    private static Connection connection;
    private GetHospitalDAO dao;
    private String redirectPage = Controller.contextPath + "/jsp/staff/tasks.jsp";


    private ViewTasks() {
    }

    public static ViewTasks getInstance(Connection conn,
                                        HttpServletRequest req,
                                        HttpServletResponse resp) {
        request = req;
        response = resp;
        connection = conn;
        if (command == null) {
            command = new ViewTasks();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");

        dao = GetHospitalDAO.getInstance(connection);

        Staff staff = (Staff) request.getSession().getAttribute("staff");

        Position position = staff.getPosition();

        Type type = null;
        if (position == Position.OPERATOR) {
            type = Type.PROCEDURE;

        } else if (position == Position.NURSE) {
            type = Type.DRUG;
        }

        List<Task> staffTaskList = dao.getStaffTasksWithoutResults(type);


        if (staffTaskList != null && !staffTaskList.isEmpty()) {
            for (Task task : staffTaskList) {
                dao.fillTaskWithAllInfo(task);
            }
        }

        request.setAttribute("staffTaskList", staffTaskList);

        return redirectPage;
    }
}
