package control.commands.implementation.staff;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.Result;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.Timestamp;

public class AddResult implements Command {

    private final static Logger logger = Logger.getLogger(AddResult.class);

    private static AddResult command;

    private static HttpServletResponse response;
    private static HttpServletRequest request;
    private static Connection connection;
    private GetHospitalDAO dao;
    private String redirectPage = Controller.contextPath + "/jsp/staff/tasks.jsp";

    private int patientId;
    private int taskId;
    private int staffId;
    private Timestamp dateOfProcedure;
    private String resultInfo;

    private AddResult() {
    }

    public static AddResult getInstance(Connection conn,
                                        HttpServletRequest req,
                                        HttpServletResponse resp) {
        request = req;
        response = resp;
        connection = conn;
        if (command == null) {
            command = new AddResult();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");

        getParametersFromRequest();

        dao = GetHospitalDAO.getInstance(connection);

        Result result = new Result(taskId,dateOfProcedure,resultInfo,null,staffId);
        int resultIdAfterInsert = dao.getResultDAO().insert(result);

        if(resultIdAfterInsert != -1){
            logger.info(": NEW RESULT INSERTED. ID = " + resultIdAfterInsert);
            String message = String.format("Result was inserted successfully. ID:'%d'",
                    resultIdAfterInsert);
            request.setAttribute("message", message);
        } else {
            logger.warn(": NEW RESULT WAS NOT INSERTED: " + result);
            request.setAttribute("message", "Something went wrong!");
        }

        return redirectPage;
    }

    private void getParametersFromRequest() {
        patientId = Integer.parseInt(request.getParameter("patientId"));
        taskId = Integer.parseInt(request.getParameter("taskId"));
        staffId = Integer.parseInt(request.getParameter("taskId"));


        // строка запроса - yyyy-mm-ddTHH:mm
        // для удачной конвертации в Timestamp должно быть yyyy-mm-dd HH:mm:ss
        String date = request.getParameter("dateOfProcedure") + ":00";
        String replace = date.replace('T', ' ');
        dateOfProcedure = Timestamp.valueOf(replace);

        resultInfo = request.getParameter("resultInfo");
    }
}