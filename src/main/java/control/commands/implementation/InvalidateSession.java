package control.commands.implementation;

import control.commands.abstraction.Command;
import control.controller.Controller;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class InvalidateSession implements Command {

    private static InvalidateSession command;
    private final static Logger logger = Logger.getLogger(InvalidateSession.class);

    private static HttpServletRequest request;
    private static Connection connection;

    private InvalidateSession() {
    }

    public static InvalidateSession getInstance(Connection conn,
                                                HttpServletRequest req,
                                                HttpServletResponse resp) {
        request = req;
        connection = conn;
        if (command == null) {
            command = new InvalidateSession();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": variable requestFormName = \"logout\" ");

        Properties properties = (Properties) request.getSession().getAttribute("properties");
        Properties cssProperties = (Properties) request.getSession().getAttribute("cssProperties");

        request.getSession().invalidate();
        logger.info(": SESSION IS INVALIDATED");

        freeConnection(connection);
        logger.info(": CONNECTION CLOSED");

        request.getSession().setAttribute("properties", properties);
        request.getSession().setAttribute("cssProperties", cssProperties);
        logger.info(": previous properties is set");

        return Controller.contextPath + "/";
    }

    private void freeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
    }

}
