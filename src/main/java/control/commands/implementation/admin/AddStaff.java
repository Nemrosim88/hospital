package control.commands.implementation.admin;

import control.commands.abstraction.Command;
import control.commands.utils.CommandsUtils;
import control.controller.Controller;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.*;
import entity.enums.Position;
import entity.enums.Role;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

/**
 * This class is for processing form with command "addStaff".
 */
public class AddStaff implements Command {

    private static final Logger logger = Logger.getLogger(AddStaff.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/jsp/addStaff.jsp";

    private static AddStaff command;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;
    private GetHospitalDAO dao;
    private User userFromRequest;
    private Registration registrationFromRequest;
    private Credentials credentialsFromRequest;
    private Staff staff;

    private AddStaff() {
    }

    public static AddStaff getInstance(Connection c,
                                        HttpServletRequest req,
                                        HttpServletResponse resp) {
        connection = c;
        request = req;
        response = resp;
        if (command == null) {
            command = new AddStaff();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info("execute()");
        dao = GetHospitalDAO.getInstance(connection);

        getUserAndRegistrationAndCredentialsFromRequest();

        if (loginAlreadyExistsInDataBase()
                || credentialsAlreadyExistsInDataBase()
                || userAlreadyExistsInDataBase()) {
            return REDIRECT_PAGE;
        } else {
            inputAndCommitChanges();
            return REDIRECT_PAGE;
        }
    }

    /**
     * Private method that combines in one place three different private methods.
     */
    private void getUserAndRegistrationAndCredentialsFromRequest() {
        logger.info("getUserAndRegistrationAndCredentialsFromRequest() start method");

        userFromRequest = CommandsUtils.getUserFromRequest(request, Role.STAFF);
        registrationFromRequest = CommandsUtils.getRegistrationFromRequest(request);
        credentialsFromRequest = CommandsUtils.getCredentialsFromRequest(request);

        userFromRequest.setCredentials(credentialsFromRequest);
        userFromRequest.setRegistration(registrationFromRequest);

        staff = new Staff();
        staff.setUser(userFromRequest);
        staff.setPosition(Position.ofString(request.getParameter("position")));

        logger.info("getUserAndRegistrationAndCredentialsFromRequest() end method");
    }

    /**
     * Method for checking if user that was from request is already exists in database.
     *
     * @return true - if exists
     */
    private boolean userAlreadyExistsInDataBase() {
        User existingUser = dao.getUserDAO().isExist(userFromRequest);
        boolean userExists = existingUser != null;

        if (userExists) {
            String loginMessage = "Пользователь с такими параметрами уже существует";
            request.setAttribute("loginMessage", loginMessage);
            request.setAttribute("staff", staff);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Private method for checking if credentials that was from request
     * is already exists in database.
     *
     * @return true - if exists
     */
    private boolean credentialsAlreadyExistsInDataBase() {
        Credentials existingCredentials = dao.getCredentialsDAO().isExist(credentialsFromRequest);
        boolean credentialsExists = existingCredentials != null;

        if (credentialsExists) {
            String passportMessage = "Пользователь с таким паспортом уже существует";

            request.setAttribute("passportMessage", passportMessage);
            request.setAttribute("staff", staff);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Private method for checking if login that was from request is already exists in database.
     *
     * @return true - if exists
     */
    private boolean loginAlreadyExistsInDataBase() {
        List<Registration> registrationList = dao.getRegistrationDAO().getByValue
                (Constants.REGISTRATION_LOGIN_COLUMN_NAME, registrationFromRequest.getLogin());
        boolean registrationExists = !registrationList.isEmpty();

        if (registrationExists) {
            String loginMessage = "Пользователь с таким логином уже существует";

            request.setAttribute("loginMessage", loginMessage);
            request.setAttribute("staff", staff);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Private method for inputting new data to database and committing changes.
     *
     * @see User
     * @see Registration
     * @see Credentials
     */
    private void inputAndCommitChanges() {

        registrationFromRequest.setPassword(CommandsUtils.generateRandomPassword());

        boolean insertionWasSuccessful = dao.insertFullStaffInfo(staff);
        if (insertionWasSuccessful) {
            logger.info(":inputAndCommitChanges() ->" +
                    " Insertion Was Successful. Staff : "
                    + staff);
        } else {
            logger.warn(":inputAndCommitChanges() -> " +
                    "Insertion Was NOT Successful. Staff:" +
                    staff);
        }

        CommandsUtils.sentEmailToUserWithRegistrationData(credentialsFromRequest,
                registrationFromRequest);

        request.setAttribute("userId", Long.valueOf("" + staff.getUser().getId()));
        request.setAttribute("staffId", Long.valueOf("" + staff.getId()));
        request.setAttribute("staff", staff);
    }


}