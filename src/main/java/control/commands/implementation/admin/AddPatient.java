package control.commands.implementation.admin;

import control.commands.abstraction.Command;
import control.controller.Controller;
import control.service.SendEmail;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.*;
import entity.enums.Role;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is for processing form with command "addPatient".
 */
public class AddPatient implements Command {

    private static final Logger logger = Logger.getLogger(AddPatient.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/jsp/addPatient.jsp";

    private static AddPatient command;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;
    private GetHospitalDAO dao;
    private User userFromRequest;
    private Registration registrationFromRequest;
    private Credentials credentialsFromRequest;
    private Patient patient;




    private AddPatient() {
    }

    public static AddPatient getInstance(Connection c,
                                         HttpServletRequest req,
                                         HttpServletResponse resp) {
        connection = c;
        request = req;
        response = resp;
        if (command == null) {
            command = new AddPatient();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info("execute()");
        dao = GetHospitalDAO.getInstance(connection);

        getUserAndRegistrationAndCredentialsFromRequest();

        if (loginAlreadyExistsInDataBase()
                || credentialsAlreadyExistsInDataBase()
                || userAlreadyExistsInDataBase()) {
            return REDIRECT_PAGE;
        } else {
            inputAndCommitChanges();
            return REDIRECT_PAGE;
        }
    }

    /**
     * Private method that combines in one place three different private methods.
     */
    private void getUserAndRegistrationAndCredentialsFromRequest() {
        userFromRequest = getUserFromRequest(request);

        registrationFromRequest = new Registration();
        registrationFromRequest.setLogin(request.getParameter("login"));

        credentialsFromRequest = getCredentialsFromRequest(request);

        userFromRequest.setCredentials(credentialsFromRequest);
        userFromRequest.setRegistration(registrationFromRequest);

        patient = new Patient();
        patient.setUser(userFromRequest);
    }

    /**
     * Method for checking if user that was from request is already exists in database.
     *
     * @return true - if exists
     */
    private boolean userAlreadyExistsInDataBase() {
        User existingUser = dao.getUserDAO().isExist(userFromRequest);
        boolean userExists = existingUser != null;

        if (userExists) {
            String loginMessage = "Пользователь с такими параметрами уже существует";
            request.setAttribute("loginMessage", loginMessage);
            request.setAttribute("patient", patient);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Private method for checking if credentials that was from request
     * is already exists in database.
     *
     * @return true - if exists
     */
    private boolean credentialsAlreadyExistsInDataBase() {
        Credentials existingCredentials = dao.getCredentialsDAO().isExist(credentialsFromRequest);
        boolean credentialsExists = existingCredentials != null;

        if (credentialsExists) {
            String passportMessage = "Пользователь с таким паспортом уже существует";

            request.setAttribute("passportMessage", passportMessage);
            request.setAttribute("patient", patient);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Private method for checking if login that was from request is already exists in database.
     *
     * @return true - if exists
     */
    private boolean loginAlreadyExistsInDataBase() {
        List<Registration> registrationList = dao.getRegistrationDAO().getByValue
                (Constants.REGISTRATION_LOGIN_COLUMN_NAME, registrationFromRequest.getLogin());
        boolean registrationExists = !registrationList.isEmpty();

        if (registrationExists) {
            String loginMessage = "Пользователь с таким логином уже существует";

            request.setAttribute("loginMessage", loginMessage);
            request.setAttribute("patient", patient);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Private method for getting user params from request and creating new User Object.
     *
     * @param request request
     * @return User
     * @see User
     * @see HttpServletRequest
     */
    private User getUserFromRequest(HttpServletRequest request) {
        String surname = request.getParameter("surname");
        String name = request.getParameter("name");
        String patronymic = request.getParameter("patronymic");
        Date dayOfBirth = Date.valueOf(request.getParameter("dayOfBirth"));
        Role role = Role.PATIENT;

        return new User(surname, name, patronymic, dayOfBirth, role);
    }

    /**
     * Private method for getting credentials params from request
     * and creating new Credentials Object.
     *
     * @param request request
     * @return Credentials
     * @see Credentials
     * @see HttpServletRequest
     */
    private Credentials getCredentialsFromRequest(HttpServletRequest request) {
        String passport = request.getParameter("passport");
        String email = request.getParameter("email");
        String phoneNumber = request.getParameter("phoneNumber");

        return new Credentials(passport, email, phoneNumber);
    }


    /**
     * Private method for inputting new data to database and committing changes.
     *
     * @see User
     * @see Registration
     * @see Credentials
     */
    private void inputAndCommitChanges() {

        registrationFromRequest.setPassword(generateRandomPassword());

        dao.insertFullPatientInfo(patient);
        logger.info("CHANGES COMMITTED. New Patient" + patient);

        try {
            SendEmail.mail(credentialsFromRequest.getEmail(),
                    registrationFromRequest.getLogin(),
                    registrationFromRequest.getPassword());
        } catch (MessagingException e) {
            e.printStackTrace();
        }

        request.setAttribute("userId", Long.valueOf("" + patient.getUser().getId()));
        request.setAttribute("patientId", Long.valueOf("" + patient.getId()));
        request.setAttribute("patient", patient);
    }

    private String generateRandomPassword() {
        int length = 6;
        boolean useLetters = true;
        boolean useNumbers = true;
        return RandomStringUtils.random(length, useLetters, useNumbers);
    }


}
