package control.commands.implementation.doctor;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.*;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

/**
 * This class is for processing form with command "allPatientsInfo".
 */
public class AllPatientsInfo implements Command {

    private static final Logger logger = Logger.getLogger(AllPatientsInfo.class);
    private static final String REDIRECT_PAGE = Controller.contextPath + "/jsp/doctor/allPatientsInfo.jsp";

    private static AllPatientsInfo command;

    private static HttpServletResponse response;
    private static HttpServletRequest request;
    private static Connection connection;
    private GetHospitalDAO dao;

    private Doctor doctor;
    private List<Task> taskList;

    private AllPatientsInfo() {
    }

    public static AllPatientsInfo getInstance(Connection conn,
                                              HttpServletRequest req,
                                              HttpServletResponse resp) {
        request = req;
        response = resp;
        connection = conn;
        if (command == null) {
            command = new AllPatientsInfo();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");

        dao = GetHospitalDAO.getInstance(connection);

        doctor = (Doctor) request.getSession().getAttribute("doctor");

        getAllDoctorTasksAndFillTasksWithFullInfo();

        request.setAttribute("taskList", taskList);

        return REDIRECT_PAGE;
    }


    /**
     * Private method that retrieves all task that were created(assigned) by doctor form request.
     * Also method fills tasks with full information about it, like surname of patient and doctor,
     * result - if they are exists; and so on.
     */
    private void getAllDoctorTasksAndFillTasksWithFullInfo() {
        logger.info(": getAllDoctorTasksAndFillTasksWithFullInfo()");

        int doctorId = doctor.getId();

        taskList = dao.getTaskDAO().getByKey(TASK_DOCTOR_ID_COLUMN_NAME, doctorId);
        logger.info(": taskList != null AND not empty -> "
                + (taskList != null && !taskList.isEmpty()));

        if (taskList != null && !taskList.isEmpty()) {
            for (Task task : taskList) {
                logger.info(": for");
                dao.fillTaskWithAllInfo(task);
            }
        }
    }
}
