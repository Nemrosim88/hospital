package control.commands.implementation.doctor;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.*;
import entity.enums.Type;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

public class DoctorTasks implements Command {

    private static DoctorTasks command;

    private final static Logger logger = Logger.getLogger(DoctorTasks.class);
    private static HttpServletResponse response;
    private static HttpServletRequest request;
    private static Connection connection;
    private GetHospitalDAO dao;
    private String redirectPage = Controller.contextPath + "/jsp/doctor/doctorTasks.jsp";


    private DoctorTasks() {
    }

    public static DoctorTasks getInstance(Connection conn,
                                          HttpServletRequest req,
                                          HttpServletResponse resp) {
        request = req;
        response = resp;
        connection = conn;
        if (command == null) {
            command = new DoctorTasks();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");

        dao = GetHospitalDAO.getInstance(connection);

        List<Task> taskList =
                dao.getTaskDAO().getByValue(TASK_TYPE_COLUMN_NAME, Type.OPERATION.toString());

        boolean listNotNullAndNotEmpty = taskList != null && !taskList.isEmpty();

        if (listNotNullAndNotEmpty) {
            for (Task task : taskList) {
                dao.fillTaskWithAllInfo(task);
            }
        }

        request.setAttribute("taskList", taskList);

        return redirectPage;
    }
}
