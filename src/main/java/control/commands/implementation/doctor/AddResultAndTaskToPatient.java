package control.commands.implementation.doctor;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.dao.GetHospitalDAO;
import entity.Result;
import entity.Task;
import entity.enums.Type;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;

/**
 * This class is for processing form with command "addResultAndTaskToPatient".
 */
public class AddResultAndTaskToPatient implements Command {

    private static final Logger logger = Logger.getLogger(AddResultAndTaskToPatient.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/jsp/doctor/doctorTasks.jsp";

    private static AddResultAndTaskToPatient command;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;
    private GetHospitalDAO dao;

    private Result result;
    private Task task;

    private AddResultAndTaskToPatient() {
    }

    public static AddResultAndTaskToPatient getInstance(Connection c,
                                                        HttpServletRequest req,
                                                        HttpServletResponse resp) {
        connection = c;
        request = req;
        response = resp;
        if (command == null) {
            command = new AddResultAndTaskToPatient();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info("execute()");
        dao = GetHospitalDAO.getInstance(connection);

        getParametersFromRequest();

        boolean insertionWasSuccessful = dao.insertOperationResultAndNewTask(result, task);

        if (insertionWasSuccessful) {
            int resultIdAfterInsert = result.getId();
            int taskIdAfterInsert = task.getId();

            String message = String.format("Insertion successful. Result ID: %d; Task ID: %d.",
                    resultIdAfterInsert, taskIdAfterInsert);
            request.setAttribute("message", message);
        } else {
            request.setAttribute("message", "ERROR!");
        }

        return REDIRECT_PAGE;
    }


    /**
     * Private method for getting all needed parameters from request.
     */
    private void getParametersFromRequest() {
        logger.info(": getParametersFromRequest()");

        //parameters for Result
        int doctorId = Integer.parseInt(request.getParameter("doctorId"));
        int taskId = Integer.parseInt(request.getParameter("taskId"));
        Timestamp dayOfOperation = getTimestampFromRequest();
        String resultInfo = request.getParameter("resultInfo");

        result = new Result(taskId, dayOfOperation, resultInfo, doctorId, null);


        //parameters for Task
        int patientId = Integer.parseInt(request.getParameter("patientId"));
        String taskInfo = request.getParameter("taskInfo");
        Type type = Type.ofString(request.getParameter("type"));
        Timestamp nowTimeForTask = new Timestamp(new Date().getTime());

        task = new Task(doctorId, patientId, taskInfo, nowTimeForTask, type);

        // log information about Result and Task
        String valuesFromRequest = String.format(
                "\nDOCTOR ID:'%s'; \n" +
                        "TASK ID:'%s'; \n" +
                        "DAY_OF_OPERATION ID:'%s'; \n" +
                        "RESULT_INFO ID:'%s'; \n" +
                        "PATIENT ID:'%s'; \n" +
                        "TASK_INFO:'%s'; \n " +
                        "TYPE:'%s';",
                doctorId,
                taskId,
                dayOfOperation,
                resultInfo,
                patientId,
                taskInfo,
                type);
        logger.info(": Values from request \"addResultAndTaskToPatient\"" + valuesFromRequest);
    }

    /**
     * Get Timestamp from request. String that returns from request is 'yyyy-mm-ddTHH:mm',
     * but for converting using Timestamp.valueOf() string should be yyyy-mm-dd HH:mm:ss.
     * After converting method returns new Timestamp.
     *
     * @return Timestamp
     * @see Timestamp
     */
    private Timestamp getTimestampFromRequest() {
        // строка запроса - yyyy-mm-ddTHH:mm
        // для удачной конвертации в Timestamp должно быть yyyy-mm-dd HH:mm:ss
        String day = request.getParameter("dayOfOperation") + ":00";
        String replace = day.replace('T', ' ');
        return Timestamp.valueOf(replace);
    }
}