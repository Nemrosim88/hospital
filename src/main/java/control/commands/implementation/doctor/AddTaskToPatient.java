package control.commands.implementation.doctor;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.dao.GetHospitalDAO;
import entity.*;
import entity.enums.Type;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;

/**
 * This class is for processing form with command "addTaskToPatient".
 */
public class AddTaskToPatient implements Command {

    private static final Logger logger = Logger.getLogger(AddTaskToPatient.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/jsp/doctor/patientInfo.jsp";

    private static AddTaskToPatient command;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;
    private GetHospitalDAO dao;

    private Task task;

    private AddTaskToPatient() {
    }

    public static AddTaskToPatient getInstance(Connection c,
                                               HttpServletRequest req,
                                               HttpServletResponse resp) {
        connection = c;
        request = req;
        response = resp;
        if (command == null) {
            command = new AddTaskToPatient();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info("execute()");
        dao = GetHospitalDAO.getInstance(connection);

        getParametersFromRequest();

        int taskIdAfterInsert = dao.getTaskDAO().insert(task);
        task.setId(taskIdAfterInsert);
        logger.info("TASK ID after insert to data base ->" + taskIdAfterInsert);

        return REDIRECT_PAGE;
    }

    /**
     * Private method for getting all needed parameters from request.
     */
    private void getParametersFromRequest() {
        int patientId = Integer.parseInt(request.getParameter("patientId"));
        int doctorId = Integer.parseInt(request.getParameter("doctorId"));
        String info = request.getParameter("info");
        Type type = Type.ofString(request.getParameter("type"));

        task = new Task(doctorId, patientId, info, new Timestamp(new Date().getTime()), type);
        logger.info("TASK from request ->" + task);
    }
}