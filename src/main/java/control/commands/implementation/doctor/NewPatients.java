package control.commands.implementation.doctor;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.dao.GetHospitalDAO;
import entity.Patient;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.util.List;

public class NewPatients implements Command {

    private final static Logger logger = Logger.getLogger(NewPatients.class);

    private static NewPatients command;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;
    private GetHospitalDAO dao;
    private final String redirectPage = Controller.contextPath + "/jsp/doctor/newPatients.jsp";


    private NewPatients() {
    }

    public static NewPatients getInstance(Connection c,
                                          HttpServletRequest req,
                                          HttpServletResponse resp) {
        connection = c;
        request = req;
        response = resp;
        if (command == null) {
            command = new NewPatients();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info("execute()");
        dao = GetHospitalDAO.getInstance(connection);

        List<Patient> allNewPatients = dao.getAllNewPatientsWithFullInfo();
        if (allNewPatients != null && !allNewPatients.isEmpty()) {
            request.setAttribute("newPatientsList", allNewPatients);
        } else {
            request.setAttribute("newPatientsList", null);
        }

        return redirectPage;
    }
}
