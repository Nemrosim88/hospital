package control.commands.implementation.doctor;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.Constants;
import data.dao.GetHospitalDAO;
import entity.*;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PatientInfo implements Command {

    private static final Logger logger = Logger.getLogger(PatientInfo.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/jsp/doctor/patientInfo.jsp";

    private static PatientInfo command;
    private static HttpServletRequest request;
    private static HttpServletResponse response;
    private static Connection connection;
    private GetHospitalDAO dao;
    private int patientId;
    private boolean lastTaskHasResult;
    private List<Task> patientTaskList;


    private PatientInfo() {
    }

    public static PatientInfo getInstance(Connection conn,
                                          HttpServletRequest req,
                                          HttpServletResponse resp) {
        connection = conn;
        request = req;
        response = resp;
        if (command == null) {
            command = new PatientInfo();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");

        dao = GetHospitalDAO.getInstance(connection);

        patientId = Integer.parseInt(request.getParameter("patientId"));
        Patient patient = dao.getAllInfoAboutPatient(patientId);

        getAllTasksOfPatientWithFullInformation();
        getAvailableDiagnosis();

        request.setAttribute("lastTasksClosed", lastTaskHasResult);
        request.setAttribute("patientTaskList", patientTaskList);
        request.setAttribute("patient", patient);

        return REDIRECT_PAGE;
    }

    /**
     * Method for retrieving all available diagnoses of patient with full information about
     * it, like about doctor's surname, name ect who set the diagnosis.
     */
    private void getAvailableDiagnosis() {
        List<Diagnosis> diagnosisList = dao.getDiagnosisDAO().getByKey
                (DIAGNOSIS_PATIENT_ID_COLUMN_NAME, patientId);

        if (diagnosisList != null && !diagnosisList.isEmpty()) {
            for (Diagnosis diagnosis : diagnosisList) {
                dao.fillDiagnosisWithAllInfo(diagnosis);
            }
            request.setAttribute("diagnosisList", diagnosisList);
        }
    }

    /**
     * Method for retrieving all available tasks of patient with full information about
     * it, like about doctor's surname, name ect who set the diagnosis and when.
     */
    private void getAllTasksOfPatientWithFullInformation() {
        patientTaskList = dao.getTaskDAO().getByKey(TASK_PATIENT_ID_COLUMN_NAME, patientId);

        for (Task task : patientTaskList) {
            dao.fillTaskWithAllInfo(task);
            Result result = task.getResult();
            lastTaskHasResult = result != null;
        }
    }

}
