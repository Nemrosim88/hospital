package control.commands.implementation.doctor;

import control.commands.abstraction.Command;
import control.controller.Controller;
import data.dao.GetHospitalDAO;
import entity.Diagnosis;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.Date;

public class DischargePatient implements Command {

    private final static Logger logger = Logger.getLogger(DischargePatient.class);
    private static final String REDIRECT_PAGE = Controller.contextPath
            + "/jsp/doctor/patientInfo.jsp";

    private static DischargePatient command;

    private static HttpServletResponse response;
    private static HttpServletRequest request;
    private static Connection connection;
    private GetHospitalDAO dao;
    private Diagnosis diagnosis;

    private DischargePatient() {
    }

    public static DischargePatient getInstance(Connection conn,
                                               HttpServletRequest req,
                                               HttpServletResponse resp) {
        request = req;
        response = resp;
        connection = conn;
        if (command == null) {
            command = new DischargePatient();
        }
        return command;
    }

    @Override
    public String execute() {
        logger.info(": execute()");

        dao = GetHospitalDAO.getInstance(connection);

        getDiagnosisFromRequest();

        boolean allWentWell = dao.insertFinalDiagnosisAndDischargePatient(diagnosis);

        if(allWentWell){
            String message = String.format("Diagnosis was inserted with ID:'%d' and patient was " +
                    "discharged", diagnosis.getId());
            request.setAttribute("message", message);
        } else {
            request.setAttribute("message", "Something went wrong");
        }

        return REDIRECT_PAGE;
    }

    /**
     * Method for retrieving request parameters and creating new Diagnosis with them.
     */
    private void getDiagnosisFromRequest() {
        int patientId = Integer.parseInt(request.getParameter("patientId"));
        int doctorId = Integer.parseInt(request.getParameter("doctorId"));
        String additionalInfo = request.getParameter("additionalInfo");
        String finalDiagnosis = request.getParameter("diagnosis");
        Timestamp dateTime = new Timestamp(new Date().getTime());

        diagnosis = new Diagnosis(patientId, doctorId, additionalInfo, dateTime, dateTime,
                finalDiagnosis);
    }
}
