package control.service;

import entity.Registration;
import org.apache.log4j.Logger;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

public class Cookies {

    private static final Logger logger = Logger.getLogger(Cookies.class);

    private Cookie login;

    public Cookies(HttpServletResponse response, Registration registration) {
        login = new Cookie("cookie_login", registration.getLogin());
        login.setDomain("localhost");
        logger.info("Created new cookies" + login.getName());

        //30 minutes
        login.setMaxAge(30 * 60);

        response.addCookie(login);
    }

    public static Cookie cookiesExist(HttpServletRequest request) {

        Cookie loginCookie = null;
        Cookie[] cookies = request.getCookies();
        logger.info(": COOKIES mass " + Arrays.toString(cookies));
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                logger.info(
                        String.format(": COOKIES mass NAME:'%s'; VALUE:'%s'",
                                cookie.getName(),
                                cookie.getValue()));
                if (cookie.getName().equals("cookie_login")) {
                    loginCookie = cookie;
                    return loginCookie;
                }
            }
        }

        return null;
    }
}
