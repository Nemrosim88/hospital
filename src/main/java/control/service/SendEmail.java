package control.service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {

    public SendEmail() {
    }

    /**
     * Method for sending email
     *
     * @throws MessagingException
     */
    public static void mail(String email, String login, String password) throws MessagingException {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.ukr.net");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("hospital_project@ukr.net",
                                "hospital12345");
                    }
                });

        try {
            String msg = "<head>\n" +
                    "Your registration data to hospital" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div>\n" +
                    "<h4>Your loging is:</h1>\n" +
                    "<h3>" + login + "</h1>\n" +
                    "<h4>Your password is:</h1>\n" +
                    "<h2>" + password + "</h1>\n" +

                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("hospital_project@ukr.net"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email));
            message.setContent(msg, "text/html; charset=utf-8");
            message.setSubject("Your have been registered to hospital.");
//            message.setText(msg);

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }


    }
}
