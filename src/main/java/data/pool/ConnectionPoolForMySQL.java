package data.pool;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionPoolForMySQL {

  private static ConnectionPoolForMySQL pool = null;
  private static DataSource dataSource = null;

  /**
   * Singleton.
   */
  private ConnectionPoolForMySQL() {
    try {
      InitialContext ic = new InitialContext();
      dataSource = (DataSource) ic.lookup("java:/comp/env/jdbc/mysql");
    } catch (NamingException e) {
      System.out.println(e);
    }
  }

  public static synchronized ConnectionPoolForMySQL getInstance() {
    if (pool == null) {
      pool = new ConnectionPoolForMySQL();
    }
    return pool;
  }

  public Connection getConnection() {
    try {
      return dataSource.getConnection();
    } catch (SQLException e) {
      System.out.println(e);
      return null;
    }
  }

  public void freeConnection(Connection c) {
    try {
      c.close();
    } catch (SQLException e) {
      System.out.println(e);
    }
  }

}
