package data.pool.abstraction;

import java.sql.Connection;

public interface ConnectionPool<E extends ConnectionPool> {

  public Connection getConnection();

  public void freeConnection(Connection connection);

}
