package data.dao.utils;

import data.dao.abstraction.EntityUtils;
import entity.User;
import entity.enums.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserUtils implements EntityUtils<User> {

    private static UserUtils utils;

    private UserUtils() {
    }

    public static UserUtils getInstance() {
        if (utils == null) {
            utils = new UserUtils();
        }
        return utils;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, USER_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName,
                                      int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s=?",
                SCHEMA_NAME,
                USER_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName,
                                        String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s=?",
                SCHEMA_NAME,
                USER_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection, User user) throws SQLException {
        String query = String.format("INSERT INTO %s.%s (%s, %s, %s, %s, %s) VALUES (?,?,?,?,?)",
                // INSERT ----------------
                SCHEMA_NAME,
                USER_TABLE_NAME,
                // COLUMN NAMES ----------
                USER_SURNAME_COLUMN_NAME,
                USER_NAME_COLUMN_NAME,
                USER_PATRONYMIC_COLUMN_NAME,
                USER_DAY_OF_BIRTH_COLUMN_NAME,
                USER_ROLE_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, user.getSurname());
        statement.setString(2, user.getName());
        statement.setString(3, user.getPatronymic());
        statement.setDate(4, user.getDayOfBirth());
        statement.setString(5, user.getRole().toString());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, User user) throws SQLException {
        String query =
                String.format("UPDATE %s.%s SET %s= ?, %s= ?, %s= ?, %s= ?, %s= ? WHERE id = ?;",
                        //UPDATE -----------------
                        SCHEMA_NAME,
                        USER_TABLE_NAME,
                        //SET --------------------
                        USER_SURNAME_COLUMN_NAME,
                        USER_NAME_COLUMN_NAME,
                        USER_PATRONYMIC_COLUMN_NAME,
                        USER_DAY_OF_BIRTH_COLUMN_NAME,
                        //WHERE ------------------
                        USER_ROLE_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, user.getSurname());
        statement.setString(2, user.getName());
        statement.setString(3, user.getPatronymic());
        statement.setDate(4, user.getDayOfBirth());
        statement.setString(5, user.getRole().toString());
        statement.setInt(6, user.getId());
        return statement;

    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                // DELETE  -----------
                SCHEMA_NAME,
                USER_TABLE_NAME,
                // WHERE -------------
                USER_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    @Override
    public PreparedStatement isExist(Connection connection, User entity) throws SQLException {
        String query =
                String.format("SELECT * FROM %s.%s WHERE %s=? AND %s=? AND %s=? AND %s=? AND %s=?;",
                        // SELECT --------------
                        SCHEMA_NAME,
                        USER_TABLE_NAME,
                        // WHERE ---------------
                        USER_SURNAME_COLUMN_NAME,
                        USER_NAME_COLUMN_NAME,
                        USER_PATRONYMIC_COLUMN_NAME,
                        USER_DAY_OF_BIRTH_COLUMN_NAME,
                        USER_ROLE_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getSurname());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getPatronymic());
        statement.setDate(4, entity.getDayOfBirth());
        statement.setString(5, entity.getRole().toString());
        return statement;
    }

    @Override
    public User getFromResultSet(ResultSet resultSet) throws SQLException {
        int enumIndex;
        String stringRole;
        Role role;
        try {
            enumIndex = resultSet.getInt(USER_ROLE_COLUMN_NAME);
            role = Role.ofStatusCode(enumIndex);
        } catch (Exception e) {
            stringRole = resultSet.getString(USER_ROLE_COLUMN_NAME);
            role = Role.ofString(stringRole);
        }
        return new User(
                resultSet.getInt(USER_ID_COLUMN_NAME),
                resultSet.getString(USER_SURNAME_COLUMN_NAME),
                resultSet.getString(USER_NAME_COLUMN_NAME),
                resultSet.getString(USER_PATRONYMIC_COLUMN_NAME),
                resultSet.getDate(USER_DAY_OF_BIRTH_COLUMN_NAME),
                role);
    }
}
