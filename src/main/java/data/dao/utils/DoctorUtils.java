package data.dao.utils;

import data.dao.abstraction.EntityUtils;
import entity.Doctor;

import java.sql.*;

public class DoctorUtils implements EntityUtils<Doctor> {

    private static DoctorUtils utils;

    private DoctorUtils() {
    }

    public static DoctorUtils getInstance() {
        if (utils == null) {
            utils = new DoctorUtils();
        }
        return utils;
    }

    @Override
    public Doctor getFromResultSet(ResultSet resultSet) throws SQLException {
        return new Doctor(
                resultSet.getInt(DOCTOR_ID_COLUMN_NAME),
                resultSet.getInt(DOCTOR_USER_ID_COLUMN_NAME),
                resultSet.getInt(DOCTOR_CARD_KEY_ID_COLUMN_NAME)
        );
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, DOCTOR_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName, int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                DOCTOR_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName, String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                DOCTOR_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection, Doctor entity) throws SQLException {
        String query = String.format("INSERT INTO %s.%s (%s, %s) VALUES (?,?)",
                SCHEMA_NAME,
                DOCTOR_TABLE_NAME,
                DOCTOR_USER_ID_COLUMN_NAME,
                DOCTOR_CARD_KEY_ID_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getUserId());
        statement.setInt(2, entity.getCardKeyId());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, Doctor entity) throws SQLException {
        String query = String.format("UPDATE %s.%s SET %s = ?, %s = ? WHERE %s = ?;",
                SCHEMA_NAME,
                DOCTOR_TABLE_NAME,
                DOCTOR_USER_ID_COLUMN_NAME,
                DOCTOR_CARD_KEY_ID_COLUMN_NAME,
                DOCTOR_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getUserId());
        statement.setInt(2, entity.getCardKeyId());
        statement.setInt(3, entity.getId());
        return statement;
    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                DOCTOR_TABLE_NAME,
                DOCTOR_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    @Override
    public PreparedStatement isExist(Connection connection, Doctor entity) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ? AND %s = ?;",
                SCHEMA_NAME,
                DOCTOR_TABLE_NAME,
                DOCTOR_USER_ID_COLUMN_NAME,
                DOCTOR_CARD_KEY_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getUserId());
        statement.setInt(2, entity.getCardKeyId());
        return statement;
    }
}
