package data.dao.utils;

import data.dao.abstraction.EntityUtils;
import entity.Diagnosis;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DiagnosisUtils implements EntityUtils<Diagnosis> {

    private static DiagnosisUtils utils;

    private DiagnosisUtils() {
    }

    public static DiagnosisUtils getInstance() {
        if (utils == null) {
            utils = new DiagnosisUtils();
        }
        return utils;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, DIAGNOSIS_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName, int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                DIAGNOSIS_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName, String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                DIAGNOSIS_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection conn,
                                    Diagnosis diagnosis) throws SQLException {
        String query = String.format("INSERT INTO %s.%s (%s,%s,%s,%s,%s,%s) VALUES (?,?,?,?,?,?)",
                SCHEMA_NAME,
                DIAGNOSIS_TABLE_NAME,
                DIAGNOSIS_PATIENT_ID_COLUMN_NAME,
                DIAGNOSIS_DOCTOR_ID_COLUMN_NAME,
                DIAGNOSIS_INFO_COLUMN_NAME,
                DIAGNOSIS_START_COLUMN_NAME,
                DIAGNOSIS_END_COLUMN_NAME,
                DIAGNOSIS_FINAL_DIAGNOSIS_COLUMN_NAME);
        PreparedStatement statement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, diagnosis.getPatientId());
        statement.setInt(2, diagnosis.getDoctorId());
        statement.setString(3, diagnosis.getInfo());
        statement.setTimestamp(4, diagnosis.getStart());
        statement.setTimestamp(5, diagnosis.getEnd());
        statement.setString(6, diagnosis.getFinalDiagnosis());

        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection,
                                    Diagnosis diagnosis) throws SQLException {
        final String query = String.format("UPDATE %s.%s SET %s=?, %s=?, %s=?, %s=?, %s=?, %s=? " +
                        "WHERE %s=?;",
                SCHEMA_NAME,
                DIAGNOSIS_TABLE_NAME,
                // ====== SET
                DIAGNOSIS_PATIENT_ID_COLUMN_NAME,
                DIAGNOSIS_DOCTOR_ID_COLUMN_NAME,
                DIAGNOSIS_INFO_COLUMN_NAME,
                DIAGNOSIS_START_COLUMN_NAME,
                DIAGNOSIS_END_COLUMN_NAME,
                DIAGNOSIS_FINAL_DIAGNOSIS_COLUMN_NAME,
                // // ====== WHERE
                DIAGNOSIS_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, diagnosis.getPatientId());
        statement.setInt(2, diagnosis.getDoctorId());
        statement.setString(3, diagnosis.getInfo());
        statement.setTimestamp(4, diagnosis.getStart());
        statement.setTimestamp(5, diagnosis.getEnd());
        statement.setString(6, diagnosis.getFinalDiagnosis());
        statement.setInt(7, diagnosis.getId());
        return statement;
    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                DIAGNOSIS_TABLE_NAME,

                DIAGNOSIS_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    @Override
    public PreparedStatement isExist(Connection connection, Diagnosis entity) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s=? AND %s=? AND %s=? AND %s = ?;",
                SCHEMA_NAME,
                DIAGNOSIS_TABLE_NAME,

                DIAGNOSIS_PATIENT_ID_COLUMN_NAME,
                DIAGNOSIS_DOCTOR_ID_COLUMN_NAME,
                DIAGNOSIS_INFO_COLUMN_NAME,
                DIAGNOSIS_END_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getPatientId());
        statement.setInt(2, entity.getDoctorId());
        statement.setString(3, entity.getInfo());
        statement.setTimestamp(4, entity.getEnd());
        return statement;
    }

    @Override
    public Diagnosis getFromResultSet(ResultSet resultSet) throws SQLException {
        return new Diagnosis(
                resultSet.getInt(DIAGNOSIS_ID_COLUMN_NAME),
                resultSet.getInt(DIAGNOSIS_PATIENT_ID_COLUMN_NAME),
                resultSet.getInt(DIAGNOSIS_DOCTOR_ID_COLUMN_NAME),
                resultSet.getString(DIAGNOSIS_INFO_COLUMN_NAME),
                resultSet.getTimestamp(DIAGNOSIS_START_COLUMN_NAME),
                resultSet.getTimestamp(DIAGNOSIS_END_COLUMN_NAME),
                resultSet.getString(DIAGNOSIS_FINAL_DIAGNOSIS_COLUMN_NAME)
        );
    }
}
