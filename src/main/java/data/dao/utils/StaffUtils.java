package data.dao.utils;


import data.dao.abstraction.EntityUtils;
import entity.Staff;
import entity.enums.Position;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class StaffUtils implements EntityUtils<Staff> {

    private static StaffUtils utils;

    private StaffUtils() {
    }

    public static StaffUtils getInstance() {
        if (utils == null) {
            utils = new StaffUtils();
        }
        return utils;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, STAFF_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName,
                                      int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                STAFF_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName,
                                        String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                STAFF_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection, Staff staff) throws SQLException {
        String query = String.format("INSERT INTO %s.%s (%s, %s) VALUES (?,?)",
                SCHEMA_NAME,
                STAFF_TABLE_NAME,
                STAFF_USER_ID_COLUMN_NAME,
                STAFF_POSITION_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, staff.getUserId());
        statement.setString(2, staff.getPosition().toString());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, Staff staff) throws SQLException {
        final String query = String.format("UPDATE %s.%s SET %s = ?, %s = ? WHERE %s = ?;",
                SCHEMA_NAME,
                STAFF_TABLE_NAME,
                STAFF_USER_ID_COLUMN_NAME,
                STAFF_POSITION_COLUMN_NAME,
                STAFF_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, staff.getUserId());
        statement.setString(2, staff.getPosition().toString());
        statement.setInt(3, staff.getId());
        return statement;
    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                STAFF_TABLE_NAME,
                STAFF_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    @Override
    public PreparedStatement isExist(Connection connection, Staff entity) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ? AND %s = ?;",
                SCHEMA_NAME,
                STAFF_TABLE_NAME,
                STAFF_USER_ID_COLUMN_NAME,
                STAFF_POSITION_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getUserId());
        statement.setString(2, entity.getPosition().toString());
        return statement;
    }

    @Override
    public Staff getFromResultSet(ResultSet resultSet) throws SQLException {
        int enumIndex;
        String stringRole;
        Position position;
        try {
            enumIndex = resultSet.getInt(STAFF_POSITION_COLUMN_NAME);
            position = Position.ofStatusCode(enumIndex);
        } catch (Exception e) {
            stringRole = resultSet.getString(STAFF_POSITION_COLUMN_NAME);
            position = Position.ofString(stringRole);
        }
        return new Staff(
                resultSet.getInt(STAFF_ID_COLUMN_NAME),
                resultSet.getInt(STAFF_USER_ID_COLUMN_NAME),
                position
        );
    }


}
