package data.dao.utils;

import data.dao.abstraction.EntityUtils;
import entity.Patient;

import java.sql.*;

public class PatientUtils implements EntityUtils<Patient> {

    private static PatientUtils utils;

    private PatientUtils() {
    }

    public static PatientUtils getInstance() {
        if (utils == null) {
            utils = new PatientUtils();
        }
        return utils;
    }

    @Override
    public Patient getFromResultSet(ResultSet resultSet) throws SQLException {
        return new Patient(
                resultSet.getInt(PATIENT_ID_COLUMN_NAME),
                resultSet.getInt(PATIENT_USER_ID_COLUMN_NAME),
                resultSet.getBoolean(PATIENT_DISCHARGE_COLUMN_NAME)
        );
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, PATIENT_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName, int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                PATIENT_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName, String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                PATIENT_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection, Patient entity) throws SQLException {
        String query = String.format("INSERT INTO %s.%s (%s, %s) VALUES (?,?)",
                SCHEMA_NAME,
                PATIENT_TABLE_NAME,
                PATIENT_USER_ID_COLUMN_NAME,
                PATIENT_DISCHARGE_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getUserId());
        statement.setBoolean(2, entity.getDischarge());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, Patient entity) throws SQLException {
        String query = String.format("UPDATE %s.%s SET %s = ?, %s = ? WHERE %s = ?;",
                SCHEMA_NAME,
                PATIENT_TABLE_NAME,
                PATIENT_USER_ID_COLUMN_NAME,
                PATIENT_DISCHARGE_COLUMN_NAME,
                PATIENT_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getUserId());
        statement.setBoolean(2, entity.getDischarge());
        statement.setInt(3, entity.getId());
        return statement;
    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                PATIENT_TABLE_NAME,
                PATIENT_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    @Override
    public PreparedStatement isExist(Connection connection, Patient entity) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                PATIENT_TABLE_NAME,
                PATIENT_USER_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getUserId());
        return statement;
    }
}
