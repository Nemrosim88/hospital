package data.dao.utils;

import data.dao.abstraction.EntityUtils;
import entity.Task;
import entity.enums.Type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TaskUtils implements EntityUtils<Task> {

    private static TaskUtils utils;

    private TaskUtils() {
    }

    public static TaskUtils getInstance() {
        if (utils == null) {
            utils = new TaskUtils();
        }
        return utils;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, TASK_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName,
                                      int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                TASK_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName,
                                        String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                TASK_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection, Task task) throws SQLException {
        String query = String.format("INSERT INTO %s.%s (%s, %s, %s, %s, %s) VALUES (?,?,?,?,?)",
                SCHEMA_NAME,
                TASK_TABLE_NAME,
                TASK_DOCTOR_ID_COLUMN_NAME,
                TASK_PATIENT_ID_COLUMN_NAME,
                TASK_INFO_COLUMN_NAME,
                TASK_DATETIME_COLUMN_NAME,
                TASK_TYPE_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, task.getDoctorId());
        statement.setInt(2, task.getPatientId());
        statement.setString(3, task.getInfo());
        statement.setTimestamp(4, task.getDatetime());
        statement.setString(5, task.getType().toString());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, Task task) throws SQLException {
        final String query =
                String.format("UPDATE %s.%s SET %s= ?, %s= ?, %s= ?, %s= ?, %s = ? WHERE %s = ?;",
                SCHEMA_NAME,
                TASK_TABLE_NAME,
                TASK_DOCTOR_ID_COLUMN_NAME,
                TASK_PATIENT_ID_COLUMN_NAME,
                TASK_INFO_COLUMN_NAME,
                TASK_DATETIME_COLUMN_NAME,
                TASK_TYPE_COLUMN_NAME,
                TASK_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, task.getDoctorId());
        statement.setInt(2, task.getPatientId());
        statement.setString(3, task.getInfo());
        statement.setTimestamp(4, task.getDatetime());
        statement.setString(5, task.getType().toString());
        statement.setInt(6, task.getId());
        return statement;
    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                TASK_TABLE_NAME,
                TASK_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    @Override
    public PreparedStatement isExist(Connection connection, Task entity) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ? AND %s = ? AND %s = ?;",
                SCHEMA_NAME,
                TASK_TABLE_NAME,
                TASK_DOCTOR_ID_COLUMN_NAME,
                TASK_PATIENT_ID_COLUMN_NAME,
                TASK_INFO_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getDoctorId());
        statement.setInt(2, entity.getPatientId());
        statement.setString(3, entity.getInfo());
        return statement;
    }

    @Override
    public Task getFromResultSet(ResultSet resultSet) throws SQLException {
        int enumIndex;
        String stringRole;
        Type type;
        try {
            enumIndex = resultSet.getInt(TASK_TYPE_COLUMN_NAME);
            type = Type.ofStatusCode(enumIndex);
        } catch (Exception e) {
            stringRole = resultSet.getString(TASK_TYPE_COLUMN_NAME);
            type = Type.ofString(stringRole);
        }

        return new Task(
                resultSet.getInt(TASK_ID_COLUMN_NAME),
                resultSet.getInt(TASK_DOCTOR_ID_COLUMN_NAME),
                resultSet.getInt(TASK_PATIENT_ID_COLUMN_NAME),
                resultSet.getString(TASK_INFO_COLUMN_NAME),
                resultSet.getTimestamp(TASK_DATETIME_COLUMN_NAME),
                type);
    }

}
