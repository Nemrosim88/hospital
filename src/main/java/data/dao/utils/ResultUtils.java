package data.dao.utils;

import data.dao.abstraction.EntityUtils;
import entity.Result;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ResultUtils implements EntityUtils<Result> {

    private static ResultUtils utils;

    private ResultUtils() {
    }

    public static ResultUtils getInstance() {
        if (utils == null) {
            utils = new ResultUtils();
        }
        return utils;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, RESULT_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection,
                                      String keyName,
                                      int key) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                RESULT_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName,
                                        String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                RESULT_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection,
                                    Result result) throws SQLException {
        String query = String.format("INSERT INTO %s.%s (%s, %s, %s, %s, %s) VALUES (?,?,?,?,?)",
                SCHEMA_NAME,
                RESULT_TABLE_NAME,
                RESULT_TASK_ID_COLUMN_NAME,
                RESULT_DATETIME_COLUMN_NAME,
                RESULT_INFO_COLUMN_NAME,
                RESULT_DOCTOR_ID_COLUMN_NAME,
                RESULT_STAFF_ID_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, result.getTaskId());
        statement.setTimestamp(2, result.getDatetime());
        statement.setString(3, result.getInfo());
        statement.setObject(4, result.getDoctorId());
        statement.setObject(5, result.getStaffId());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, Result result) throws SQLException {
        final String query =
                String.format("UPDATE %s.%s SET %s =?, %s =?, %s =?, %s =?, %s =? WHERE %s = ?;",
                        SCHEMA_NAME,
                        RESULT_TABLE_NAME,
                        RESULT_TASK_ID_COLUMN_NAME,
                        RESULT_DATETIME_COLUMN_NAME,
                        RESULT_INFO_COLUMN_NAME,
                        RESULT_DOCTOR_ID_COLUMN_NAME,
                        RESULT_STAFF_ID_COLUMN_NAME,
                        RESULT_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, result.getTaskId());
        statement.setTimestamp(2, result.getDatetime());
        statement.setString(3, result.getInfo());
        statement.setObject(4, result.getDoctorId());
        statement.setInt(5, result.getStaffId());
        statement.setInt(6, result.getId());
        return statement;
    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                RESULT_TABLE_NAME,
                RESULT_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    @Override
    public PreparedStatement isExist(Connection connection, Result entity) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ? AND %s = ?;",
                SCHEMA_NAME,
                RESULT_TABLE_NAME,
                RESULT_TASK_ID_COLUMN_NAME,
                RESULT_INFO_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, entity.getTaskId());
        statement.setString(2, entity.getInfo());
        return statement;
    }

    @Override
    public Result getFromResultSet(ResultSet resultSet) throws SQLException {
        return new Result(
                resultSet.getInt(RESULT_ID_COLUMN_NAME),
                resultSet.getInt(RESULT_TASK_ID_COLUMN_NAME),
                resultSet.getTimestamp(RESULT_DATETIME_COLUMN_NAME),
                resultSet.getString(RESULT_INFO_COLUMN_NAME),
                (Integer) resultSet.getObject(RESULT_DOCTOR_ID_COLUMN_NAME),
                (Integer) resultSet.getObject(RESULT_STAFF_ID_COLUMN_NAME)
        );
    }

}
