package data.dao.utils;

import data.dao.abstraction.EntityUtils;
import entity.Credentials;

import java.sql.*;

public class CredentialsUtils implements EntityUtils<Credentials> {

    private static CredentialsUtils utils;

    private CredentialsUtils() {
    }

    public static CredentialsUtils getInstance() {
        if (utils == null) {
            utils = new CredentialsUtils();
        }
        return utils;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, CREDENTIALS_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection, String keyName, int key) throws
            SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                CREDENTIALS_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName,
                                        String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                CREDENTIALS_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection conn, Credentials credentials) throws
            SQLException {
        String query = String.format("INSERT INTO %s.%s (%s, %s, %s, %s) VALUES (?,?,?,?)",
                SCHEMA_NAME,
                CREDENTIALS_TABLE_NAME,
                CREDENTIALS_USER_ID_COLUMN_NAME,
                CREDENTIALS_PASSPORT_COLUMN_NAME,
                CREDENTIALS_EMAIL_COLUMN_NAME,
                CREDENTIALS_PHONE_NUMBER_COLUMN_NAME);
        PreparedStatement statement = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, credentials.getUserId());
        statement.setString(2, credentials.getPassport());
        statement.setString(3, credentials.getEmail());
        statement.setString(4, credentials.getPhoneNumber());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, Credentials credentials) throws
            SQLException {
        String query = String.format("UPDATE %s.%s SET %s = ?, %s = ?, %s = ? WHERE %s = ?;",
                SCHEMA_NAME,
                CREDENTIALS_TABLE_NAME,
                CREDENTIALS_PASSPORT_COLUMN_NAME,
                CREDENTIALS_EMAIL_COLUMN_NAME,
                CREDENTIALS_PHONE_NUMBER_COLUMN_NAME,
                CREDENTIALS_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, credentials.getPassport());
        statement.setString(2, credentials.getEmail());
        statement.setString(3, credentials.getPhoneNumber());
        statement.setInt(4, credentials.getUserId());
        return statement;
    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws
            SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                CREDENTIALS_TABLE_NAME,
                CREDENTIALS_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    @Override
    public PreparedStatement isExist(Connection connection,
                                     Credentials entity) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ? AND %s = ? AND %s = ?;",
                SCHEMA_NAME,
                CREDENTIALS_TABLE_NAME,
                CREDENTIALS_PASSPORT_COLUMN_NAME,
                CREDENTIALS_EMAIL_COLUMN_NAME,
                CREDENTIALS_PHONE_NUMBER_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, entity.getPassport());
        statement.setString(2, entity.getEmail());
        statement.setString(3, entity.getPhoneNumber());
        return statement;
    }

    @Override
    public Credentials getFromResultSet(ResultSet resultSet) throws SQLException {
        return new Credentials(
                resultSet.getInt(CREDENTIALS_ID_COLUMN_NAME),
                resultSet.getInt(CREDENTIALS_USER_ID_COLUMN_NAME),
                resultSet.getString(CREDENTIALS_PASSPORT_COLUMN_NAME),
                resultSet.getString(CREDENTIALS_EMAIL_COLUMN_NAME),
                resultSet.getString(CREDENTIALS_PHONE_NUMBER_COLUMN_NAME)
        );
    }

}
