package data.dao.utils;

import control.commands.implementation.Login;
import data.dao.abstraction.EntityUtils;
import entity.Registration;
import org.apache.log4j.Logger;

import java.sql.*;

public class RegistrationUtils implements EntityUtils<Registration> {

    private static RegistrationUtils utils;
    private final static Logger logger = Logger.getLogger(RegistrationUtils.class);

    private RegistrationUtils() {
    }

    public static RegistrationUtils getInstance() {
        if (utils == null) {
            utils = new RegistrationUtils();
        }
        return utils;
    }

    @Override
    public String getAll() {
        return String.format("SELECT * FROM %s.%s", SCHEMA_NAME, REGISTRATION_TABLE_NAME);
    }

    @Override
    public PreparedStatement getByKey(Connection connection, String keyName, int key) throws
            SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                REGISTRATION_TABLE_NAME,
                keyName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, key);
        return statement;
    }

    @Override
    public PreparedStatement getByValue(Connection connection,
                                        String valueName,
                                        String value) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ?",
                SCHEMA_NAME,
                REGISTRATION_TABLE_NAME,
                valueName);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, value);
        return statement;
    }

    @Override
    public PreparedStatement insert(Connection connection, Registration registration) throws
            SQLException {
        String query = String.format("INSERT INTO %s.%s (%s, %s, %s) VALUES (?,?,?)",
                SCHEMA_NAME,
                REGISTRATION_TABLE_NAME,
                REGISTRATION_USER_ID_COLUMN_NAME,
                REGISTRATION_LOGIN_COLUMN_NAME,
                REGISTRATION_PASSWORD_COLUMN_NAME);
        PreparedStatement statement =
                connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, registration.getUserId());
        statement.setString(2, registration.getLogin());
        statement.setString(3, registration.getPassword());
        return statement;
    }

    @Override
    public PreparedStatement update(Connection connection, Registration registration) throws
            SQLException {
        final String query = String.format("UPDATE %s.%s SET %s=?, %s = ?, %s = ? WHERE %s = ?;",
                SCHEMA_NAME,
                REGISTRATION_TABLE_NAME,
                REGISTRATION_USER_ID_COLUMN_NAME,
                REGISTRATION_LOGIN_COLUMN_NAME,
                REGISTRATION_PASSWORD_COLUMN_NAME,
                REGISTRATION_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, registration.getUserId());
        statement.setString(2, registration.getLogin());
        statement.setString(3, registration.getPassword());
        statement.setInt(4, registration.getId());
        return statement;
    }

    @Override
    public PreparedStatement delete(Connection connection, int id) throws
            SQLException {
        String query = String.format("DELETE FROM %s.%s WHERE %s = ?;",
                SCHEMA_NAME,
                REGISTRATION_TABLE_NAME,
                REGISTRATION_ID_COLUMN_NAME);
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, id);
        return statement;
    }

    /**
     * If such login exists in DB.
     *
     * @param connection connection
     * @param entity     entity
     * @return PreparedStatement
     * @throws SQLException
     */
    @Override
    public PreparedStatement isExist(Connection connection,
                                     Registration entity) throws SQLException {
        String query = String.format("SELECT * FROM %s.%s WHERE %s = ? AND %s = ?;",
                SCHEMA_NAME,
                REGISTRATION_TABLE_NAME,
                REGISTRATION_LOGIN_COLUMN_NAME,
                REGISTRATION_PASSWORD_COLUMN_NAME);
        if (connection != null) {
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, entity.getLogin());
            statement.setString(2, entity.getPassword());
            return statement;
        } else {
            logger.error(": isExist() -> connection = null  !!");
            return null;
        }

    }

    @Override
    public Registration getFromResultSet(ResultSet resultSet) throws SQLException {
        return new Registration(
                resultSet.getInt(REGISTRATION_ID_COLUMN_NAME),
                resultSet.getInt(REGISTRATION_USER_ID_COLUMN_NAME),
                resultSet.getString(REGISTRATION_LOGIN_COLUMN_NAME),
                resultSet.getString(REGISTRATION_PASSWORD_COLUMN_NAME));
    }

}
