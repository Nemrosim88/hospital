package data.dao;

import data.Constants;
import data.dao.implementation.HospitalDAO;
import data.dao.utils.*;
import entity.*;
import entity.enums.Type;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GetHospitalDAO implements Constants {

    private final static Logger logger = Logger.getLogger(GetHospitalDAO.class);
    private static Connection connection;
    private static GetHospitalDAO dao;


    private GetHospitalDAO() {
    }

    public static GetHospitalDAO getInstance(Connection c) {
        connection = c;
        if (dao == null) {
            dao = new GetHospitalDAO();
        }
        return dao;
    }

    public HospitalDAO<User> getUserDAO() {
        return new HospitalDAO<>(connection, UserUtils.getInstance());
    }

    public HospitalDAO<Credentials> getCredentialsDAO() {
        return new HospitalDAO<>(connection, CredentialsUtils.getInstance());
    }

    public HospitalDAO<Registration> getRegistrationDAO() {
        return new HospitalDAO<>(connection, RegistrationUtils.getInstance());
    }

    public HospitalDAO<Patient> getPatientDAO() {
        return new HospitalDAO<>(connection, PatientUtils.getInstance());
    }

    public HospitalDAO<Doctor> getDoctorDAO() {
        return new HospitalDAO<>(connection, DoctorUtils.getInstance());
    }

    public HospitalDAO<Staff> getStaffDAO() {
        return new HospitalDAO<>(connection, StaffUtils.getInstance());
    }

    public HospitalDAO<Diagnosis> getDiagnosisDAO() {
        return new HospitalDAO<>(connection, DiagnosisUtils.getInstance());
    }

    public HospitalDAO<Result> getResultDAO() {
        return new HospitalDAO<>(connection, ResultUtils.getInstance());
    }

    public HospitalDAO<Task> getTaskDAO() {
        return new HospitalDAO<>(connection, TaskUtils.getInstance());
    }

    public boolean insertFullPatientInfo(Patient patient) {

        try {
            connection.setAutoCommit(false);
            connection.setSavepoint();

            Integer userId = dao.getUserDAO().insert(patient.getUser());


            Registration registration = patient.getUser().getRegistration();
            registration.setUserId(userId);
            dao.getRegistrationDAO().insert(registration);

            Credentials credentials = patient.getUser().getCredentials();
            credentials.setUserId(userId);
            dao.getCredentialsDAO().insert(credentials);

            patient.setUserId(userId);
            patient.getUser().setId(userId);

            Integer patientId = dao.getPatientDAO().insert(patient);
            patient.setId(patientId);

            connection.commit();
            return true;

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
                logger.error(e.getLocalizedMessage());
            }
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public boolean insertFullUserInfo(User user) {
        logger.info(": insertFullUserInfo()");

        try {
            connection.setAutoCommit(false);
            connection.setSavepoint();

            Integer userId = dao.getUserDAO().insert(user);
            user.setId(userId);

            Registration registration = user.getRegistration();
            registration.setUserId(userId);
            dao.getRegistrationDAO().insert(registration);

            Credentials credentials = user.getCredentials();
            credentials.setUserId(userId);
            dao.getCredentialsDAO().insert(credentials);

            connection.commit();
            return true;

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
                logger.error(e.getLocalizedMessage());
            }
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public boolean insertFullDoctorInfo(Doctor doctor) {

        try {
            connection.setAutoCommit(false);
            connection.setSavepoint();

            User user = doctor.getUser();
            Integer userId = dao.getUserDAO().insert(user);
            user.setId(userId);
            doctor.setUserId(userId);

            Registration registration = user.getRegistration();
            registration.setUserId(userId);
            dao.getRegistrationDAO().insert(registration);

            Credentials credentials = user.getCredentials();
            credentials.setUserId(userId);
            dao.getCredentialsDAO().insert(credentials);

            Integer doctorId = dao.getDoctorDAO().insert(doctor);
            doctor.setId(doctorId);

            connection.commit();
            return true;

        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
                logger.error(e.getLocalizedMessage());
            }
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public boolean insertFullStaffInfo(Staff staff) {

        try {
            connection.setAutoCommit(false);
            connection.setSavepoint();

            User user = staff.getUser();
            Integer userId = dao.getUserDAO().insert(user);
            user.setId(userId);

            Registration registration = user.getRegistration();
            registration.setUserId(userId);
            int registrationId = dao.getRegistrationDAO().insert(registration);

            Credentials credentials = user.getCredentials();
            credentials.setUserId(userId);
            int credentialsId = dao.getCredentialsDAO().insert(credentials);

            staff.setUserId(userId);
            Integer staffId = dao.getStaffDAO().insert(staff);
            staff.setId(staffId);

            if (userId != -1 && registrationId != -1 && credentialsId != -1) {
                connection.commit();
                return true;
            } else {
                connection.rollback();
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Get all information about task: patient's and doctors's
     * (surname, name, passport, phone number, ect)
     *
     * @param task task
     * @return
     */
    public void fillTaskWithAllInfo(Task task) {

        // Set to task ALL info about patient
        int patientId = task.getPatientId();
        task.setPatient(getAllInfoAboutPatient(patientId));

        // Set to task ALL info about doctor
        int doctorId = task.getDoctorId();
        task.setDoctor(getAllInfoAboutDoctor(doctorId));

        task.setResult(getAllInfoAboutTaskResults(task));

    }

    public void fillDiagnosisWithAllInfo(Diagnosis diagnosis) {

        // Set to diagnosis ALL info about patient
        int patientId = diagnosis.getPatientId();
        diagnosis.setPatient(getAllInfoAboutPatient(patientId));

        // Set to diagnosis ALL info about doctor
        int doctorId = diagnosis.getDoctorId();
        diagnosis.setDoctor(getAllInfoAboutDoctor(doctorId));
    }

    /**
     * Get all patient information
     * (USER: surname, name, ect; CREDENTIAL: passport, phoneNumber, ect).
     *
     * @param patientId patientId
     * @return Patient
     * @see Patient
     */
    public Patient getAllInfoAboutPatient(int patientId) {
        Patient patient = getPatientDAO().getByKey(PATIENT_ID_COLUMN_NAME, patientId).get(0);

        // Can't be Patient without User info
        User user = getUserDAO().getByKey(USER_ID_COLUMN_NAME, patient.getUserId()).get(0);
        patient.setUser(user);

        List<Credentials> credentialsList =
                getCredentialsDAO().getByKey(CREDENTIALS_USER_ID_COLUMN_NAME, user.getId());

        // User can have no credentials
        if (credentialsList != null && !credentialsList.isEmpty()) {
            Credentials credentials = credentialsList.get(0);
            user.setCredentials(credentials);
        }

        return patient;
    }

    /**
     * Get all doctor information (USER: surname, name, ect; CREDENTIAL: passport, phoneNumber, ect)
     *
     * @param doctorId doctorID
     * @return Doctor
     * @see Doctor
     */
    public Doctor getAllInfoAboutDoctor(int doctorId) {

        Doctor doctor = getDoctorDAO().getByKey(PATIENT_ID_COLUMN_NAME, doctorId).get(0);

        // Can't be Doctor without User info
        User user = getUserDAO().getByKey(USER_ID_COLUMN_NAME, doctor.getUserId()).get(0);
        doctor.setUser(user);

        // User can have no credentials
        List<Credentials> credentialsList =
                getCredentialsDAO().getByKey(CREDENTIALS_USER_ID_COLUMN_NAME, user.getId());

        // Uses can have no credentials
        if (credentialsList != null && !credentialsList.isEmpty()) {
            Credentials credentials = credentialsList.get(0);
            user.setCredentials(credentials);
        }

        return doctor;
    }

    /**
     * Get all staff information (USER: surname, name, ect; CREDENTIAL: passport, phoneNumber, ect)
     *
     * @param staffId staffId
     * @return Staff
     * @see Staff
     */
    public Staff getAllInfoAboutStaff(int staffId) {

        Staff staff = getStaffDAO().getByKey(STAFF_ID_COLUMN_NAME, staffId).get(0);

        // Can't be Staff without User info
        User user = getUserDAO().getByKey(USER_ID_COLUMN_NAME, staff.getUserId()).get(0);

        // User can have no credentials
        List<Credentials> credentialsList =
                getCredentialsDAO().getByKey(CREDENTIALS_USER_ID_COLUMN_NAME, user.getId());

        // Uses can have no credentials
        if (credentialsList != null && !credentialsList.isEmpty()) {
            Credentials credentials = credentialsList.get(0);
            user.setCredentials(credentials);
        }

        staff.setUser(user);

        return staff;
    }

    /**
     * Get all task results (if available)
     *
     * @param task Task
     * @return Result
     * @see Result
     */
    public Result getAllInfoAboutTaskResults(Task task) {

        List<Result> resultList =
                dao.getResultDAO().getByKey(RESULT_TASK_ID_COLUMN_NAME, task.getId());

        // If there any result?
        Result result = null;
        if (resultList != null && !resultList.isEmpty()) {
            result = resultList.get(0);

            Integer doctorId = result.getDoctorId();
            if (doctorId != null) {
                Doctor doctor = getAllInfoAboutDoctor(doctorId);
                result.setDoctor(doctor);
            }

            Integer staffId = result.getStaffId();
            if (staffId != null) {
                Staff staff = getAllInfoAboutStaff(staffId);
                result.setStaff(staff);
            }
        }
        return result;
    }

    /**
     * Метод возвращает список новых пациентов которым не назначено никаких задач.
     *
     * @return
     */
    public List<Patient> getAllNewPatientsWithFullInfo() {

        List<Task> taskList = getTaskDAO().getAll();
        List<Patient> patientList = getPatientDAO().getAll();
        HospitalDAO<Patient> dao = getPatientDAO();

        for (Task task : taskList) {
            patientList.remove(dao.getByKey(PATIENT_ID_COLUMN_NAME, task.getPatientId()).get(0));
        }

        for (int i = 0; i < patientList.size(); i++) {
            Patient patient = patientList.get(i);
            patientList.set(i, getAllInfoAboutPatient(patient.getId()));
        }

        return patientList;
    }

    public boolean insertOperationResultAndNewTask(Result result, Task task) {
        logger.info(": insertOperationResultAndNewTask()");
        try {
            connection.setAutoCommit(false);
            connection.setSavepoint();

            int resultIdAfterInsert = getResultDAO().insert(result);
            int taskIdAfterInsert = getTaskDAO().insert(task);

            if (resultIdAfterInsert != -1 && taskIdAfterInsert != -1) {
                result.setId(resultIdAfterInsert);
                task.setId(taskIdAfterInsert);
                connection.commit();
                return true;
            } else {
                connection.rollback();
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }


    public boolean insertFinalDiagnosisAndDischargePatient(Diagnosis diagnosis) {
        logger.info(": insertFinalDiagnosisAndDischargePatient()");
        try {
            connection.setAutoCommit(false);
            connection.setSavepoint();

            int diagnosisIdAfterInsert = getDiagnosisDAO().insert(diagnosis);

            int patientId = diagnosis.getPatientId();
            Patient patient = getPatientDAO().getByKey(PATIENT_ID_COLUMN_NAME, patientId).get(0);
            patient.setDischarge(true);

            boolean patientUpdatedSuccessfully = getPatientDAO().update(patient);

            if (patientUpdatedSuccessfully && diagnosisIdAfterInsert != -1) {
                diagnosis.setId(diagnosisIdAfterInsert);
                connection.commit();
                return true;
            } else {
                connection.rollback();
                return false;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
            return false;
        } finally {
            try {
                connection.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public List<Task> getStaffTasksWithoutResults(Type type) {
        logger.info(": getStaffTasksWithoutResults()");

        List<Task> allStaffTaskList = getTaskDAO().getByValue(TASK_TYPE_COLUMN_NAME,
                type.toString());

        List<Task> staffTaskListWithoutResults = new ArrayList<>();
        for (Task task : allStaffTaskList) {
            List<Result> byKey = getResultDAO().getByKey(RESULT_TASK_ID_COLUMN_NAME, task.getId());
            if (byKey == null || byKey.isEmpty()) {
                staffTaskListWithoutResults.add(task);
            }
        }

        return staffTaskListWithoutResults;
    }


}

