package data.dao.abstraction;


import data.Constants;
import entity.abstraction.Entity;

public interface EntityUtils<E extends Entity> extends Gettable<E>, Query<E>, Constants {

}
