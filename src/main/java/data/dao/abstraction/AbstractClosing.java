package data.dao.abstraction;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractClosing {


  protected void closeStatement(Statement statement) {
    try {
      if (statement != null) {
        statement.close();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }


  private void closeResultSet(ResultSet resultSet) {
    try {
      if (resultSet != null) {
        resultSet.close();
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }


  protected void closeStatementAndResultSet(Statement statement, ResultSet resultSet) {
    closeResultSet(resultSet);
    closeStatement(statement);
  }


}
