package data.dao.abstraction;

import entity.abstraction.Entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface Query<E extends Entity> {

  /**
   * SQL Query for getting all entities from DB.
   *
   * @return String representation of query
   */
  String getAll();

  /**
   * Method for processing SQL Query AND statement for getting entity from DB by ID.
   *
   * @param connection connection
   * @param keyName    Name of the key by which entity should be found
   * @param key        id of entity in database
   * @return PreparedStatement for processing
   * @see Entity
   * @see java.sql.Statement
   * @see PreparedStatement
   * @see SQLException
   */
  PreparedStatement getByKey(Connection connection,
                             String keyName,
                             int key) throws SQLException;

  /**
   * Method for processing SQL Query AND statement for getting entity from DB by ID.
   *
   * @param connection connection
   * @param valueName    Name of the value by which entity should be found
   * @param value        value of entity in database
   * @return PreparedStatement for processing
   * @see Entity
   * @see java.sql.Statement
   * @see PreparedStatement
   * @see SQLException
   */
  PreparedStatement getByValue(Connection connection,
                               String valueName,
                               String value) throws SQLException;

  /**
   * Method for processing SQL Query AND statement for inserting entity to DB.
   *
   * @param connection connection
   * @param entity     class that implements Entity
   * @return PreparedStatement for processing
   * @see Entity
   * @see java.sql.Statement
   * @see PreparedStatement
   * @see SQLException
   */
  PreparedStatement insert(Connection connection,
                           E entity) throws SQLException;

  /**
   * Method for processing SQL Query AND statement for updating entity in DB.
   *
   * @param connection connection
   * @param entity     class that implements Entity
   * @return PreparedStatement for processing
   * @see Entity
   * @see java.sql.Statement
   * @see PreparedStatement
   * @see SQLException
   */
  PreparedStatement update(Connection connection,
                           E entity) throws SQLException;

  /**
   * Method for processing SQL Query AND statement for deleting entity from DB.
   *
   * @param connection connection
   * @param id         id of entity
   * @return PreparedStatement for processing
   * @see Entity
   * @see java.sql.Statement
   * @see PreparedStatement
   * @see SQLException
   */
  PreparedStatement delete(Connection connection,
                           int id) throws SQLException;

  /**
   * Method for checking existence of entity in data base.
   *
   * @param entity entity
   * @return true - if entity already exists in database, otherwise - false
   * @throws SQLException
   * @see Entity
   * @see SQLException
   * @see PreparedStatement
   */
  PreparedStatement isExist(Connection connection,
                            E entity) throws SQLException;

}
