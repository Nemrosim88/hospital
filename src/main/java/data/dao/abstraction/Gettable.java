package data.dao.abstraction;

import entity.abstraction.Entity;
import java.sql.ResultSet;
import java.sql.SQLException;

public interface Gettable<E extends Entity> {

  /**
   * Method for extraction entity from Result Set.
   * @param resultSet resultSet
   * @return Entity
   * @throws SQLException
   * @see ResultSet
   * @see Entity
   */
  E getFromResultSet(ResultSet resultSet) throws SQLException;

}
