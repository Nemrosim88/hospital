package data;

public interface Constants {

  public static final String SCHEMA_NAME = "hospital";

  String USER_TABLE_NAME = "user";
  String USER_ID_COLUMN_NAME = "id";
  String USER_NAME_COLUMN_NAME = "name";
  String USER_SURNAME_COLUMN_NAME = "surname";
  String USER_PATRONYMIC_COLUMN_NAME = "patronymic";
  String USER_DAY_OF_BIRTH_COLUMN_NAME = "dayOfBirth";
  String USER_ROLE_COLUMN_NAME = "role";

  String DOCTOR_TABLE_NAME = "doctor";
  String DOCTOR_ID_COLUMN_NAME = "id";
  String DOCTOR_USER_ID_COLUMN_NAME = "userId";
  String DOCTOR_CARD_KEY_ID_COLUMN_NAME = "cardKeyId";

  String PATIENT_TABLE_NAME = "patient";
  String PATIENT_ID_COLUMN_NAME = "id";
  String PATIENT_USER_ID_COLUMN_NAME = "userId";
  String PATIENT_DISCHARGE_COLUMN_NAME = "discharge";

  String CREDENTIALS_TABLE_NAME = "credentials";
  String CREDENTIALS_ID_COLUMN_NAME = "id";
  String CREDENTIALS_USER_ID_COLUMN_NAME = "userId";
  String CREDENTIALS_PASSPORT_COLUMN_NAME = "passport";
  String CREDENTIALS_EMAIL_COLUMN_NAME = "email";
  String CREDENTIALS_PHONE_NUMBER_COLUMN_NAME = "phoneNumber";

  String REGISTRATION_TABLE_NAME = "registration";
  String REGISTRATION_ID_COLUMN_NAME = "id";
  String REGISTRATION_USER_ID_COLUMN_NAME = "userId";
  String REGISTRATION_LOGIN_COLUMN_NAME = "login";
  String REGISTRATION_PASSWORD_COLUMN_NAME = "password";

  String TASK_TABLE_NAME = "task";
  String TASK_ID_COLUMN_NAME = "id";
  String TASK_DOCTOR_ID_COLUMN_NAME = "doctorId";
  String TASK_PATIENT_ID_COLUMN_NAME = "patientId";
  String TASK_INFO_COLUMN_NAME = "info";
  String TASK_DATETIME_COLUMN_NAME = "datetime";
  String TASK_TYPE_COLUMN_NAME = "type";

  String RESULT_TABLE_NAME = "result";
  String RESULT_ID_COLUMN_NAME = "id";
  String RESULT_TASK_ID_COLUMN_NAME = "taskId";
  String RESULT_DATETIME_COLUMN_NAME = "datetime";
  String RESULT_INFO_COLUMN_NAME = "info";
  String RESULT_DOCTOR_ID_COLUMN_NAME = "doctorId";
  String RESULT_STAFF_ID_COLUMN_NAME = "staffId";

  String STAFF_TABLE_NAME = "staff";
  String STAFF_ID_COLUMN_NAME = "id";
  String STAFF_USER_ID_COLUMN_NAME = "userId";
  String STAFF_POSITION_COLUMN_NAME = "position";

  String DIAGNOSIS_TABLE_NAME = "diagnosis";
  String DIAGNOSIS_ID_COLUMN_NAME = "id";
  String DIAGNOSIS_PATIENT_ID_COLUMN_NAME = "patientId";
  String DIAGNOSIS_DOCTOR_ID_COLUMN_NAME = "doctorId";
  String DIAGNOSIS_INFO_COLUMN_NAME = "info";
  String DIAGNOSIS_START_COLUMN_NAME = "start";
  String DIAGNOSIS_END_COLUMN_NAME = "end";
  String DIAGNOSIS_FINAL_DIAGNOSIS_COLUMN_NAME = "finalDiagnosis";












}
