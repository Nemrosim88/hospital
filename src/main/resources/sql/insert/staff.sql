INSERT INTO hospital.staff (id, userId, position)
VALUES
  (1, 2, 'operator'),
  (2, 3, 'operator'),
  (3, 13, 'nurse'),
  (4, 14, 'nurse'),
  (5, 16, 'operator'),
  (6, 18, 'nurse'),
  (7, 20, 'nurse'),
  (8, 21, 'nurse')
