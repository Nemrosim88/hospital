CREATE TABLE hospital.credentials (
  id          INT(11)    NOT NULL AUTO_INCREMENT,
  userID      INT(11)    NOT NULL,
  passport    VARCHAR(8) NOT NULL,
  email       VARCHAR(45)         DEFAULT NULL,
  phoneNumber VARCHAR(45)         DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT credentialsUserId FOREIGN KEY (userID) REFERENCES user (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
)