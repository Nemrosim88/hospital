CREATE TABLE hospital.staff (
  id       INT(11)                    NOT NULL AUTO_INCREMENT,
  userId   INT(11)                    NOT NULL,
  position ENUM ('nurse', 'operator') NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT staffUserId FOREIGN KEY (userId) REFERENCES user (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
)