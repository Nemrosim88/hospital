CREATE TABLE hospital.result (
  id       INT(11)      NOT NULL AUTO_INCREMENT,
  taskId   INT(11)      NOT NULL,
  datetime DATETIME     NOT NULL,
  info     VARCHAR(500) NOT NULL,
  doctorId INT(11)               DEFAULT NULL,
  staffId  INT(11)               DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT resultDoctorId FOREIGN KEY (doctorId) REFERENCES doctor (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT resultStaffId FOREIGN KEY (staffId) REFERENCES staff (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT resultTaskId FOREIGN KEY (taskId) REFERENCES task (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)