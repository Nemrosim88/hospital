CREATE TABLE hospital.registration (
  id       INT(11)     NOT NULL AUTO_INCREMENT,
  userId   INT(11)     NOT NULL,
  login    VARCHAR(45) NOT NULL,
  password VARCHAR(45) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT registrationUserId FOREIGN KEY (userId) REFERENCES user (id)
    ON DELETE CASCADE
    ON UPDATE NO ACTION
)